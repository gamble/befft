/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"
#include <float.h>
#include <time.h>

#ifdef BEFFT_COLORS
#define BLACK "\e[0m"
#define SET_BLACK printf(BLACK)
#define GREEN "\e[1;32m"
#define SET_GREEN printf(GREEN)
#define RED "\e[1;31m"
#define SET_RED printf(RED)
#else
#define BLACK 
#define SET_BLACK
#define GREEN 
#define SET_GREEN 
#define RED 
#define SET_RED 
#endif

#if _MSC_VER
#define BEFFT_SSCANF sscanf_s
#else
#define BEFFT_SSCANF sscanf
#endif

/* generate random vectors with random errors */
void generate_random_vector( double * xre, double * xim, double * xab, double * xab_max, int coeff_error, be_size_t size ){
    *xab_max = 0;
    double coeff_err = pow(10, coeff_error);
    for (be_size_t j=0; j<size; j++) {
        xre[j] = ((double) rand())/((double) rand());
        xim[j] = ((double) rand())/((double) rand());
		xab[j] = ((double) rand());
		while( !( isnormal(xre[j])&& isnormal(xim[j])&&isnormal(xab[j]) ) ) {
			xre[j] = ((double) rand())/((double) rand());
			xim[j] = ((double) rand())/((double) rand());
			xab[j] = ((double) rand());
		}
        xab[j] = coeff_err*xab[j]/RAND_MAX;
        *xab_max = BEFFT_MAX(*xab_max, xab[j]);
    }
}

int main(int argc, char* argv[]) {
    
    uint nbCalls = 100;
    uint n = 4;
    uint nbPols = 1;
    int  c = -5;
    
    if (argc >= 2)
        BEFFT_SSCANF(argv[1], "%u", &n);
    
    if (argc >= 3)
        BEFFT_SSCANF(argv[2], "%u", &nbPols);
    
    if (argc >= 4)
        BEFFT_SSCANF(argv[3], "%u", &nbCalls);
    
    if (argc >= 5)
        BEFFT_SSCANF(argv[4], "%d", &c);
    
    printf("test fft for %u random vectors with log2 of size %u, and max of componentwise error %.1e\n", nbPols, n, pow(10,c));
    
#if !defined(BEFFT_HAS_ARB) && !defined(BEFFT_HAS_MPFR)
    if (n>=13) {
        SET_RED;
        printf("input vectors of length > 2^12 are not supported");
        SET_BLACK;
        printf("\n");
        return 0;
    }
#endif
    
    be_size_t N = ((be_size_t) 0x1)<<n;
    srand (0);
    
    double * xre   = (double *) befft_malloc (N*sizeof(double));
    double * xim   = (double *) befft_malloc (N*sizeof(double));
    double * xab   = (double *) befft_malloc (N*sizeof(double));
    double * yre_s = (double *) befft_malloc (N*sizeof(double));
    double * yim_s = (double *) befft_malloc (N*sizeof(double));
    double * yab_s = (double *) befft_malloc (N*sizeof(double));
    double * yre_e = (double *) befft_malloc (N*sizeof(double));
    double * yim_e = (double *) befft_malloc (N*sizeof(double));
    double * yab_e = (double *) befft_malloc (N*sizeof(double));
    double * yre_d = (double *) befft_malloc (N*sizeof(double));
    double * yim_d = (double *) befft_malloc (N*sizeof(double));
    double * yab_d = (double *) befft_malloc (N*sizeof(double));
    double xab_max; /* max of the absolute error on x = inf morm of xab*/
    
    double total_time_s = 0.;
    double total_time_e = 0.;
    double total_time_d = 0.;
    
    int    total_underflow = 0;
    
    double total_abserror_s = 0.;
    double total_abserror_e = 0.;
    double total_abserror_d = 0.;

    uint j;
    
    /* precomputation for befft */
    befft_fft_rad2_t rad2;
    clock_t start_befft_pre = clock();
    befft_fft_rad2_init(rad2, n);
    double befft_precomp_time = (double) (clock() - start_befft_pre)/CLOCKS_PER_SEC;
    
    for (uint i = 0; i<nbPols; i++) {
        
        int underflow = 0;
    
        generate_random_vector( xre, xim, xab, &xab_max, c, N );
    
        clock_t start_s = clock();
        double abserror_s = 0;
        for (j = 0; j<nbCalls; j++) {
            int underflow_t = befft_fft_rad2_ub_abs_error_precomp(yre_s, yim_s, &abserror_s, xre, xim, xab_max, rad2);
            underflow = underflow || underflow_t;
        }
        double time_s     = (double) (clock() - start_s)/CLOCKS_PER_SEC;
        total_time_s+=(time_s/j);
        total_underflow += underflow;
        total_abserror_s+=abserror_s;
        
        underflow = 0;
        clock_t start_e = clock();
        for (j = 0; j<nbCalls; j++) {
            int underflow_t = befft_fft_rad2_vec_abs_error_precomp ( yre_e, yim_e, yab_e, xre, xim, xab, rad2);
            underflow = underflow || underflow_t;
        }
        double time_e     = (double) (clock() - start_e)/CLOCKS_PER_SEC;
        double abserror_e = _be_real_vec_infnorm( yab_e, N );
        total_time_e+=(time_e/j);
        total_underflow += underflow;
        total_abserror_e+=abserror_e;
        
        underflow = 0;
        clock_t start_d = clock();
        for (j = 0; j<nbCalls; j++) {
            int underflow_t = befft_fft_rad2_dynamic_precomp(yre_d, yim_d, yab_d, xre, xim, xab, rad2);
            underflow = underflow || underflow_t;
        }
        double time_d = (double) (clock() - start_d)/CLOCKS_PER_SEC;
        double abserror_d = _be_real_vec_infnorm( yab_d, N );
        total_time_d+=(time_d/j);
        total_underflow += underflow;
        total_abserror_d+=abserror_d;
    
    }

    printf("&&&&&&&&&&&& time in befft_fft precomp                                       : %f\n", befft_precomp_time );   
    printf("&&&&&&&&&&&& average time in befft with static error                         : %.10f\n", total_time_s/nbPols );
    printf("&&&&&&&&&&&& average max of errors                                                      : %.10e\n", total_abserror_s/nbPols );
    printf("&&&&&&&&&&&& average time in befft with static error 2                       : %.10f\n", total_time_e/nbPols );
    printf("&&&&&&&&&&&& average max of errors                                                      : %.10e\n", total_abserror_e/nbPols );
    printf("&&&&&&&&&&&& average time in befft with dynamic error                        : %.10f\n", total_time_d/nbPols );
    printf("&&&&&&&&&&&& average max of errors                                                      : %.10e\n", total_abserror_d/nbPols );

    printf("&&&&&&&&&&&& underflow occured in                                              ");
    if (total_underflow > 0) SET_RED; else SET_GREEN; printf("%d", total_underflow); SET_BLACK; printf(" cases\n");
    
    befft_fft_rad2_clear(rad2);
    
/* in case n>=13, should empty flint caches */
#ifdef BEFFT_HAS_ARB
    flint_cleanup();
#endif
    
#ifdef BEFFT_HAS_MPFR
    mpfr_free_cache ();
#endif
    
    befft_free(xre);
    befft_free(xim);
    befft_free(xab);
    befft_free(yre_s);
    befft_free(yim_s);
    befft_free(yab_s);
    befft_free(yre_e);
    befft_free(yim_e);
    befft_free(yab_e);
    befft_free(yre_d);
    befft_free(yim_d);
    befft_free(yab_d);
    return 0;
}

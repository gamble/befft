all: alone

withArb:
	mkdir -p build
	cd build && cmake -D HAS_ARB=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
alone:
	mkdir -p build
	cd build && cmake -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
noinline:
	mkdir -p build
	cd build && cmake -D NO_INLINE=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
# all:
# 	mkdir -p build
# 	cd build && cmake -D HAS_ARB=1 -D HAS_MPFR=1 -DCMAKE_BUILD_TYPE=Release ../
# 	make -C build -j4 VERBOSE=1
	
withCRlibm:
	mkdir -p build
	cd build && cmake -D HAS_ARB=1 -D HAS_CRLIBM=1 -D HAS_MPFR=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
withMPFR:
	mkdir -p build
	cd build && cmake -DHAS_MPFR=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1

withARBandMPFR:
	mkdir -p build
	cd build && cmake -DHAS_ARB=1 -DHAS_MPFR=1 -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1
	
debug:
	mkdir -p build
	cd build && cmake -D HAS_ARB=1 -D HAS_MPFR=1 -DCMAKE_BUILD_TYPE=Debug ../
	make -C build -j4 VERBOSE=1
	
clang:
	mkdir -p build
	cd build && cmake -D CMAKE_C_COMPILER=clang -D HAS_ARB=1 -D HAS_MPFR=1 -DCMAKE_BUILD_TYPE=Debug ../
	make -C build -j4 VERBOSE=1
	
install:
	CMAKE_NO_VERBOSE=1 make -C build install -j4 VERBOSE=1
	
clean:
	rm -rf lib
	rm -rf bin
	rm -rf build
	rm -rf test_devel/*.o
	rm -rf dist 
	rm -rf befft.egg-info
	
	
# those commands have to be called with nmake on x64 Native Tools Command Prompt for VS

buildForWindowsStandalone:
	cmake -S . -B build -G "NMake Makefiles" -DBUILD_SHARED_LIBS=1 -DCMAKE_BUILD_TYPE=Release
	cmake --build build
	
buildForWindows:
	cmake -S . -B build -G "NMake Makefiles" -DHAS_ARB=1 -DHAS_MPFR=1 -DCMAKE_TOOLCHAIN_FILE=C:/Users/imbach/vcpkg/scripts/buildsystems/vcpkg.cmake -DBUILD_SHARED_LIBS=1 -DCMAKE_BUILD_TYPE=Release
	cmake --build build
	
# cmake -S . -B build -G "NMake Makefiles" -DBUILD_SHARED_LIBS=1 -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=C:/Users/imbach/vcpkg/scripts/buildsystems/vcpkg.cmake
	
buildForWindowsWithArb:
	cmake -S . -B build -G "NMake Makefiles" -DHAS_ARB=1 -DCMAKE_TOOLCHAIN_FILE=C:/Users/imbach/vcpkg/scripts/buildsystems/vcpkg.cmake -DBUILD_SHARED_LIBS=1 -DCMAKE_BUILD_TYPE=Release
	cmake --build build
	
buildForWindowsWithMpfr:
	cmake -S . -B build -G "NMake Makefiles" -DHAS_MPFR=1 -DCMAKE_TOOLCHAIN_FILE=C:/Users/imbach/vcpkg/scripts/buildsystems/vcpkg.cmake -DBUILD_SHARED_LIBS=1 -DCMAKE_BUILD_TYPE=Release
	cmake --build build
	
buildForWindowsDebug:
	cmake -S . -B build -G "NMake Makefiles" -DBUILD_SHARED_LIBS=1 -DCMAKE_BUILD_TYPE=Debug
	cmake --build build
	
installForWindows:
	cmake --build build
	cmake --install build

cleanForWindows:
	rmdir /s /q bin build lib dist befft.egg-info
	
testAlone:
	bin/check_befft
	
testWithArb:
	bin/check_befft 1 14 10
	
testWindowsAlone:
	.\bin\check_befft.exe

# INSTALL_LIBRARY_TARGET=/usr/local/lib
# INSTALL_HEADERS_TARGET=/usr/local/include
# 
# install:
# 	cp lib/libbefft.so ${INSTALL_LIBRARY_TARGET}
# 	cp src/*.h ${INSTALL_HEADERS_TARGET}

#test_befft_base:
#	bin/test_befft_base
	
#log2len=4
#nbvecs=2
#nbcalls=100
#coeffe=-5
#verbosity=0
#
#random_vectors:
#	bin/random_vectors ${log2len} ${nbvecs} ${nbcalls} ${coeffe}
#
#test_befft:
#	bin/test_befft ${log2len} ${nbvecs} ${nbcalls} ${coeffe} ${verbosity}
## 	valgrind --tool=memcheck --leak-check=yes bin/test_befft ${log2len} ${nbvecs} ${nbcalls} ${coeffe} ${verbosity}

#test_befft_valgrind:
#	bin/test_befft ${log2len} ${nbvecs} ${nbcalls} ${coeffe} ${verbosity}
#	valgrind --tool=memcheck --leak-check=yes bin/test_befft ${log2len} ${nbvecs} ${nbcalls} ${coeffe} ${verbosity}

#test_befft_vectorized:
#	bin/test_befft_vectorized ${log2len} ${nbvecs} ${nbcalls} ${coeffe} ${verbosity}
# 	valgrind --tool=memcheck --leak-check=yes bin/test_befft_vectorized ${log2len} ${nbvecs} ${coeffe} ${verbosity}

#test_befft_norm:
#	bin/test_befft_norm ${log2len} ${nbvecs} ${nbcalls} ${coeffe} ${verbosity}
# 	valgrind --tool=memcheck --leak-check=yes bin/test_befft_norm ${log2len} ${nbvecs} ${coeffe} ${verbosity}

#test_be:
#	bin/test_be
	
#test_be_vec:
#	bin/test_be_vec
#	valgrind --tool=memcheck --leak-check=yes bin/test_be_vec
#k=1
#test_crlibm:
#	bin/test_crlibm ${log2len} ${k}
#	
#test_mpfr:
#	bin/test_mpfr ${log2len} ${k}
#	valgrind --tool=memcheck --leak-check=yes bin/test_mpfr ${log2len} ${k}
	
#see_fft:
#	gcc -O2 -std=c99 -march=native -ftree-vectorize -funroll-loops -frounding-math -I./src -S -fverbose-asm -fopt-info-vec src/fft/fft_rad2_static.c
# 	cat fft.s
# 	rm  fft.s

#see_fft_missed:
#	gcc -O2 -std=c99 -march=native -ftree-vectorize -funroll-loops -frounding-math -I./src -S -fverbose-asm -fopt-info-vec-missed src/fft/fft_rad2_static.c
	
#see_fft_vect:
#	gcc -O2 -std=c99 -march=native -ftree-vectorize -funroll-loops -frounding-math -I./src -S -fverbose-asm -fopt-info-vec src/fft/fft_rad2_static_vect.c
# 	cat fft.s
# 	rm  fft.s

#see_fft_vect_missed:
#	gcc -O2 -std=c99 -march=native -ftree-vectorize -funroll-loops -frounding-math -I./src -S -fverbose-asm -fopt-info-vec-missed src/fft/fft_rad2_static_vect.c
	
#see_fft_dyna:
#	gcc -O2 -std=c99 -march=native -ftree-vectorize -funroll-loops -frounding-math -I./src -S -fverbose-asm -fopt-info-vec src/fft/fft_rad2_dynamic.c
# 	cat fft.s
# 	rm  fft.s

#see_fft_dyna_missed:
#	gcc -O2 -std=c99 -march=native -ftree-vectorize -funroll-loops -frounding-math -I./src -S -fverbose-asm -fopt-info-vec-missed src/fft/fft_rad2_dynamic.c
	
#see_norm:
#	gcc -O2 -std=c99 -march=native -ftree-vectorize -funroll-loops -frounding-math -I./src -S -fverbose-asm -fopt-info-vec src/be_vec/norms.c

#see_norm_missed:
#	gcc -O2 -std=c99 -march=native -ftree-vectorize -funroll-loops -frounding-math -I./src -S -fverbose-asm -fopt-info-vec-missed src/be_vec/norms.c
	
#profile_vectorized:
#	valgrind --tool=callgrind bin/test_befft_vectorized 10 5 1 -5 0 
#	kcachegrind callgrind.out.$!
#	
#profile_befft:
#	valgrind --tool=callgrind bin/test_befft 12 5 1 -5 0 
#	kcachegrind callgrind.out.$!

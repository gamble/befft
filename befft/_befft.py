#****************************************************************************
#        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
#                                Remi Imbach     <remi.imbach@laposte.net>
# 
#    This file is part of befft.
#
#    befft is free software: you can redistribute it and/or modify 
#    it under the terms of the GNU Lesser General Public License as 
#    published by the Free Software Foundation, either version 3 of 
#    the License, or (at your option) any later version.
#
#    befft is distributed in the hope that it will be useful, but 
#    WITHOUT ANY WARRANTY; without even the implied warranty of 
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#    See the GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public 
#    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
#*****************************************************************************

#import os
#import glob
#import platform
#import numpy as np
#import ctypes as ct
#import math

#befft_path = os.path.join(os.path.dirname(__file__), "..")
#print(befft_path)
#befft_lib  = glob.glob(os.path.join(befft_path, "libbefft.cp*"))[0]
#print(befft_lib)
#if platform.system() == "Linux":
    ##befft_lib  = os.path.join(befft_lib, "lib", "libbefft.so")
    #befft_lib  = ct.cdll.LoadLibrary(befft_lib)
#elif platform.system() == "Windows":
	##befft_lib  = os.path.join(befft_lib, "bin", "befft")
	#befft_lib  = ct.windll.LoadLibrary(befft_lib)
	
#fft_rad2_dynamic_signature = [ ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                               #ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.c_uint ]
#befft_lib.befft_fft_rad2_dynamic.argtypes = fft_rad2_dynamic_signature
#befft_lib.befft_fft_rad2_dynamic.restype  = ct.c_int 

#def fft_rad2( p_mid, p_rad ):
    #if (p_mid.shape[0] != p_rad.shape[0]):
        #raise ValueError("sizes of input vectors do not match")
    #k = math.log2(p_mid.shape[0])
    #if (math.floor(k) != k) or (k < 0):
        #raise ValueError("sizes of input vectors must be a positive power of 2")
    #k=int(k)
    #K=int(2**k)
    #p_re = np.ctypeslib.as_ctypes(np.array(p_mid.real))
    #p_im = np.ctypeslib.as_ctypes(np.array(p_mid.imag))
    #p_ae = np.ctypeslib.as_ctypes(np.array(p_rad))
    
    #KdoublesArray = ct.c_double*K
    #y_re = KdoublesArray()
    #y_im = KdoublesArray()
    #y_ae = KdoublesArray()
    
    #befft_lib.befft_fft_rad2_dynamic( y_re, y_im, y_ae, p_re, p_im, p_ae, ct.c_uint(k) )
    
    #y_mid = np.zeros(K, dtype=np.complex128)
    #y_rad = np.zeros(K, dtype=np.double)
    #for i in range(0,K):
        #y_mid[i] = (y_re[i]) + (y_im[i])*1j
        #y_rad[i] = y_ae[i]
    #return y_mid, y_rad

from befft._interface import _c_fft_rad2
import math
import numpy as np

def fft_rad2( p_mid, p_rad ):
    
    p_re = np.array(p_mid.real, dtype=np.float64)
    p_im = np.array(p_mid.imag, dtype=np.float64)
    p_ae = np.array(p_rad, dtype=np.float64)
    
    k = math.log2(p_mid.shape[0])
    if (math.floor(k) != k) or (k < 0):
        raise ValueError("sizes of input vectors must be a positive power of 2")
    k=int(k)
    K=int(2**k)
    
    y_re = np.zeros(K, dtype=np.float64)
    y_im = np.zeros(K, dtype=np.float64)
    y_ae = np.zeros(K, dtype=np.float64)
    
    #print("p_re: ", p_re);
    #print("p_im: ", p_im);
    #print("p_ae: ", p_ae);
    #print("y_re: ", y_re);
    #print("y_im: ", y_im);
    #print("y_ae: ", y_ae);
    
    #print("before call")
    status = _c_fft_rad2( y_re, y_im, y_ae, p_re, p_im, p_ae, k )
    #print("after call")
    y_mid = np.zeros(K, dtype=np.complex128)
    y_rad = np.zeros(K, dtype=np.float64)
    for i in range(0,K):
        y_mid[i] = (y_re[i]) + (y_im[i])*1j
        y_rad[i] = y_ae[i]
        
    return y_mid, y_rad

/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

// #define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "Python.h"
#include "numpy/ndarrayobject.h"
#include "befft.h"

/* arguments should be: */
/* numpy arrays y_real, y_imag, y_aber, x_real, x_imag, x_aber */
/* unsigned integer n */
static PyObject* py_fft_rad2_dynamic(PyObject* self, PyObject* args) {
    
    PyObject *arg1=NULL, *arg2=NULL, *arg3=NULL, *arg4=NULL, *arg5=NULL, *arg6=NULL;
    int log2size;
    PyArrayObject *y_real, *y_imag, *y_aber, *x_real, *x_imag, *x_aber;
    if(!PyArg_ParseTuple(args, "OOOOOOi", &arg1, &arg2, &arg3, &arg4, &arg5, &arg6, &log2size)) {
        return NULL;
    }
    // Check input type
    if( !PyArray_Check(arg1) || !PyArray_Check(arg2) || !PyArray_Check(arg3) 
     || !PyArray_Check(arg4) || !PyArray_Check(arg5) || !PyArray_Check(arg6) ) {
        PyErr_SetString(PyExc_TypeError, "The arguments should be numpy arrays");
        return NULL;
    }
    // get input and output
    y_real = (PyArrayObject*) arg1;
    y_imag = (PyArrayObject*) arg2;
    y_aber = (PyArrayObject*) arg3;
    x_real = (PyArrayObject*) arg4;
    x_imag = (PyArrayObject*) arg5;
    x_aber = (PyArrayObject*) arg6;
    int Dyr= PyArray_NDIM(y_real);
    int Dyi= PyArray_NDIM(y_imag);
    int Dya= PyArray_NDIM(y_aber);
    int Dxr= PyArray_NDIM(x_real);
    int Dxi= PyArray_NDIM(x_imag);
    int Dxa= PyArray_NDIM(x_aber);
    
    if(  (Dyr<1) || (Dyr>1) 
      || (Dyi<1) || (Dyi>1) 
      || (Dya<1) || (Dya>1)
      || (Dxr<1) || (Dxr>1)
      || (Dxi<1) || (Dxi>1)
      || (Dxa<1) || (Dxa>1) ) {
        PyErr_SetString(PyExc_TypeError, "Arrays should have dimension 1");
        return NULL;
    }
    npy_intp Nyr = PyArray_DIM(y_real, Dyr-1);
    npy_intp Nyi = PyArray_DIM(y_imag, Dyi-1);
    npy_intp Nya = PyArray_DIM(y_aber, Dya-1);
    npy_intp Nxr = PyArray_DIM(x_real, Dxr-1);
    npy_intp Nxi = PyArray_DIM(x_imag, Dxi-1);
    npy_intp Nxa = PyArray_DIM(x_aber, Dxa-1);
    // Check layout
    if(    !PyArray_ISCARRAY_RO(y_real) 
        || !PyArray_ISCARRAY_RO(y_imag) 
        || !PyArray_ISCARRAY_RO(y_aber)
        || !PyArray_ISCARRAY_RO(x_real)
        || !PyArray_ISCARRAY_RO(x_imag)
        || !PyArray_ISCARRAY_RO(x_aber)
    ) {
        PyErr_SetString(PyExc_TypeError, "Arrays should have a C contiguous layout");
        return NULL;
    }
    // Check sizes
    if (  (Dyr != Dyi) || (Dyr != Dya) || (Dyr != Dxr) || (Dyr != Dxi) || (Dyr != Dxa)
       || (Nyr != Nyi) || (Nyr != Nya) || (Nyr != Nxr) || (Nyr != Nxi) || (Nyr != Nxa) ) {
        PyErr_SetString(PyExc_TypeError, "Array sizes don't match");
        return NULL;
    }
    // Check types in array
    if( ( PyArray_DESCR(y_real)->type_num != NPY_FLOAT64 )
      ||( PyArray_DESCR(y_real)->type_num != NPY_FLOAT64 )
      ||( PyArray_DESCR(y_real)->type_num != NPY_FLOAT64 )
      ||( PyArray_DESCR(y_real)->type_num != NPY_FLOAT64 )
      ||( PyArray_DESCR(y_real)->type_num != NPY_FLOAT64 )
      ||( PyArray_DESCR(y_real)->type_num != NPY_FLOAT64 ) ) {
        PyErr_SetString(PyExc_TypeError, "Array types should float64");
        return NULL;
    }
    //     int status = befft_fft_rad2_dynamic ( (double*) PyArray_DATA(y_real), 
//                                           (double*) PyArray_DATA(y_imag),
//                                           (double*) PyArray_DATA(y_aber),
//                                           (double*) PyArray_DATA(x_real),
//                                           (double*) PyArray_DATA(x_imag),
//                                           (double*) PyArray_DATA(x_aber),
//                                           (uint) log2size );
    befft_fft_rad2_t rad2;
    befft_fft_rad2_init( rad2, (uint) log2size );
    
    int status = befft_fft_rad2_dynamic_precomp ( (double*) PyArray_DATA(y_real), 
                                                  (double*) PyArray_DATA(y_imag),
                                                  (double*) PyArray_DATA(y_aber),
                                                  (double*) PyArray_DATA(x_real),
                                                  (double*) PyArray_DATA(x_imag),
                                                  (double*) PyArray_DATA(x_aber),
                                                  rad2 );
    
    befft_fft_rad2_clear(rad2);
    
    return PyLong_FromLong(status);
}

static PyMethodDef befftMethods[] = {
    {"_c_fft_rad2", py_fft_rad2_dynamic, METH_VARARGS,
     "Compute fft with error bound."},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef befftmodule = {
    PyModuleDef_HEAD_INIT,
    "_interface",
    NULL,
    -1,
    befftMethods
//     NULL,
//     NULL,
//     NULL,
//     NULL
};

PyMODINIT_FUNC PyInit__interface(void)
{
    PyObject *m;
    m = PyModule_Create(&befftmodule);
    if (m == NULL) {
        return NULL;
    }
    import_array();
    return m;
}

#****************************************************************************
#        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
#                                Remi Imbach     <remi.imbach@laposte.net>
# 
#    This file is part of befft.
#
#    befft is free software: you can redistribute it and/or modify 
#    it under the terms of the GNU Lesser General Public License as 
#    published by the Free Software Foundation, either version 3 of 
#    the License, or (at your option) any later version.
#
#    befft is distributed in the hope that it will be useful, but 
#    WITHOUT ANY WARRANTY; without even the implied warranty of 
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#    See the GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public 
#    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
#*****************************************************************************

# from befft._befft import fft_rad2

from befft._interface import _c_fft_rad2
import math
import numpy as np

def fft_rad2( p_mid, p_rad ):
    
    p_re = np.array(p_mid.real, dtype=np.float64)
    p_im = np.array(p_mid.imag, dtype=np.float64)
    p_ae = np.array(p_rad, dtype=np.float64)
    
    k = math.log2(p_mid.shape[0])
    if (math.floor(k) != k) or (k < 0):
        raise ValueError("sizes of input vectors must be a positive power of 2")
    k=int(k)
    K=int(2**k)
    
    y_re = np.zeros(K, dtype=np.float64)
    y_im = np.zeros(K, dtype=np.float64)
    y_ae = np.zeros(K, dtype=np.float64)
    
    #print("p_re: ", p_re);
    #print("p_im: ", p_im);
    #print("p_ae: ", p_ae);
    #print("y_re: ", y_re);
    #print("y_im: ", y_im);
    #print("y_ae: ", y_ae);
    
    #print("before call")
    status = _c_fft_rad2( y_re, y_im, y_ae, p_re, p_im, p_ae, k )
    #print("after call")
    y_mid = np.zeros(K, dtype=np.complex128)
    y_rad = np.zeros(K, dtype=np.float64)
    for i in range(0,K):
        y_mid[i] = (y_re[i]) + (y_im[i])*1j
        y_rad[i] = y_ae[i]
        
    return y_mid, y_rad

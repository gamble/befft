/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"
#include <float.h>
#include <time.h>

#ifdef BEFFT_HAS_ARB
#include "acb_dft.h"
#include "flint/flint.h"
#include "arf.h"
#endif

#ifdef BEFFT_HAS_CRLIBM
#include "crlibm.h"
#endif

#ifdef BEFFT_COLORS
#define BLACK "\e[0m"
#define SET_BLACK printf(BLACK)
#define GREEN "\e[1;32m"
#define SET_GREEN printf(GREEN)
#define RED "\e[1;31m"
#define SET_RED printf(RED)
#else
#define BLACK 
#define SET_BLACK
#define GREEN 
#define SET_GREEN 
#define RED 
#define SET_RED 
#endif

#if _MSC_VER
#define BEFFT_SSCANF sscanf_s
#else
#define BEFFT_SSCANF sscanf
#endif

void print_table( double * xre, double * xim, ulong size ){
    printf("[ \n");
    for (ulong j=0; j<size; j++) {
        printf("  <%.5e + I%.5e>, \n", xre[j], xim[j]);
    }
    printf("] \n");
}

#ifdef BEFFT_HAS_ARB

void print_acb_vec( acb_ptr v, ulong size ){
    printf("[ \n");
    for (ulong j=0; j<size; j++) {
        acb_printd(v+j, 5); printf("\n");
    }
    printf("] \n");
}

int _be_center_is_in_acb( const double yre, const double yim, acb_t z ){
    arf_t re,im;
    arf_init(re);
    arf_init(im);
    
    arf_set_d(re, yre);
    arf_set_d(im, yim);
    int res = arb_contains_arf( acb_realref(z), re ) && arb_contains_arf( acb_imagref(z), im );
    
    arf_clear(re);
    arf_clear(im);
    
    return res;
}

int _be_overlaps_acb( const double yre, const double yim, const double yab, acb_t z ){
    acb_t y;
    acb_init(y);
    
    arf_set_d( arb_midref(acb_realref(y)), yre);
    arf_set_d( arb_midref(acb_imagref(y)), yim);
    mag_set_d( arb_radref(acb_realref(y)), yab);
    mag_set_d( arb_radref(acb_imagref(y)), yab);
    int res = acb_overlaps(y,z);
    
    acb_clear(y);
    
    return res;
}

int _be_vec_overlaps_acb_vec( double * yre, double * yim, const double yab, ulong size, acb_ptr z ){
    for (ulong j=0; j<size; j++) {
        if ( _be_overlaps_acb(yre[j],yim[j],yab,z+j)==0 )
            return 0;
    }
    return 1;
}

int _be_vec_aber_overlaps_acb_vec( double * yre, double * yim, double * yab, ulong size, acb_ptr z ){
    for (ulong j=0; j<size; j++) {
        if ( _be_overlaps_acb(yre[j],yim[j],yab[j],z+j)==0 )
            return 0;
    }
    return 1;
}

int _be_vec_center_is_in_acb_vec( double * yre, double * yim, ulong size, acb_ptr z ){
    for (ulong j=0; j<size; j++) {
        if ( _be_center_is_in_acb( yre[j], yim[j], z+j )==0 )
            return 0;
    }
    return 1;
}

void _be_center_max_dist_to_acb( arf_t dest, const double yre, const double yim, acb_t z ){
    arf_t re,im;
    arf_init(re);
    arf_init(im);
    
    arf_set_d(re, yre);
    arf_set_d(im, yim);
    arf_sub(re, re, arb_midref(acb_realref(z)), BEFFT_PREC, ARF_RND_UP );
    arf_sub(im, im, arb_midref(acb_imagref(z)), BEFFT_PREC, ARF_RND_UP  );
    arf_abs(re, re );
    arf_abs(im, im );
    arf_max(dest, re, im);
    
    arf_clear(re);
    arf_clear(im);
}

double _be_vec_center_max_dist_to_acb_vec( double * yre, double * yim, ulong size, acb_ptr z ){
    arf_t dist, temp;
    arf_init(dist);
    arf_init(temp);
    _be_center_max_dist_to_acb( dist, yre[0], yim[0], z+0 );
    for (ulong j=1; j<size; j++) {
        _be_center_max_dist_to_acb( temp, yre[j], yim[j], z+j );
        arf_max(dist, dist, temp);
    }
    double res = arf_get_d( dist, ARF_RND_UP );
    arf_clear(dist);
    arf_clear(temp);
    return res;
}

double _acb_vec_max_error( acb_ptr z, ulong size ) {
    mag_t max;
    mag_init(max);
    mag_max(max, arb_radref( acb_realref(z+0) ), arb_radref( acb_imagref(z+0) ) );
    for (ulong j=1; j<size; j++) {
        mag_max(max, max, arb_radref( acb_realref(z+j) ) );
        mag_max(max, max, arb_radref( acb_imagref(z+j) ) );
    }
    double res = mag_get_d(max);
    mag_clear(max);
    return res;
}
#endif //ifdef BEFFT_HAS_ARB

int _be_vec_centers_equal( double * yre, double * yim, double * xre, double * xim, ulong size){
    for (ulong j=0; j<size; j++) {
        if ( (yre[j]!=xre[j])||(yim[j]!=xim[j]) )
            return 0;
    }
    return 1;
}

void generate_random_vector( double * xre, double * xim, double * xab, double * xab_max, int coeff_error, ulong size ){
    *xab_max = 0;
    double coeff_err = pow(10, coeff_error);
    double max_val = 0;
    for (ulong j=0; j<size; j++) {
//         xre[j] = DBL_MIN*((double) rand())/((double) rand());
        xre[j] = ((double) rand())/((double) rand());
        xim[j] = ((double) rand())/((double) rand());
		xab[j] = ((double) rand());
		while( !( isnormal(xre[j])&& isnormal(xim[j])&&isnormal(xab[j]) ) ) {
			xre[j] = ((double) rand())/((double) rand());
			xim[j] = ((double) rand())/((double) rand());
			xab[j] = ((double) rand());
		}
        xab[j] = coeff_err*xab[j]/RAND_MAX;
        *xab_max = BEFFT_MAX(*xab_max, xab[j]);
        max_val = BEFFT_MAX( max_val, xre[j] );
        max_val = BEFFT_MAX( max_val, xim[j] );
    }
    max_val*=BEFFT_SQRTTWO;
//    max_val*=1000;
//	printf("generate_random_vector: max_val: %.16f\n", max_val);
    /* set max norm to 1 */
    for (ulong j=0; j<size; j++) {
        xre[j] = xre[j]/max_val;
        xim[j] = xim[j]/max_val;
    }
}

int main(int argc, char* argv[]) {
    
    uint nbCalls = 100;
    uint n = 4;
    uint nbPols = 1;
    int  c = -5;
    int  verbosity=1;
    
    if (argc >= 2)
        BEFFT_SSCANF(argv[1], "%u", &n);
    
    if (argc >= 3)
        BEFFT_SSCANF(argv[2], "%u", &nbPols);
    
    if (argc >= 4)
        BEFFT_SSCANF(argv[3], "%u", &nbCalls);
    
    if (argc >= 5)
        BEFFT_SSCANF(argv[4], "%d", &c);
    
    if (argc >= 6)
        BEFFT_SSCANF(argv[5], "%d", &verbosity);
    
    printf("test fft for %u random vectors with log2 of size %u, and max of componentwise error %.1e\n", nbPols, n, pow(10,c));
    
    be_size_t N = ((be_size_t) 0x1)<<n;
    srand (0);
    
    double * xre   = (double *) befft_malloc (N*sizeof(double));
    double * xim   = (double *) befft_malloc (N*sizeof(double));
    double * xab   = (double *) befft_malloc (N*sizeof(double));
    double * yre_s = (double *) befft_malloc (N*sizeof(double));
    double * yim_s = (double *) befft_malloc (N*sizeof(double));
    double * yab_s = (double *) befft_malloc (N*sizeof(double));
    double * yre_e = (double *) befft_malloc (N*sizeof(double));
    double * yim_e = (double *) befft_malloc (N*sizeof(double));
    double * yab_e = (double *) befft_malloc (N*sizeof(double));
    double * yre_d = (double *) befft_malloc (N*sizeof(double));
    double * yim_d = (double *) befft_malloc (N*sizeof(double));
    double * yab_d = (double *) befft_malloc (N*sizeof(double));
    double * yre_g = (double *) befft_malloc (N*sizeof(double));
    double * yim_g = (double *) befft_malloc (N*sizeof(double));
    double * yab_g = (double *) befft_malloc (N*sizeof(double));
    
    double * x   = (double *) befft_malloc (3*N*sizeof(double));
    double * y   = (double *) befft_malloc (3*N*sizeof(double));
//     double * yre_o = (double *) befft_malloc (N*sizeof(double));
//     double * yim_o = (double *) befft_malloc (N*sizeof(double));
//     double * yab_o = (double *) befft_malloc (N*sizeof(double));
    double xab_max; /* max of the absolute error on x = inf morm of xab*/
    
    double total_time_s = 0.;
    double total_time_e = 0.;
    double total_time_d = 0.;
    double total_time_g = 0.;
//     double total_time_o = 0.;
    
    int    total_underflow = 0;
    int    all_same_centers = 1;
    
    double total_abserror_s = 0.;
    double total_abserror_e = 0.;
    double total_abserror_d = 0.;
    double total_abserror_g = 0.;
//     double total_abserror_o = 0.;
 
#ifdef BEFFT_HAS_ARB
    acb_ptr xacb = _acb_vec_init(N);
    acb_ptr zacb = _acb_vec_init(N);
    
    double total_time_a = 0.;
    int center_in_acb = 1;
    int center_s_overlaps_acb = 1;
    int center_e_overlaps_acb = 1;
    int center_d_overlaps_acb = 1;
    int center_g_overlaps_acb = 1;
//     int center_o_overlaps_acb = 1;
    double max_dists_centers_2_centers = 0.;
    double total_abserror_a = 0.;
#endif

    uint j;
    
    /* precomputation for befft */
    befft_fft_rad2_t rad2;
#ifdef BEFFT_HAS_CRLIBM
    ulong No2 = N>>1;
    double * omegas_re = (double *) befft_malloc ((No2)*sizeof(double));
    double * omegas_im = (double *) befft_malloc ((No2)*sizeof(double));
    double   omegas_ae;
    
    clock_t start_crlibm_pre = clock();
    for (j = 0; j<nbCalls; j++)
        _be_vec_uint_with_crlibm( omegas_re, omegas_im, &(omegas_ae), n-1);
    
    double crlibm_precomp_time = (double) (clock() - start_crlibm_pre)/(j*CLOCKS_PER_SEC);
    clock_t start_befft_pre = clock();
    befft_fft_rad2_init_with_omegas( rad2, omegas_re, omegas_im, omegas_ae, n );
    double befft_precomp_time = (double) (clock() - start_befft_pre)/CLOCKS_PER_SEC;
    
//     be_vec_t omegas;
//     be_vec_init(omegas, No2);
//     be_vec_omega_ulong( omegas, n-1 );
//     double *omega_re = be_vec_realref(omegas);
//     double *omega_im = be_vec_imagref(omegas);
//     double *omega2_re = be_vec_realref(befft_fft_rad2_omegasref(rad2));
//     double *omega2_im = be_vec_imagref(befft_fft_rad2_omegasref(rad2));
//     for (ulong k=0; k< No2; k++) {
//         printf("-%lu-th order %lu root of unity computed in befft   : %.20e + I %.20e\n", k, No2, omega_re[k], omega_im[k] );
//         printf("                              max of relative errors: % .20e         \n", be_vec_aberref(omegas));
//         printf("-%lu-th order %lu root of unity computed with CRlibm: %.20e + I %.20e\n", k, No2, omega2_re[k], omega2_im[k] );
//         printf("                              max of relative errors: % .20e         \n", be_vec_aberref(befft_fft_rad2_omegasref(rad2)));
//     }
//     be_vec_clear(omegas);
#else
    clock_t start_befft_pre = clock();
    befft_fft_rad2_init(rad2, n);
    double befft_precomp_time = (double) (clock() - start_befft_pre)/CLOCKS_PER_SEC;
#endif
    
#ifdef BEFFT_HAS_ARB
    /* precomputation for acb_dft */
    acb_dft_rad2_t dftrad2;
    clock_t start_acb_pre = clock();
    acb_dft_rad2_init( dftrad2, n, BEFFT_PREC );
    double acb_precomp_time = (double) (clock() - start_acb_pre)/CLOCKS_PER_SEC;
#endif
    
    for (uint i = 0; i<nbPols; i++) {
        
        int underflow = 0;
        
        if (verbosity)
            printf("&&& vec %u\n", i);
    
        generate_random_vector( xre, xim, xab, &xab_max, c, N );
        for (ulong ind=0; ind<N; ind++) {
            x[3*ind]   = xre[ind];
            x[3*ind+1] = xim[ind];
            x[3*ind+2] = xab[ind];
        }
    
        clock_t start_s = clock();
        double abserror_s = 0;
        for (j = 0; j<nbCalls; j++) {
            int underflow_t = befft_fft_rad2_ub_abs_error_precomp(yre_s, yim_s, &abserror_s, xre, xim, xab_max, rad2);
            underflow = underflow || underflow_t;
        }
        double time_s     = (double) (clock() - start_s)/CLOCKS_PER_SEC;
        total_time_s+=(time_s/j);
        total_underflow += underflow;
        total_abserror_s+=abserror_s;
        if (verbosity) {
			if (i==12) {
				printf(" input vector: \n");
				for (ulong ind=0; ind<N; ind++) {
					_be_printd (xre[ind], xim[ind], xab[ind], 20); printf("\n");
				}
			}
            printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft with static error  : %f\n", time_s/j );
            printf("      underflow occured?                               : %d\n", underflow );
            printf("      absolute error                                   : %.5e\n", abserror_s );
        }
        
        underflow = 0;
        clock_t start_e = clock();
        for (j = 0; j<nbCalls; j++) {
            int underflow_t = befft_fft_rad2_vec_abs_error_precomp ( yre_e, yim_e, yab_e, xre, xim, xab, rad2);
            underflow = underflow || underflow_t;
        }
        double time_e     = (double) (clock() - start_e)/CLOCKS_PER_SEC;
        double abserror_e = _be_real_vec_infnorm( yab_e, N );
        int center_equal_e = _be_vec_centers_equal(yre_e, yim_e, yre_s, yim_s, N);
        all_same_centers&=center_equal_e;
        total_time_e+=(time_e/j);
        total_underflow += underflow;
        total_abserror_e+=abserror_e;
        if (verbosity) {
            printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft with static error 2: %f\n", time_e/j );
            printf("      underflow occured?                               : %d\n", underflow );
            printf("      max of absolute errors                           : %.5e\n", abserror_e );
            printf("      same centers?                                    : %d\n", center_equal_e );
        }
        
        underflow = 0;
        clock_t start_d = clock();
        for (j = 0; j<nbCalls; j++) {
            int underflow_t = befft_fft_rad2_dynamic_precomp(yre_d, yim_d, yab_d, xre, xim, xab, rad2);
            underflow = underflow || underflow_t;
        }
        double time_d = (double) (clock() - start_d)/CLOCKS_PER_SEC;
        double abserror_d = _be_real_vec_infnorm( yab_d, N );
        int center_equal_d = _be_vec_centers_equal(yre_d, yim_d, yre_s, yim_s, N);
        all_same_centers&=center_equal_d;
        total_time_d+=(time_d/j);
        total_underflow += underflow;
        total_abserror_d+=abserror_d;
        if (verbosity) {
            printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft with dynamic error : %f\n", time_d/j );
            printf("      underflow occured?                               : %d\n", underflow );
            printf("      max of absolute errors                           : %.5e\n", abserror_d );
            printf("      same centers?                                    : %d\n", center_equal_d );
        }
        
        underflow = 0;
        clock_t start_g = clock();
        for (j = 0; j<nbCalls; j++) {
            int underflow_t = befft_fft_rad2_dynamic_precomp_gathered(y, x, rad2);
            underflow = underflow || underflow_t;
        }
        double time_g = (double) (clock() - start_g)/CLOCKS_PER_SEC;
        for (ulong ind=0; ind<N; ind++) {
            yre_g[ind]=y[3*ind]  ;
            yim_g[ind]=y[3*ind+1];
            yab_g[ind]=y[3*ind+2];
        }
        double abserror_g = _be_real_vec_infnorm( yab_g, N );
        int center_equal_g = _be_vec_centers_equal(yre_g, yim_g, yre_s, yim_s, N);
        all_same_centers&=center_equal_g;
        total_time_g+=(time_g/j);
        total_underflow += underflow;
        total_abserror_g+=abserror_g;
        if (verbosity) {
            printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft with dynamic error : %f\n", time_g/j );
            printf("      underflow occured?                               : %d\n", underflow );
            printf("      max of absolute errors                           : %.5e\n", abserror_g );
            printf("      same centers?                                    : %d\n", center_equal_g );
        }
        
//         underflow = 0;
//         clock_t start_o = clock();
//         for (j = 0; j<nbCalls; j++) {
//             int underflow_t = befft_fft_rad2_dynamic_old_precomp(yre_o, yim_o, yab_o, xre, xim, xab, rad2);
//             underflow = underflow || underflow_t;
//         }
//         double time_o = (double) (clock() - start_o)/CLOCKS_PER_SEC;
//         double abserror_o = _be_real_vec_infnorm( yab_o, N );
//         int center_equal_o = _be_vec_centers_equal(yre_o, yim_o, yre_o, yim_s, N);
//         all_same_centers&=center_equal_o;
//         total_time_o+=(time_o/j);
//         total_underflow += underflow;
//         total_abserror_o+=abserror_o;
//         if (verbosity) {
//             printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft with old dynamic error : %f\n", time_o/j );
//             printf("      underflow occured?                               : %d\n", underflow );
//             printf("      max of absolute errors                           : %.5e\n", abserror_o );
//             printf("      same centers?                                    : %d\n", center_equal_o );
//         }
    
#ifdef BEFFT_HAS_ARB
        for (ulong j=0; j<N; j++){
            acb_set_d_d(xacb+j, xre[j], xim[j]);
            mag_set_d( arb_radref( acb_realref(xacb+j) ), xab[j] );
            mag_set_d( arb_radref( acb_imagref(xacb+j) ), xab[j] );
        }
        
        clock_t start_a = clock();
        for (j = 0; j<nbCalls; j++) {
               acb_dft_rad2_precomp( zacb, xacb, dftrad2, BEFFT_PREC);
        }
        double time_a = (double) (clock() - start_a)/CLOCKS_PER_SEC;
        double abserror_a = _acb_vec_max_error( zacb, N );
        total_time_a+=(time_a/j);
        total_abserror_a+=abserror_a;
       
        if (verbosity) {
            printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in acbfft                   : %f\n", (time_a/j) );
            printf("      max of absolute errors                           : %.5e\n", abserror_a );
            printf("\n");
        }
    
        center_in_acb         = center_in_acb         && _be_vec_center_is_in_acb_vec(yre_s, yim_s, N, zacb);
        center_s_overlaps_acb = center_s_overlaps_acb && _be_vec_overlaps_acb_vec(yre_s, yim_s, abserror_s, N, zacb);
        center_e_overlaps_acb = center_e_overlaps_acb && _be_vec_aber_overlaps_acb_vec(yre_e, yim_e, yab_e, N, zacb);
        center_d_overlaps_acb = center_d_overlaps_acb && _be_vec_aber_overlaps_acb_vec(yre_d, yim_d, yab_d, N, zacb);
        center_g_overlaps_acb = center_g_overlaps_acb && _be_vec_aber_overlaps_acb_vec(yre_g, yim_g, yab_g, N, zacb);
//         center_o_overlaps_acb = center_o_overlaps_acb && _be_vec_aber_overlaps_acb_vec(yre_o, yim_o, yab_o, N, zacb);
        max_dists_centers_2_centers += _be_vec_center_max_dist_to_acb_vec(yre_s, yim_s, N, zacb );
        if (verbosity) {
            printf("      is center(yre+Iyim) in zacb                      : %d\n", 
                    _be_vec_center_is_in_acb_vec(yre_s, yim_s, N, zacb) );
            printf("      does yre+Iyim with static  error   overlaps zacb : %d\n", 
                    _be_vec_overlaps_acb_vec(yre_s, yim_s, abserror_s, N, zacb) );
            printf("      does yre+Iyim with static  error 2 overlaps zacb : %d\n", 
                    _be_vec_aber_overlaps_acb_vec(yre_e, yim_e, yab_e, N, zacb) );
            printf("      does yre+Iyim with dynamic error   overlaps zacb : %d\n", 
                    _be_vec_aber_overlaps_acb_vec(yre_d, yim_d, yab_d, N, zacb) );
            printf("      does yre+Iyim with dynamic error g overlaps zacb : %d\n", 
                    _be_vec_aber_overlaps_acb_vec(yre_g, yim_g, yab_g, N, zacb) );
//             printf("      does yre+Iyim with dynamic error o overlaps zacb : %d\n", 
//                     _be_vec_aber_overlaps_acb_vec(yre_o, yim_o, yab_o, N, zacb) );
            printf("      max of dists center to center                    : %.5e\n",
                    _be_vec_center_max_dist_to_acb_vec(yre_s, yim_s, N, zacb ) );
        }
#endif
    }
    
#ifdef BEFFT_HAS_CRLIBM
    printf("&&&&&&&&&&&& time in computing omegas with CRlibm                            : %f\n", crlibm_precomp_time );
    befft_free(omegas_re);
    befft_free(omegas_im);
#endif
    printf("&&&&&&&&&&&& time in befft_fft precomp                                       : %f\n", befft_precomp_time );
#ifdef BEFFT_HAS_ARB
    printf("&&&&&&&&&&&& time in acb_dft_rad2 precomp                                    : %f\n", acb_precomp_time );
#endif    
    printf("&&&&&&&&&&&& average time in befft with static error                         : %.10f\n", total_time_s/nbPols );
    printf("&&&&&&&&&&&& average max of errors                                                      : %.10e\n", total_abserror_s/nbPols );
    printf("&&&&&&&&&&&& average time in befft with static error 2                       : %.10f\n", total_time_e/nbPols );
    printf("&&&&&&&&&&&& average max of errors                                                      : %.10e\n", total_abserror_e/nbPols );
    printf("&&&&&&&&&&&& average time in befft with dynamic error                        : %.10f\n", total_time_d/nbPols );
    printf("&&&&&&&&&&&& average max of errors                                                      : %.10e\n", total_abserror_d/nbPols );
    printf("&&&&&&&&&&&& average time in befft with dynamic error g                      : %.10f\n", total_time_g/nbPols );
    printf("&&&&&&&&&&&& average max of errors                                                      : %.10e\n", total_abserror_g/nbPols );
//     printf("&&&&&&&&&&&& average time in befft with dynamic error old                    : %.10f\n", total_time_o/nbPols );
//     printf("&&&&&&&&&&&& average max of errors                                                      : %.10e\n", total_abserror_o/nbPols );
#ifdef BEFFT_HAS_ARB
    printf("&&&&&&&&&&&& average time in acbfft                                          : %.10f\n", total_time_a/nbPols );
    printf("&&&&&&&&&&&& average max of errors                                                      : %.10e\n", total_abserror_a/nbPols );
#endif
    printf("&&&&&&&&&&&& underflow occured in                                              ");
    if (total_underflow > 0) SET_RED; else SET_GREEN; printf("%d", total_underflow); SET_BLACK; printf(" cases\n");
    printf("&&&&&&&&&&&& for each vector, all beffts have same centers                   : ");
    if (all_same_centers) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
    
    befft_fft_rad2_clear(rad2);
    
#ifdef BEFFT_HAS_ARB
    printf("&&&&&&&&&&&& for each vector, centers of beffts are in acbfft balls          : ");
    if (center_in_acb) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
    printf("&&&&&&&&&&&& for each vector, fft with static error intersects acbfft balls  : ");
    if (center_s_overlaps_acb) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
//     printf("&&&&&&&&&&&& for each vector, fft with static error v intersects acbfft balls: ");
//     if (center_sv_overlaps_acb) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
    printf("&&&&&&&&&&&& for each vector, fft with static error 2 intersects acbfft balls: ");
    if (center_e_overlaps_acb) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
    printf("&&&&&&&&&&&& for each vector, fft with dynamic error intersects acbfft balls : ");
    if (center_d_overlaps_acb) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
    printf("&&&&&&&&&&&& for each vector, fft with dynamic error intersects acbfft balls : ");
    if (center_g_overlaps_acb) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
//     printf("&&&&&&&&&&&& for each vector, fft with dynamic error o intersects acbfft balls : ");
//     if (center_o_overlaps_acb) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
    printf("&&&&&&&&&&&& average max of dists center to center                           :  %.5e\n", max_dists_centers_2_centers/nbPols);
    
    _acb_vec_clear(xacb, N);
    _acb_vec_clear(zacb, N);
    
    acb_dft_rad2_clear( dftrad2 );
    
    flint_cleanup();
    
#endif
    
#ifdef BEFFT_HAS_MPFR
    mpfr_free_cache ();
#endif
    
    befft_free(xre);
    befft_free(xim);
    befft_free(xab);
    befft_free(yre_s);
    befft_free(yim_s);
    befft_free(yab_s);
    befft_free(yre_e);
    befft_free(yim_e);
    befft_free(yab_e);
    befft_free(yre_d);
    befft_free(yim_d);
    befft_free(yab_d);
    befft_free(yre_g);
    befft_free(yim_g);
    befft_free(yab_g);
    befft_free(x);
    befft_free(y);
//     befft_free(yre_o);
//     befft_free(yim_o);
//     befft_free(yab_o);
    return 0;
}

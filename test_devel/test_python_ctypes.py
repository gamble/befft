#****************************************************************************
#        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
#                                Remi Imbach     <remi.imbach@laposte.net>
# 
#    This file is part of befft.
#
#    befft is free software: you can redistribute it and/or modify 
#    it under the terms of the GNU Lesser General Public License as 
#    published by the Free Software Foundation, either version 3 of 
#    the License, or (at your option) any later version.
#
#    befft is distributed in the hope that it will be useful, but 
#    WITHOUT ANY WARRANTY; without even the implied warranty of 
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#    See the GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public 
#    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
#*****************************************************************************

import numpy as np
import ctypes as ct
import math

# loading library
befft=ct.cdll.LoadLibrary("../lib/libbefft.so")
# accessing functions
befft.befft_fft_rad2_dynamic

#passing parameters by reference
x_mid = ct.c_double()
x_rad = ct.c_double()
y     = ct.c_longlong(2**53)
befft._be_real_set_si_aber( ct.byref(x_mid), ct.byref(x_rad), y )
print(x_mid) #should be 9007199254740992
print(x_rad) #should be 0
y     = ct.c_longlong(2**53+1)
befft._be_real_set_si_aber( ct.byref(x_mid), ct.byref(x_rad), y )
print(x_mid) #should be 9007199254740992
print(x_rad) #should be 1
y     = ct.c_longlong(2**53+2)
befft._be_real_set_si_aber( ct.byref(x_mid), ct.byref(x_rad), y )
print(x_mid) #should be 9007199254740994
print(x_rad) #should be 0.0

def be_set_si( n ):
    """ Construct a real ball containing integer n
    >>> [ be_set_si(n) for n in range(-3,3) ]
    [(-3.0, 0.0), (-2.0, 0.0), (-1.0, 0.0), (0.0, 0.0), (1.0, 0.0), (2.0, 0.0)]
    >>> [ be_set_si(n) for n in range(2**53-1,2**53+4) ]
    [(9007199254740991.0, 0.0), (9007199254740992.0, 0.0), (9007199254740992.0, 1.0), (9007199254740994.0, 0.0), (9007199254740996.0, 1.0)]"""
    if math.floor(n) != n:
        raise ValueError("n must be exact integer")
    x_mid = ct.c_double()
    x_rad = ct.c_double()
    y     = ct.c_longlong(n)
    befft._be_real_set_si_aber( ct.byref(x_mid), ct.byref(x_rad), y )
    return x_mid.value, x_rad.value

# Arrays
k = 2
K = 2**k
KdoublesArray = ct.c_double*K
Omegas_re = KdoublesArray()
Omegas_im = KdoublesArray()
Omegas_ae = ct.c_double()
print(Omegas_re)
befft._be_vec_omega_precomp.argtypes = ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.c_size_t
befft._be_vec_omega_precomp.restype = ct.c_int 
befft._be_vec_omega_precomp( Omegas_re, Omegas_im, ct.byref(Omegas_ae), ct.c_size_t(K) )

#numpy arrays
Onpre = np.ctypeslib.as_array(Omegas_re)
Onpim = np.ctypeslib.as_array(Omegas_im)
befft._be_vec_omega_precomp( Omegas_re, Omegas_im, ct.byref(Omegas_ae), ct.c_size_t(K) )
print(Onpre)
print(Onpim)

def be_vec_roots_of_unity( k ):
    """ Returns the 2^k 2^k-th order roots of unity """
    if math.floor(k) != k:
        raise ValueError("n must be exact integer")
    if (k>30):
        raise ValueError("k is too large")
    K = 2**k
    KdoublesArray = ct.c_double*K
    Omegas_re = KdoublesArray()
    Omegas_im = KdoublesArray()
    Omegas_ae = ct.c_double()
    befft._be_vec_omega_precomp( Omegas_re, Omegas_im, ct.byref(Omegas_ae), ct.c_size_t(K) )
    Omegas_mid = np.zeros(K, dtype=np.complex128)
    Omegas_rad = np.zeros(K, dtype=np.double)
    for i in range(0,K):
        Omegas_mid[i] = (Omegas_re[i]) + (Omegas_im[i])*1j
        Omegas_rad[i] = Omegas_ae.value
    return Omegas_mid, Omegas_rad

    

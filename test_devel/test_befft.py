#****************************************************************************
#        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
#                                Remi Imbach     <remi.imbach@laposte.net>
# 
#    This file is part of befft.
#
#    befft is free software: you can redistribute it and/or modify 
#    it under the terms of the GNU Lesser General Public License as 
#    published by the Free Software Foundation, either version 3 of 
#    the License, or (at your option) any later version.
#
#    befft is distributed in the hope that it will be useful, but 
#    WITHOUT ANY WARRANTY; without even the implied warranty of 
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#    See the GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public 
#    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
#*****************************************************************************

import numpy as np
import befft

k=4
K=2**k

p_mid = np.ones( K, dtype=np.complex128 )
p_rad = np.ones( K, dtype=np.float64 )
p_rad = (1e-10)*p_rad

y_mid, y_rad = befft.fft_rad2( p_mid, p_rad )
q_mid, q_rad = befft.fft_rad2( y_mid, y_rad )

print("k: ", k)
print("y_mid: ", y_mid)
print("y_rad: ", y_rad)
print("q_mid: ", q_mid)
print("q_rad: ", q_rad)
print("\n")

k=5
K=2**k

p_mid = np.ones( K, dtype=np.complex128 )
p_rad = np.ones( K, dtype=np.float64 )
p_rad = (1e-10)*p_rad

y_mid, y_rad = befft.fft_rad2( p_mid, p_rad )
q_mid, q_rad = befft.fft_rad2( y_mid, y_rad )

print("k: ", k)
print("y_mid: ", y_mid)
print("y_rad: ", y_rad)
print("q_mid: ", q_mid)
print("q_rad: ", q_rad)
print("\n")

# K=7
# p_mid = np.ones( K, dtype=np.complex128 )
# p_rad = np.ones( K, dtype=np.double )
# p_rad = (1e-10)*p_rad

# y_mid, y_rad = befft.fft_rad2( p_mid, p_rad )
# q_mid, q_rad = befft.fft_rad2( y_mid, y_rad )

k=12
K=2**k

p_mid = np.ones( K, dtype=np.complex128 )
p_rad = np.ones( K, dtype=np.float64 )
p_rad = (1e-10)*p_rad
y_mid, y_rad = befft.fft_rad2( p_mid, p_rad )
q_mid, q_rad = befft.fft_rad2( y_mid, y_rad )

print("k: ", k)
print("y_mid: ", y_mid)
print("y_rad: ", y_rad)
print("q_mid: ", q_mid)
print("q_rad: ", q_rad)
print("\n")

k=13
K=2**k

p_mid = np.ones( K, dtype=np.complex128 )
p_rad = np.ones( K, dtype=np.float64 )
p_rad = (1e-10)*p_rad
y_mid, y_rad = befft.fft_rad2( p_mid, p_rad )
q_mid, q_rad = befft.fft_rad2( y_mid, y_rad )

print("k: ", k)
print("y_mid: ", y_mid)
print("y_rad: ", y_rad)
print("q_mid: ", q_mid)
print("q_rad: ", q_rad)
print("\n")

#exec(open("test_devel/test_befft.py").read())

/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft_base.h"

void hexdump( char * d, size_t len ) {
	
	printf("0x");
	for (int i=(int)len-1; i>=0; i--)
		printf("%02X", *((char*)(d+i)) );	
}

int main( void ) {
    
	// double a = 4.75;
	// hexdump ( (char *)&a, sizeof(double) );
	// printf( "\n" );
	
    /* save exception flags and re-init exceptions */
    fexcept_t except_save;
    fegetexceptflag (&except_save, FE_ALL_EXCEPT);
    feclearexcept (FE_ALL_EXCEPT);
    
    /* save rounding mode and set double rounding mode to NEAREST */
    int rounding_save = fegetround();
    int ressetnearest = fesetround (FE_TONEAREST);
    if (ressetnearest!=0) {
        printf("setting rounding to nearest failed!!!\n");
    }
    
    int double_exception = befft_test_and_print_all_exception(1, "befft_base.c");
    printf("double exception so far : %d\n", double_exception );
    feclearexcept (FE_ALL_EXCEPT);
	printf("\n");
    
	double a = BEFFT_NEXT(0);
    printf("BEFFT_NEXT(0) : %e ", a );
	hexdump( (char*)&a , sizeof(double) ); printf("\n");
    double_exception = befft_test_and_print_all_exception(1, "befft_base.c");
    printf("double exception so far : %d\n", double_exception );
    feclearexcept (FE_ALL_EXCEPT);
	printf("\n");
    
	a = BEFFT_PREV(0);
    printf("BEFFT_PREV(0) : %e ", a );
	hexdump( (char*)&a , sizeof(double) ); printf("\n");
    double_exception = befft_test_and_print_all_exception(1, "befft_base.c");
    printf("double exception so far : %d\n", double_exception );
    feclearexcept (FE_ALL_EXCEPT);
    printf("\n");
	
	a = BEFFT_U;
    printf("BEFFT_U       : %e ", a );
	hexdump( (char*)&a , sizeof(double) ); printf("\n");
    double_exception = befft_test_and_print_all_exception(1, "befft_base.c");
    printf("double exception so far : %d\n", double_exception );
    feclearexcept (FE_ALL_EXCEPT);
    printf("\n");
	
	a = BEFFT_A;
    printf("BEFFT_A       : %e ", a );
	hexdump( (char*)&a , sizeof(double) ); printf("\n");
    double_exception = befft_test_and_print_all_exception(1, "befft_base.c");
    printf("double exception so far : %d\n", double_exception );
    feclearexcept (FE_ALL_EXCEPT);
    printf("\n");
	
	a = BEFFT_A/2;
    printf("BEFFT_A/2       : %e ", a );
	hexdump( (char*)&a , sizeof(double) ); printf("\n");
    double_exception = befft_test_and_print_all_exception(1, "befft_base.c");
    printf("double exception so far : %d\n", double_exception );
    feclearexcept (FE_ALL_EXCEPT);
    printf("\n");
	
	printf( "sizeof char    : %zu\n", sizeof(char) );
	printf( "sizeof char *  : %zu\n", sizeof(char*) );
    printf( "sizeof uint    : %zu\n", sizeof(uint) );
    printf( "sizeof size_t  : %zu\n", sizeof(size_t) );
    printf( "sizeof besize_t: %zu\n", sizeof(be_size_t) );
    printf( "sizeof ulong   : %zu\n", sizeof(ulong) );
    printf( "sizeof slong   : %zu\n", sizeof(slong) );
    printf( "sizeof void *  : %zu\n", sizeof(void*) );
    
    /* restore exception flags and rounding mode */
    fesetexceptflag (&except_save, FE_ALL_EXCEPT);
    fesetround (rounding_save);
    
    return 0;
}

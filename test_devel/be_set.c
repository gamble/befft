/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"

int main( void ) {
    
    double dest_re, dest_aber;
    
    printf("&&&&&&&&&&&&&&& slong 0 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, 0 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    
    printf("&&&&&&&&&&&&&&& slong 2^53 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, (slong)0x1 << 53 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    printf("&&&&&&&&&&&&&&& slong 2^53+1 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 53)+1 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    printf("&&&&&&&&&&&&&&& slong 2^53+2 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 53)+2 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    
    printf("&&&&&&&&&&&&&&& slong 2^54 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 54) );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    printf("&&&&&&&&&&&&&&& slong 2^54+1 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 54)+1 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    printf("&&&&&&&&&&&&&&& slong 2^54+2 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 54)+2 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    printf("&&&&&&&&&&&&&&& slong 2^54+3 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 54)+3 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    printf("&&&&&&&&&&&&&&& slong 2^54+4 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 54)+4 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    
    printf("&&&&&&&&&&&&&&& slong 2^55 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 55) );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    printf("&&&&&&&&&&&&&&& slong 2^55+1 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 55)+1 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    printf("&&&&&&&&&&&&&&& slong 2^55+8 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 55)+8 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    printf("&&&&&&&&&&&&&&& slong 2^55+16 converted in double with error &&&&&&&&&&&&&&&&&&&\n");
    _be_real_set_si_aber( &dest_re, &dest_aber, ((slong)0x1 << 55)+16 );
    printf( "dest_re: %.16f, dest_aber %.16E\n", dest_re, dest_aber);
    
    
    return 0;
}

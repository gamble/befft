#****************************************************************************
#        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
#                                Remi Imbach     <remi.imbach@laposte.net>
# 
#    This file is part of befft.
#
#    befft is free software: you can redistribute it and/or modify 
#    it under the terms of the GNU Lesser General Public License as 
#    published by the Free Software Foundation, either version 3 of 
#    the License, or (at your option) any later version.
#
#    befft is distributed in the hope that it will be useful, but 
#    WITHOUT ANY WARRANTY; without even the implied warranty of 
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#    See the GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public 
#    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
#*****************************************************************************

import numpy as np
import ctypes as ct
import math

# loading library
befft=ct.cdll.LoadLibrary("../lib/libbefft.so")
fft_rad2_dynamic_signature = [ ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.POINTER(ct.c_double),
                               ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.c_uint ]
befft.befft_fft_rad2_dynamic.argtypes = fft_rad2_dynamic_signature
befft.befft_fft_rad2_dynamic.restype  = ct.c_int 

def befft_fft_rad2( p_mid, p_rad ):
    if (p_mid.shape[0] != p_rad.shape[0]):
        raise ValueError("sizes of input vectors do not match")
    k = math.log2(p_mid.shape[0])
    if (math.floor(k) != k) or (k < 0):
        raise ValueError("sizes of input vectors must be a positive power of 2")
    k=int(k)
    p_re = np.ctypeslib.as_ctypes(np.array(p_mid.real))
    p_im = np.ctypeslib.as_ctypes(np.array(p_mid.imag))
    p_ae = np.ctypeslib.as_ctypes(np.array(p_rad))
    
    KdoublesArray = ct.c_double*K
    y_re = KdoublesArray()
    y_im = KdoublesArray()
    y_ae = KdoublesArray()
    
    befft.befft_fft_rad2_dynamic( y_re, y_im, y_ae, p_re, p_im, p_ae, ct.c_uint(k) )
    
    y_mid = np.zeros(K, dtype=np.complex128)
    y_rad = np.zeros(K, dtype=np.double)
    for i in range(0,K):
        y_mid[i] = (y_re[i]) + (y_im[i])*1j
        y_rad[i] = y_ae[i]
    return y_mid, y_rad
    
k=4
K=2**k

p_mid = np.ones( K, dtype=np.complex128 )
p_rad = np.ones( K, dtype=np.double )
p_rad = (1e-10)*p_rad
#math.log2(p_mid.shape[0])
#p_mid.real
#p_re = np.ctypeslib.as_ctypes(np.array(p_mid.real))

y_mid, y_rad = befft_fft_rad2( p_mid, p_rad )
q_mid, q_rad = befft_fft_rad2( y_mid, y_rad )

y = np.fft.fft(p_mid)
q = np.fft.fft(y)

#exec(open("test_call_befft.py").read())

/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft_base.h"
#include <fenv.h>
#include <float.h>

int main( void ) {
    
    feclearexcept (FE_ALL_EXCEPT);
    
    printf("FE_UNDERFLOW: %d\n", fetestexcept(FE_UNDERFLOW));
    
    double inf = ldexp(0x1, BEFFT_EMIN);
    printf("inf : %.16e\n", inf );
    printf("FE_UNDERFLOW after definition of 2^(%d): %d\n", BEFFT_EMIN, fetestexcept(FE_UNDERFLOW));
    if ( fpclassify(inf) == FP_SUBNORMAL) printf("subnormal number!\n");
    inf = inf/2;
    printf("inf/2 : %.16e\n", inf );
    printf("FE_UNDERFLOW after divison by 2: %d\n", fetestexcept(FE_UNDERFLOW));
    if ( fpclassify(inf) == FP_SUBNORMAL) printf("subnormal number!\n");
    
    feclearexcept (FE_ALL_EXCEPT);
    double inf2 = DBL_MIN;
    printf("inf2 : %.16e\n", inf2 );
    printf("FE_UNDERFLOW after definition of DBL_MIN: %d\n", fetestexcept(FE_UNDERFLOW));
    if ( fpclassify(inf) == FP_SUBNORMAL) printf("subnormal number!\n");
    inf2 = inf2/2;
    printf("inf2/2 : %.16e\n", inf2 );
    printf("FE_UNDERFLOW after divison by 2: %d\n", fetestexcept(FE_UNDERFLOW));
    if ( fpclassify(inf) == FP_SUBNORMAL) printf("subnormal number!\n");
    
//     feclearexcept (FE_ALL_EXCEPT);
//     double inf3 = DBL_TRUE_MIN;
//     printf("inf3 : %.16e\n", inf3 );
//     printf("FE_UNDERFLOW after definition of DBL_TRUE_MIN: %d\n", fetestexcept(FE_UNDERFLOW));
//     if ( fpclassify(inf) == FP_SUBNORMAL) printf("subnormal number!\n");
//     inf3 = inf3/2;
//     printf("inf3/2 : %.16e\n", inf3 );
//     printf("FE_UNDERFLOW after divison by 2: %d\n", fetestexcept(FE_UNDERFLOW));
//     if ( fpclassify(inf) == FP_SUBNORMAL) printf("subnormal number!\n");
    
    if ( fpclassify(0.) == FP_SUBNORMAL) printf("zero is subnormal number!\n");
//     double inf2 = inf*inf;
//     printf("inf2 : %.16e\n", inf2 );
//     printf("inf2 is zero: %d\n", ( fpclassify(inf2) == FP_ZERO) );
//     printf("FE_UNDERFLOW after squaring: %d\n", fetestexcept(FE_UNDERFLOW));
//     if ( fpclassify(inf2) == FP_SUBNORMAL) printf("subnormal number!\n");
    
    
    printf("FE_UNDERFLOW at the end: %d\n", fetestexcept(FE_UNDERFLOW));
    
}

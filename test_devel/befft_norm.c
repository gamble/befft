/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"
#include <float.h>

#ifdef BEFFT_HAS_ARB
#include "acb_dft.h"
#include "flint/flint.h"
#include "arf.h"
#endif

#define BLACK "\e[0m"
#define SET_BLACK printf(BLACK)
#define GREEN "\e[1;32m"
#define SET_GREEN printf(GREEN)
#define RED "\e[1;31m"
#define SET_RED printf(RED)

void print_table( double * xre, double * xim, ulong size ){
    printf("[ \n");
    for (ulong j=0; j<size; j++) {
        printf("  <%.5e + I%.5e>, \n", xre[j], xim[j]);
    }
    printf("] \n");
}

#ifdef BEFFT_HAS_ARB
void print_acb_vec( acb_ptr v, ulong size ){
    printf("[ \n");
    for (ulong j=0; j<size; j++) {
        acb_printd(v+j, 5); printf("\n");
    }
    printf("] \n");
}

int _be_center_is_in_acb( const double yre, const double yim, acb_t z ){
    arf_t re,im;
    arf_init(re);
    arf_init(im);
    
    arf_set_d(re, yre);
    arf_set_d(im, yim);
    int res = arb_contains_arf( acb_realref(z), re ) && arb_contains_arf( acb_imagref(z), im );
    
    arf_clear(re);
    arf_clear(im);
    
    return res;
}

int _be_overlaps_acb( const double yre, const double yim, const double yab, acb_t z ){
    acb_t y;
    acb_init(y);
    
    arf_set_d( arb_midref(acb_realref(y)), yre);
    arf_set_d( arb_midref(acb_imagref(y)), yim);
    mag_set_d( arb_radref(acb_realref(y)), yab);
    mag_set_d( arb_radref(acb_imagref(y)), yab);
//     printf("y: "); acb_printd(y,10);printf("\n");
//     printf("z: "); acb_printd(z,10);printf("\n");
    int res = acb_overlaps(y,z);
//     printf("res: %d\n", res);
    
    acb_clear(y);
    
    return res;
}

int _be_vec_overlaps_acb_vec( double * yre, double * yim, const double yab, ulong size, acb_ptr z ){
    for (ulong j=0; j<size; j++) {
        if ( _be_overlaps_acb(yre[j],yim[j],yab,z+j)==0 )
            return 0;
    }
    return 1;
}

int _be_vec_aber_overlaps_acb_vec( double * yre, double * yim, double * yab, ulong size, acb_ptr z ){
    for (ulong j=0; j<size; j++) {
        if ( _be_overlaps_acb(yre[j],yim[j],yab[j],z+j)==0 )
            return 0;
    }
    return 1;
}

int _be_vec_center_is_in_acb_vec( double * yre, double * yim, ulong size, acb_ptr z ){
    for (ulong j=0; j<size; j++) {
        if ( _be_center_is_in_acb( yre[j], yim[j], z+j )==0 )
            return 0;
    }
    return 1;
}

int _be_vec_centers_equal( double * yre, double * yim, double * xre, double * xim, ulong size){
    for (ulong j=0; j<size; j++) {
        if ( (yre[j]!=xre[j])||(yim[j]!=xim[j]) )
            return 0;
    }
    return 1;
}

void _be_center_max_dist_to_acb( arf_t dest, const double yre, const double yim, acb_t z ){
    arf_t re,im;
    arf_init(re);
    arf_init(im);
    
    arf_set_d(re, yre);
    arf_set_d(im, yim);
    arf_sub(re, re, arb_midref(acb_realref(z)), BEFFT_PREC, ARF_RND_UP );
    arf_sub(im, im, arb_midref(acb_imagref(z)), BEFFT_PREC, ARF_RND_UP  );
    arf_abs(re, re );
    arf_abs(im, im );
    arf_max(dest, re, im);
    
    arf_clear(re);
    arf_clear(im);
}

double _be_vec_center_max_dist_to_acb_vec( double * yre, double * yim, ulong size, acb_ptr z ){
    arf_t dist, temp;
    arf_init(dist);
    arf_init(temp);
    _be_center_max_dist_to_acb( dist, yre[0], yim[0], z+0 );
    for (ulong j=1; j<size; j++) {
        _be_center_max_dist_to_acb( temp, yre[j], yim[j], z+j );
        arf_max(dist, dist, temp);
    }
    double res = arf_get_d( dist, ARF_RND_UP );
    arf_clear(dist);
    arf_clear(temp);
    return res;
}

double _acb_vec_max_error( acb_ptr z, ulong size ) {
    mag_t max;
    mag_init(max);
    mag_max(max, arb_radref( acb_realref(z+0) ), arb_radref( acb_imagref(z+0) ) );
    for (ulong j=1; j<size; j++) {
        mag_max(max, max, arb_radref( acb_realref(z+j) ) );
        mag_max(max, max, arb_radref( acb_imagref(z+j) ) );
    }
    double res = mag_get_d(max);
    mag_clear(max);
    return res;
}

void acb_vec_twonorm( arb_t res, acb_ptr z, ulong size, slong prec ) {
    arb_zero(res);
    for (ulong j=0; j<size; j++) {
        arb_addmul(res, acb_realref(z+j), acb_realref(z+j), prec);
        arb_addmul(res, acb_imagref(z+j), acb_imagref(z+j), prec);
    }
    arb_sqrt(res, res, prec);
}
#endif

void generate_random_vector( double * xre, double * xim, double * xab, double * xab_max, int coeff_error, ulong size ){
    *xab_max = 0;
    double coeff_err = pow(10, coeff_error);
    for (ulong j=0; j<size; j++) {
//         xre[j] = DBL_MIN*((double) rand())/((double) rand());
        xre[j] = ((double) rand())/((double) rand());
        xim[j] = ((double) rand())/((double) rand());
//         xab[j] = 0.;
        xab[j] = coeff_err*((double) rand())/RAND_MAX;
        *xab_max = BEFFT_MAX(*xab_max, xab[j]);
    }
}

double _be_vec_twonorm_ub_naive( double * _real, double * _imag, ulong size ){
    double res = 0;
    double re, im;
    for (ulong j = 0; j<size; j++) {
        re = BEFFT_NEXT(_real[j]*_real[j]);
        im = BEFFT_NEXT(_imag[j]*_imag[j]);
        res = BEFFT_NEXT( res + re );
        res = BEFFT_NEXT( res + im );
    }
    res = BEFFT_NEXT( sqrt(res) );
    return res;
}

double _be_vec_twonorm_ub_fast( double * _real, double * _imag, ulong size ){
    double res = _be_twonorm_ub_fast( _real[0], _imag[0] );
    for (ulong j = 1; j<size; j++)
        res = BEFFT_MAX( res, _be_twonorm_ub_fast( _real[j], _imag[j] ) );
    double restemp = BEFFT_NEXT( sqrt(size) );
    res = BEFFT_NEXT( restemp*res );
    return res;
}

int main(int argc, char* argv[]) {
    
    uint nbCalls = 100;
    uint n = 4;
    uint nbPols = 1;
    int  c = -5;
    int  verbosity=1;
    
    if (argc >= 2)
        sscanf(argv[1], "%u", &n);
    
    if (argc >= 3)
        sscanf(argv[2], "%u", &nbPols);
    
    if (argc >= 4)
        sscanf(argv[3], "%u", &nbCalls);
    
    if (argc >= 5)
        sscanf(argv[4], "%d", &c);
    
    if (argc >= 6)
        sscanf(argv[5], "%d", &verbosity);
    
    printf("test fft for %u random vectors with log2 of size %u, and max of componentwise error %.1e\n", nbPols, n, pow(10,c));
    
    ulong N = ((ulong) 0x1)<<n;
    srand (0);
    
    double * xre   = (double *) befft_malloc (N*sizeof(double));
    double * xim   = (double *) befft_malloc (N*sizeof(double));
    double * xab   = (double *) befft_malloc (N*sizeof(double));
    double xab_max;
    
    acb_ptr zacb = _acb_vec_init(N);
    acb_ptr zacbE = _acb_vec_init(N);
    arb_t av_twonorm, twonorm, twonorm_ub;
    arb_t av_twonormE, twonormE, twonorm_ubE;
    arb_init(twonorm);
    arb_init(twonorm_ub);
    arb_init(av_twonorm);
    arb_init(twonormE);
    arb_init(twonorm_ubE);
    arb_init(av_twonormE);
    arb_zero(av_twonorm);
    
    double total_time_ub_fast = 0.;
    double total_time_ub_slow = 0.;
    double total_time_ub_slow2 = 0.;
    int    total_underflow = 0;
    int    total_underflowE = 0;
    int    total_ub_ge_acb = 1;
    int    total_ub_ge_acbE = 1;
    
    double total_time_acb = 0.;
    
    double av_twonorm_ub_fast = 0.;
    double av_twonorm_ub_slow = 0.;
    double av_twonorm_ub_slow2 = 0.;
    double av_twonorm_ub_fastE = 0.;
    double av_twonorm_ub_slowE = 0.;
    double av_twonorm_ub_slow2E = 0.;
    
    uint j;
    
    for (uint i = 0; i<nbPols; i++) {
        
        if (verbosity)
            printf("&&& vec %u\n", i);
    
        generate_random_vector( xre, xim, xab, &xab_max, c, N );
        for (ulong j=0; j<N; j++){
            acb_set_d_d(zacb+j, xre[j], xim[j]);
//             mag_set_d( arb_radref( acb_realref(zacb+j) ), xab[j] );
//             mag_set_d( arb_radref( acb_imagref(zacb+j) ), xab[j] );
            acb_set_d_d(zacbE+j, xab[j], xab[j]);
        }
        
        clock_t start_a = clock();
        for (j = 0; j<nbCalls; j++) {
            acb_vec_twonorm(twonorm, zacb, N, BEFFT_PREC);
            acb_vec_twonorm(twonormE, zacbE, N, BEFFT_PREC);
        }
        double time_acb     = (double) (clock() - start_a)/CLOCKS_PER_SEC;
        total_time_acb+=(time_acb/j);
        if (verbosity) {
            printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in acb_twonorm      : %.10e\n", time_acb/j );
            printf("      twonorm                                           : "); arb_printd(twonorm, 10); printf("\n");
            printf("      twonormE                                          : "); arb_printd(twonormE, 10); printf("\n");
        }
        arb_add(av_twonorm, av_twonorm, twonorm, BEFFT_PREC);
        arb_add(av_twonormE, av_twonormE, twonormE, BEFFT_PREC);
    
        int underflow = 0;
        feclearexcept (FE_ALL_EXCEPT);
        double twonorm_ub_fast, twonorm_ub_fastE;
        
        clock_t start_f = clock();
        for (j = 0; j<nbCalls; j++) {
            twonorm_ub_fast  = _be_vec_twonorm_ub_fast(xre, xim, N);
        }
        double time_ub_fast     = (double) (clock() - start_f)/CLOCKS_PER_SEC;
        underflow = !!fetestexcept(FE_UNDERFLOW);
        total_time_ub_fast+=(time_ub_fast/j);
        if (underflow)
            total_underflow+=1;
        arb_set_d(twonorm_ub, twonorm_ub_fast);
        int ub_ge_acb = arb_ge(twonorm_ub, twonorm);
        total_ub_ge_acb = total_ub_ge_acb&&ub_ge_acb;
        av_twonorm_ub_fast+=twonorm_ub_fast;
        
        int underflowE = 0;
        feclearexcept (FE_ALL_EXCEPT);
        clock_t start_fE = clock();
        for (j = 0; j<nbCalls; j++) {
            twonorm_ub_fastE = _be_vec_twonorm_ub_fast(xab, xab, N);
        }
        double time_ub_fastE     = (double) (clock() - start_fE)/CLOCKS_PER_SEC;
        underflowE = !!fetestexcept(FE_UNDERFLOW);
        total_time_ub_fast+=(time_ub_fastE/j);
        if (underflowE)
            total_underflowE+=1;
        arb_set_d(twonorm_ubE, twonorm_ub_fastE);
        int ub_ge_acbE = arb_ge(twonorm_ubE, twonormE);
        total_ub_ge_acbE = total_ub_ge_acbE&&ub_ge_acbE;
        av_twonorm_ub_fastE+=twonorm_ub_fastE;
        
        if (verbosity) {
            printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in twonorm_ub_fast  : %.10e\n", time_ub_fast/j + time_ub_fastE/j);
            printf("      underflow occured?                               : %d\n", underflow );
            printf("      upper bound to twonorm                           : %.10e\n", twonorm_ub_fast );
            printf("      upper bound to twonorm >= twonorm                : %d   \n", ub_ge_acb );
            printf("      underflowE occured?                              : %d\n", underflowE );
            printf("      upper bound to twonormE                          : %.10e\n", twonorm_ub_fastE );
            printf("      upper bound to twonormE >= twonormE              : %d   \n", ub_ge_acbE );
        }
        
        underflow = 0;
        feclearexcept (FE_ALL_EXCEPT);
        double twonorm_ub_slow, twonorm_ub_slowE;
        
        clock_t start_s = clock();
        for (j = 0; j<nbCalls; j++) {
            twonorm_ub_slow  = _be_vec_twonorm_ub_naive(xre, xim, N);
        }
        double time_ub_slow     = (double) (clock() - start_s)/CLOCKS_PER_SEC;
        underflow = !!fetestexcept(FE_UNDERFLOW);
        total_time_ub_slow+=(time_ub_slow/j);
        if (underflow)
            total_underflow+=1;
        arb_set_d(twonorm_ub, twonorm_ub_slow);
        ub_ge_acb = arb_ge(twonorm_ub, twonorm);
        total_ub_ge_acb = total_ub_ge_acb&&ub_ge_acb;
        av_twonorm_ub_slow+=twonorm_ub_slow;
        
        underflowE = 0;
        feclearexcept (FE_ALL_EXCEPT);
        clock_t start_sE = clock();
        for (j = 0; j<nbCalls; j++) {
            twonorm_ub_slowE = _be_vec_twonorm_ub_naive(xab, xab, N);
        }
        double time_ub_slowE     = (double) (clock() - start_sE)/CLOCKS_PER_SEC;
        underflowE = !!fetestexcept(FE_UNDERFLOW);
        total_time_ub_slow+=(time_ub_slowE/j);
        if (underflowE)
            total_underflowE+=1;
        arb_set_d(twonorm_ubE, twonorm_ub_slowE);
        ub_ge_acbE = arb_ge(twonorm_ubE, twonormE);
        total_ub_ge_acbE = total_ub_ge_acbE&&ub_ge_acbE;
        av_twonorm_ub_slowE+=twonorm_ub_slowE;
        
        if (verbosity) {
            printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in twonorm_ub_naive  : %.10e\n", time_ub_slow/j + time_ub_slowE/j );
            printf("      underflow occured?                               : %d\n", underflow );
            printf("      upper bound to twonorm                           : %.10e\n", twonorm_ub_slow );
            printf("      upper bound to twonorm >= twonorm                : %d   \n", ub_ge_acb );
            printf("      underflowE occured?                              : %d\n", underflowE );
            printf("      upper bound to twonormE                          : %.10e\n", twonorm_ub_slowE );
            printf("      upper bound to twonormE >= twonormE              : %d   \n", ub_ge_acbE );
        }
        
        underflow = 0;
        feclearexcept (FE_ALL_EXCEPT);
        double twonorm_ub_slow2, twonorm_ub_slow2E;
        
        clock_t start_s2 = clock();
        for (j = 0; j<nbCalls; j++) {
            twonorm_ub_slow2  = _be_vec_twonorm_ub(xre, xim, N);
        }
        double time_ub_slow2     = (double) (clock() - start_s2)/CLOCKS_PER_SEC;
        underflow = !!fetestexcept(FE_UNDERFLOW);
        total_time_ub_slow2+=(time_ub_slow2/j);
        if (underflow)
            total_underflow+=1;
        arb_set_d(twonorm_ub, twonorm_ub_slow2);
        ub_ge_acb = arb_ge(twonorm_ub, twonorm);
        total_ub_ge_acb = total_ub_ge_acb&&ub_ge_acb;
        av_twonorm_ub_slow2+=twonorm_ub_slow2;
        
        underflowE = 0;
        feclearexcept (FE_ALL_EXCEPT);
        clock_t start_s2E = clock();
        for (j = 0; j<nbCalls; j++) {
            twonorm_ub_slow2E = _be_vec_twonorm_ub(xab, xab, N);
        }
        double time_ub_slow2E     = (double) (clock() - start_s2E)/CLOCKS_PER_SEC;
        underflowE = !!fetestexcept(FE_UNDERFLOW);
        total_time_ub_slow2+=(time_ub_slow2E/j);
        if (underflowE)
            total_underflowE+=1;
        arb_set_d(twonorm_ubE, twonorm_ub_slow2E);
        ub_ge_acbE = arb_ge(twonorm_ubE, twonormE);
        total_ub_ge_acbE = total_ub_ge_acbE&&ub_ge_acbE;
        av_twonorm_ub_slow2E+=twonorm_ub_slow2E;
        
        if (verbosity) {
            printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in twonorm_ub       : %.10e\n", time_ub_slow2/j + time_ub_slow2E/j );
            printf("      underflow occured?                               : %d\n", underflow );
            printf("      upper bound to twonorm                           : %.10e\n", twonorm_ub_slow2 );
            printf("      upper bound to twonorm >= twonorm                : %d   \n", ub_ge_acb );
            printf("      underflowE occured?                              : %d\n", underflowE );
            printf("      upper bound to twonormE                          : %.10e\n", twonorm_ub_slow2E );
            printf("      upper bound to twonormE >= twonormE              : %d   \n", ub_ge_acbE );
        }
        
    }
    
    arb_div_ui(av_twonorm, av_twonorm, nbPols, BEFFT_PREC);
    arb_div_ui(av_twonormE, av_twonormE, nbPols, BEFFT_PREC);
    
    printf("&&&&&&&&&&&& average time in acb                                             : %.10e\n", total_time_acb/nbPols );
    printf("             average twonorm                                 : "); arb_printd(av_twonorm, 10); printf("\n");
    printf("             average twonormE                                : "); arb_printd(av_twonormE, 10); printf("\n");
    printf("&&&&&&&&&&&& average time in twonorm_ub_fast                                 : %.10e\n", total_time_ub_fast/nbPols );
    printf("             average upper bound to twonorm                  : %.10e\n", av_twonorm_ub_fast/nbPols );
    printf("             average upper bound to twonormE                 : %.10e\n", av_twonorm_ub_fastE/nbPols );
    printf("&&&&&&&&&&&& average time in twonorm_ub_naive                                : %.10e\n", total_time_ub_slow/nbPols );
    printf("             average upper bound to twonorm                  : %.10e\n", av_twonorm_ub_slow/nbPols );
    printf("             average upper bound to twonormE                 : %.10e\n", av_twonorm_ub_slowE/nbPols );
    printf("&&&&&&&&&&&& average time in twonorm_ub                                      : %.10e\n", total_time_ub_slow2/nbPols );
    printf("             average upper bound to twonorm                  : %.10e\n", av_twonorm_ub_slow2/nbPols );
    printf("             average upper bound to twonormE                 : %.10e\n", av_twonorm_ub_slow2E/nbPols );
    printf("&&&&&&&&&&&& underflow occured in                                              ");
    if (total_underflow > 0) SET_RED; else SET_GREEN; printf("%d", total_underflow); SET_BLACK; printf(" cases\n");
    printf("&&&&&&&&&&&& underflowE occured in                                             ");
    if (total_underflowE > 0) SET_RED; else SET_GREEN; printf("%d", total_underflowE); SET_BLACK; printf(" cases\n");
    printf("&&&&&&&&&&&& for each vector, upper bound to twonorm >= twonorm              : ");
    if (total_ub_ge_acb) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
    printf("&&&&&&&&&&&& for each vector, upper bound to twonormE >= twonormE            : ");
    if (total_ub_ge_acbE) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
    
    _acb_vec_clear(zacb, N);
    arb_clear(twonorm);
    arb_clear(twonorm_ub);
    arb_clear(av_twonorm);
    
    _acb_vec_clear(zacbE, N);
    arb_clear(twonormE);
    arb_clear(twonorm_ubE);
    arb_clear(av_twonormE);
    
    flint_cleanup();
    
    befft_free(xre);
    befft_free(xim);
    befft_free(xab);
    return 0;
}

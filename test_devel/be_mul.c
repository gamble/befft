/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"

int main( void ) {
    
    be_t dest, srca, srcb;
    be_init(dest);
    be_init(srca);
    be_init(srcb);
    
    printf("&&&&&&&&&&&&&&& mul 0 and 0 with no error computation &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_exact(srca, 0, 0);
    be_set_double_exact(srcb, 0, 0);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    be_mul(dest, srca, srcb);
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 0 and 0 with    error computation &&&&&&&&&&&&&&&&&&&\n");
    be_mul_error(dest, srca, srcb);
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 0 and 0 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, 0, 0, .1);
    be_mul_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 0 and 1+i both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, 1, 1, .1);
    be_mul_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 0 and .5 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, .5, 0, .1);
    be_mul_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 1 and .5 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 0, .1);
    be_set_double_error(srcb, .5, 0, .1);
    be_mul_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 1+i and .5(1+i) both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 1., .1);
    be_set_double_error(srcb, .5, .5, .1);
    be_mul_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    
    printf("&&&&&&&&&&&&&&& mul 1+i and .5 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 1., .1);
    be_set_double_error(srcb, .5, 0, .1);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    _be_mul_be_real_aber( &(dest->_real), &(dest->_imag), &(dest->_aber),
                          srca->_real,    srca->_imag,    srca->_aber,
                          srcb->_real,                    srcb->_aber );
    printf("dest with real mult: "); be_printd (dest, 20); printf("\n");
    be_mul_error(dest, srca, srcb);
    printf("dest with comp mult: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& mul 1+i with abs error .1 and .5 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 1., .1);
    be_set_double_error(srcb, .5, 0, 0);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    _be_mul_be_real_aber( &(dest->_real), &(dest->_imag), &(dest->_aber),
                          srca->_real,    srca->_imag,    srca->_aber,
                          srcb->_real,                    srcb->_aber );
    printf("dest with real mult: "); be_printd (dest, 20); printf("\n");
    be_mul_error(dest, srca, srcb);
    printf("dest with comp mult: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& mul .5 with abs error .1 and .25 with abs error .01 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, .5, .0, .1);
    be_set_double_error(srcb, .25, .0, .01);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    _be_real_mul_aber( &(dest->_real), &(dest->_aber),
                       srca->_real,    srca->_aber,
                       srcb->_real,    srcb->_aber );
    dest->_imag = 0.;
    printf("dest with real mult: "); be_printd (dest, 20); printf("\n");
    be_mul_error(dest, srca, srcb);
    printf("dest with comp mult: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& mul .5 with abs error .1 and slong 2 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, .5, .0, .1);
    be_set_double_error(srcb, 2, .0, 0.);
    printf("srca: "); be_print (srca); printf("\n");
//     printf("srcb: "); be_print (srcb); printf("\n");
    _be_real_mul_si_aber( &(dest->_real), &(dest->_aber),
                          srca->_real,    srca->_aber,
                          (slong)2 );
    dest->_imag = 0.;
    printf("dest with real mult: "); be_printd (dest, 20); printf("\n");
    _be_real_mul_aber( &(dest->_real), &(dest->_aber),
                       srca->_real,    srca->_aber,
                       srcb->_real,    srcb->_aber  );
    printf("dest with no error: "); be_printd (dest, 20); printf("\n");
    
    be_clear(dest);
    be_clear(srca);
    be_clear(srcb);
    
    return 0;
}

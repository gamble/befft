/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"

int main( void ) {
    
    printf("&&&&&&&&&&&&&&& (1+I0) +/-0   contains 0: %d\n", _be_contains_zero( 1, 0, 0 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 1, 0, 0 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (1+I0) +/-1/2 contains 0: %d\n", _be_contains_zero( 1, 0, 1/2 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 1, 0, 1/2 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (1+I0) +/-1   contains 0: %d\n", _be_contains_zero( 1, 0, 1 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 1, 0, 1 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (1+I0) +/-2   contains 0: %d\n", _be_contains_zero( 1, 0, 2 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 1, 0, 2 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (0+I1) +/-0   contains 0: %d\n", _be_contains_zero( 0, 1, 0 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 0, 1, 0 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (0+I1) +/-1/2 contains 0: %d\n", _be_contains_zero( 0, 1, 1/2 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 0, 1, 1/2 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (0+I1) +/-1   contains 0: %d\n", _be_contains_zero( 0, 1, 1 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 0, 1, 1 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (0+I1) +/-2   contains 0: %d\n", _be_contains_zero( 0, 1, 2 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 0, 1, 2 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (0+I0) +/-0   contains 0: %d\n", _be_contains_zero( 0, 0, 0 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 0, 0, 0 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (0+I0) +/-1/2 contains 0: %d\n", _be_contains_zero( 0, 0, 1/2 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 0, 0, 1/2 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (1+I1) +/-0   contains 0: %d\n", _be_contains_zero( 1, 1, 0 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 1, 1, 0 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (1+I1) +/-1/2 contains 0: %d\n", _be_contains_zero( 1, 1, 1/2 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 1, 1, 1/2 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (1+I1) +/-1   contains 0: %d\n", _be_contains_zero( 1, 1, 1 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 1, 1, 1 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (1+I1) +/-2   contains 0: %d\n", _be_contains_zero( 1, 1, 2 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 1, 1, 2 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (1+I1) +/-1.1 contains 0: %d\n", _be_contains_zero( 1, 1, 1.1 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 1, 1, 1.1 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (1+I1) +/-1.5 contains 0: %d\n", _be_contains_zero( 1, 1, 1.5 ));
    printf("                                is non 0: %d\n", _be_is_non_zero( 1, 1, 1.5 ));
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& (1+I1) +/-sqrt(2) contains 0: %d\n", _be_contains_zero( 1, 1, sqrt(2) ));
    printf("                                    is non 0: %d\n", _be_is_non_zero( 1, 1, sqrt(2) ));
    printf("\n");
    
    return 0;
}

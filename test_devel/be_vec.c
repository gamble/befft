/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be_vec.h"

int main( void ) {
    
    uint k=3;
    be_size_t K = ( (be_size_t) 0x1 ) << k;
    be_vec_t v;
    be_vec_init(v, K);
    be_vec_omega_uint(v, k);
    
    printf("%u %zu-th order roots of unit: \n", k, K);
    be_vec_print(v);
    printf("\n");
    
    be_vec_clear(v);
    
    return 0;
}

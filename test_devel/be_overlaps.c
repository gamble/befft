/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"

int main( void ) {
    
    printf("&&&&&&&&&&&&&&& 0 and 0 with no error &&&&&&&&&&&&&&&&&&&\n");
    printf(" overlaps real: %d\n", _be_real_overlaps ( 0., 0., 0., 0. ) );
    printf("\n");
    printf("&&&&&&&&&&&&&&& 0 and 1 with no error &&&&&&&&&&&&&&&&&&&\n");
    printf(" overlaps real: %d\n", _be_real_overlaps ( 0., 0., 1., 0. ) );
    printf("\n");
    printf("&&&&&&&&&&&&&&& 0 and .2 with error .1 &&&&&&&&&&&&&&&&&&&\n");
    printf(" overlaps real: %d\n", _be_real_overlaps ( 0., .1, .2, .1 ) );
    printf("\n");
    
    be_t srca, srcb;
    be_init(srca);
    be_init(srcb);
    
    printf("&&&&&&&&&&&&&&& 0 and 0 with no error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .0);
    be_set_double_error(srcb, 0, 0, .0);
    printf(" overlaps: %d\n", be_overlaps ( srca, srcb ) );
    printf("\n");
    printf("&&&&&&&&&&&&&&& 0 and 1+I with no error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .0);
    be_set_double_error(srcb, 1, 1, .0);
    printf(" overlaps: %d\n", be_overlaps ( srca, srcb ) );
    printf("\n");
    printf("&&&&&&&&&&&&&&& 0 and 1+I with .5 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .5);
    be_set_double_error(srcb, 1, 1, .5);
    printf(" overlaps: %d\n", be_overlaps ( srca, srcb ) );
    printf("\n");
    printf("&&&&&&&&&&&&&&& 0 and 1 with .5 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .5);
    be_set_double_error(srcb, 1, 0, .5);
    printf(" overlaps: %d\n", be_overlaps ( srca, srcb ) );
    printf("\n");
    be_clear(srca);
    be_clear(srcb);
}

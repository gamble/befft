/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"

int main( void ) {
    
    be_t dest, srca, srcb;
    be_init(dest);
    be_init(srca);
    be_init(srcb);
    
    printf("&&&&&&&&&&&&&&& add 0 and 0 with no error computation &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_exact(srca, 0, 0);
    be_set_double_exact(srcb, 0, 0);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    be_add(dest, srca, srcb);
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& add 0 and 0 with    error computation &&&&&&&&&&&&&&&&&&&\n");
    be_add_error(dest, srca, srcb);
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& add 0 and 0 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, 0, 0, .1);
    be_add_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& add 0 and 1+i both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, 1, 1, .1);
    be_add_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& add 0 and .5 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, .5, 0, .1);
    be_add_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& add 1 and .5 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 0, .1);
    be_set_double_error(srcb, .5, 0, .1);
    be_add_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& add 1+i and .5(1+i) both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 1., .1);
    be_set_double_error(srcb, .5, .5, .1);
    be_add_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    
    printf("&&&&&&&&&&&&&&& sub 0 and 0 with no error computation &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_exact(srca, 0, 0);
    be_set_double_exact(srcb, 0, 0);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    be_sub(dest, srca, srcb);
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& sub 0 and 0 with    error computation &&&&&&&&&&&&&&&&&&&\n");
    be_sub_error(dest, srca, srcb);
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& sub 0 and 0 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, 0, 0, .1);
    be_sub_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& sub 0 and 1+i both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, 1, 1, .1);
    be_sub_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& sub 0 and .5 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, .5, 0, .1);
    be_sub_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& sub 1 and .5 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 0, .1);
    be_set_double_error(srcb, .5, 0, .1);
    be_sub_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& sub 1+i and .5(1+i) both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 1., .1);
    be_set_double_error(srcb, .5, .5, .1);
    be_sub_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    
    printf("&&&&&&&&&&&&&&& mul 0 and 0 with no error computation &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_exact(srca, 0, 0);
    be_set_double_exact(srcb, 0, 0);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    be_mul(dest, srca, srcb);
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 0 and 0 with    error computation &&&&&&&&&&&&&&&&&&&\n");
    be_mul_error(dest, srca, srcb);
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 0 and 0 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, 0, 0, .1);
    be_mul_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 0 and 1+i both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, 1, 1, .1);
    be_mul_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 0 and .5 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    be_set_double_error(srcb, .5, 0, .1);
    be_mul_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 1 and .5 both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 0, .1);
    be_set_double_error(srcb, .5, 0, .1);
    be_mul_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    printf("&&&&&&&&&&&&&&& mul 1+i and .5(1+i) both with abs error .1 &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 1., .1);
    be_set_double_error(srcb, .5, .5, .1);
    be_mul_error(dest, srca, srcb);
    printf("srca: "); be_print (srca); printf("\n");
    printf("srcb: "); be_print (srcb); printf("\n");
    printf("dest: "); be_print (dest); printf("\n");
    
#ifdef BEFFT_HAS_ARB    
    acb_t ball;
    acb_init(ball);
    
    printf("&&&&&&&&&&&&&&& set dest from acb 0 &&&&&&&&&&&&&&&&&&&\n");
    acb_zero(ball);
    be_set_acb(dest, ball);
    printf("dest: "); be_print (dest); printf("\n");
    
    printf("&&&&&&&&&&&&&&& set dest from acb 1 &&&&&&&&&&&&&&&&&&&\n");
    acb_one(ball);
    be_set_acb(dest, ball);
    printf("dest: "); be_print (dest); printf("\n");
    
    printf("&&&&&&&&&&&&&&& set dest from acb I &&&&&&&&&&&&&&&&&&&\n");
    acb_onei(ball);
    be_set_acb(dest, ball);
    printf("dest: "); be_print (dest); printf("\n");
    
    printf("&&&&&&&&&&&&&&& set dest from acb -1 &&&&&&&&&&&&&&&&&&&\n");
    acb_one(ball);
    acb_neg(ball, ball);
    be_set_acb(dest, ball);
    printf("dest: "); be_print (dest); printf("\n");
    
    printf("&&&&&&&&&&&&&&& set dest from acb -I &&&&&&&&&&&&&&&&&&&\n");
    acb_onei(ball);
    acb_neg(ball, ball);
    be_set_acb(dest, ball);
    printf("dest: "); be_print (dest); printf("\n");
    
    slong prec = 53;
    ulong K = 1024;
    slong j = 0;
    printf("&&&&&&&&&&&&&&& set dest from acb e^(-Pi*I*%ld/%lu) at prec %ld &&&&&&&&&&&&&&&&&&&\n", j, K, prec);
    be_omega_slong_ulong( dest, j, K );
    printf("dest: "); be_print (dest); printf("\n");
    j=K/2;
    printf("&&&&&&&&&&&&&&& set dest from acb e^(-Pi*I*%ld/%lu) at prec %ld &&&&&&&&&&&&&&&&&&&\n", j, K, prec);
    be_omega_slong_ulong( dest, j, K );
    printf("dest: "); be_print (dest); printf("\n");
    j=K;
    printf("&&&&&&&&&&&&&&& set dest from acb e^(-Pi*I*%ld/%lu) at prec %ld &&&&&&&&&&&&&&&&&&&\n", j, K, prec);
    be_omega_slong_ulong( dest, j, K );
    printf("dest: "); be_print (dest); printf("\n");
    j=3*K/2;
    printf("&&&&&&&&&&&&&&& set dest from acb e^(-Pi*I*%ld/%lu) at prec %ld &&&&&&&&&&&&&&&&&&&\n", j, K, prec);
    be_omega_slong_ulong( dest, j, K );
    printf("dest: "); be_print (dest); printf("\n");
    
    for ( j=1; j<10; j++){
        printf("&&&&&&&&&&&&&&& set dest from acb e^(-Pi*I*%ld/%lu) at prec %ld &&&&&&&&&&&&&&&&&&&\n", j, K, prec);
        be_omega_slong_ulong( dest, j, K );
        printf("dest: "); be_print (dest); printf("\n");
    }
    
    acb_clear(ball);
#endif
    be_clear(dest);
    be_clear(srca);
    be_clear(srcb);
    return 0;
}

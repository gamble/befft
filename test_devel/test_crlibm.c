/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"
#include "crlibm.h"

void omega_uint_with_crlibm( double omega_re[], double omega_im[], double *omega_ae, uint n){
    fenv_t fe;
    feholdexcept(&fe);
    ulong N  = ((ulong) 0x1)<<n;
    ulong No2= N>>1;
    omega_re[0] = 1.;
    omega_im[0] = 0.;
    for( ulong j = 1; j<No2; j++ ){
        double j2oN = ldexp( -((double) j), -n);
        omega_re[j] = cospi_rn( j2oN );
        omega_im[j] = sinpi_rn( j2oN );
        omega_re[N-j] = -omega_re[j];
        omega_im[N-j] = omega_im[j];
    }
    omega_re[No2] = 0.;
    omega_im[No2] = -1.;
    *omega_ae    = ldexp(1, -BEFFT_PREC);
    
    feclearexcept (FE_UNDERFLOW);
    feupdateenv(&fe);
}

int main(int argc, char* argv[]) {
    
    uint n = 4;
    ulong k = 1;
    if (argc >= 2)
        sscanf(argv[1], "%u", &n);
    
    if (argc >= 3)
        sscanf(argv[2], "%lu", &k);
    
    ulong q = ( (ulong) 0x1 ) << n;
    printf("test crlibm for n=%u, q=%lu, k=%lu\n", n, q, k);
    
    be_vec_t omegas;
    be_vec_init(omegas, q/2);
    double *omega_re = be_vec_realref(omegas);
    double *omega_im = be_vec_imagref(omegas);
    
    be_vec_omega_ulong( omegas, n-1 );
    
    double k2oq = ldexp( -((double) k), -(n-1));
    double cosPik2oq = cospi_rn( k2oq );
    double sinPik2oq = sinpi_rn( k2oq );
    
    printf("-%lu-th order %lu root of unity computed with CRlibm: %.20e + I %.20e\n", k, q, cosPik2oq, sinPik2oq );
    printf("                              max of relative errors: 1ulp           \n"); 
    
    if (q<=4096){
    printf("-%lu-th order %lu root of unity in befft table      : %.20e + I %.20e\n", k, q, omega_re[k], omega_im[k] );
    printf("                              max of relative errors: % .20e         \n", be_vec_aberref(omegas));
    } else {
    printf("-%lu-th order %lu root of unity computed in befft   : %.20e + I %.20e\n", k, q, omega_re[k], omega_im[k] );
    printf("                              max of relative errors: % .20e         \n", be_vec_aberref(omegas));
    }
    
    omega_uint_with_crlibm( omega_re, omega_im, &(be_vec_aberref(omegas)), n-1);
    
    printf("-%lu-th order %lu root of unity computed with CRlibm: %.20e + I %.20e\n", k, q, omega_re[k], omega_im[k] );
    printf("                              max of relative errors: % .20e         \n", be_vec_aberref(omegas));
    
    be_vec_clear(omegas);
    
    befft_fft_rad2_t rad2;
    befft_fft_rad2_init_with_omegas( rad2, omega_re, omega_im, be_vec_aberref(omegas), n );
    
    
    befft_fft_rad2_clear(rad2);
    
    return 0;
}

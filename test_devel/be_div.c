/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"

int main( void ) {
    
    be_t dest, srca, srcb;
    be_init(dest);
    be_init(srca);
    be_init(srcb);
    
    printf("&&&&&&&&&&&&&&& inv 0 with no error computation &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, 0);
    _be_real_inv(&(dest->_real), srca->_real);
    dest->_imag = 0.;
    dest->_aber = 0.;
    printf("dest with real inv: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& inv 0 with 0 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, 0);
    _be_real_inv_aber(&(dest->_real), &(dest->_aber),
                      srca->_real,    srca->_aber );
    dest->_imag = 0.;
    printf("dest with real inv: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& inv 0 with .1 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 0, 0, .1);
    _be_real_inv_aber(&(dest->_real), &(dest->_aber),
                      srca->_real,    srca->_aber );
    dest->_imag = 0.;
    printf("dest with real inv: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& inv 1 with 0 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 0, 0);
    _be_real_inv_aber(&(dest->_real), &(dest->_aber),
                      srca->_real,    srca->_aber );
    dest->_imag = 0.;
    printf("dest with real inv: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& inv 1 with .1 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1, 0, .1);
    _be_real_inv_aber(&(dest->_real), &(dest->_aber),
                      srca->_real,    srca->_aber );
    dest->_imag = 0.;
    printf("dest with real inv: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& inv 2 with 0 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 2., 0, 0);
    _be_real_inv_aber(&(dest->_real), &(dest->_aber),
                      srca->_real,    srca->_aber );
    dest->_imag = 0.;
    printf("dest with real inv: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& inv 2 with .2 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 2., 0, .2);
    _be_real_inv_aber(&(dest->_real), &(dest->_aber),
                      srca->_real,    srca->_aber );
    dest->_imag = 0.;
    printf("dest with real inv: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& div 1 with no error and 2 with .2 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 0, .0);
    be_set_double_error(srcb, 2., 0, .2);
    _be_real_div_aber(&(dest->_real), &(dest->_aber),
                      srca->_real,    srca->_aber,
                      srcb->_real,    srcb->_aber );
    dest->_imag = 0.;
    printf("dest with real inv: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& div 1 with .1 error and 2 with .2 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 0, .1);
    be_set_double_error(srcb, 2., 0, .2);
    _be_real_div_aber(&(dest->_real), &(dest->_aber),
                      srca->_real,    srca->_aber,
                      srcb->_real,    srcb->_aber );
    dest->_imag = 0.;
    printf("dest with real inv: "); be_printd (dest, 20); printf("\n");
    
    printf("&&&&&&&&&&&&&&& div 1 with .01 error and .1 with .2 error &&&&&&&&&&&&&&&&&&&\n");
    be_set_double_error(srca, 1., 0, .1);
    be_set_double_error(srcb, .1, 0, .2);
    _be_real_div_aber(&(dest->_real), &(dest->_aber),
                      srca->_real,    srca->_aber,
                      srcb->_real,    srcb->_aber );
    dest->_imag = 0.;
    printf("dest with real inv: "); be_printd (dest, 20); printf("\n");
    
    be_clear(dest);
    be_clear(srca);
    be_clear(srcb);
    
    return 0;
}

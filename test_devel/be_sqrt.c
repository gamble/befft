/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"

int main( void ) {
    
    double dest, dest_aber, dest2, dest2_aber;
    printf("&&&&&&&&&&&&&&& sqrt of -1. +/- 0. &&&&&&&&&&&&&&&&&&&\n");
    _be_real_sqrt_aber( &dest, &dest_aber, -1.0, 0. );
    printf(" %.16f +/- %.16f\n", dest, dest_aber);
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& sqrt of 1/2 +/- 1. &&&&&&&&&&&&&&&&&&&\n");
    _be_real_sqrt_aber( &dest, &dest_aber, 0.5, 1 );
    printf(" %.16f +/- %.16f\n", dest, dest_aber);
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& sqrt of 1 +/- 0. &&&&&&&&&&&&&&&&&&&\n");
    _be_real_sqrt_aber( &dest, &dest_aber, 1, 0 );
    printf(" %.16f +/- %.16f\n", dest, dest_aber);
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& sqrt of 2 +/- 0. &&&&&&&&&&&&&&&&&&&\n");
    _be_real_sqrt_aber( &dest, &dest_aber, 2, 0 );
    printf(" %.16f +/- %.16f\n", dest, dest_aber);
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& sqrt of 1 +/- 0.1 &&&&&&&&&&&&&&&&&&&\n");
    _be_real_sqrt_aber( &dest, &dest_aber, 1, 0.1 );
    printf(" %.16f +/- %.16f\n", dest, dest_aber);
    printf(" interval: [ %.16f, %.16f ]\n", dest-dest_aber, dest+dest_aber);
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& sqrt of 1.1 +/- 0 &&&&&&&&&&&&&&&&&&&\n");
    _be_real_sqrt_aber( &dest2, &dest2_aber, 1.1, 0. );
    printf(" %.16f +/- %.16f\n", dest2, dest2_aber);
    printf("intersect? %d\n", _be_real_overlaps ( dest, dest_aber, dest2, dest2_aber ) );
    printf("\n");
    
    printf("&&&&&&&&&&&&&&& sqrt of 0.9 +/- 0 &&&&&&&&&&&&&&&&&&&\n");
    _be_real_sqrt_aber( &dest2, &dest2_aber, 0.9, 0. );
    printf(" %.16f +/- %.16f\n", dest2, dest2_aber);
    printf("intersect? %d\n", _be_real_overlaps ( dest, dest_aber, dest2, dest2_aber ) );
    printf("\n");
}

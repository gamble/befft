/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/


#include "befft.h"
#include "mpfr.h"

/* Lemma 1: let 0 < u <= 1                            */
/*          then 0 < u^2 < u < 1 and (1+u)^2 < 1 + 4u */
/* Lemma 2: let 0 <= a <= Pi/(2(1+4u))                */
/*          and 0 <= at<= (1+u)^u a                   */
/*          then sin(at) <= (1+8u)*sin(a)             */
/*          and |cos(at)|<= (1+8u)*|cos(a)|           */
/* Lemma 3: let p >= n-1                              */
/*          and a <= 2*(2^(n-1)-1)*Pi/2^(n)           */
/*          then a <= Pi/(2(1+4u)) with u = 2^-p      */
void omega_uint_with_mpfr( double omega_re[], double omega_im[], double *omega_ae, uint n){
    mpfr_prec_t prec = BEFFT_PREC+10;
    /* let u  = 2^-BEFFT_PREC */
    /* let up = 2^-prec */
    /* ensures all the integers between 0 and 2^n are exactly represented by floating points with prec-bits mantissas*/
    /* and prec >= n-1 */
    if (n>=prec) {
        prec = n+10;
    }
    mpfr_set_default_prec (prec);
    
    mpfr_t k2oq;                 /* represents 2k/q = 2k/2^(n+1) = k/2^(n) */
    mpfr_t k2Pioq;               /* represents 2kPi/q */
    mpfr_t cosk2Pioq, sink2Pioq; /* represent  cos( 2kPi/q ) and sin( 2kPi/q ) */
    mpfr_t Pi;
    mpfr_init(k2oq);
    mpfr_init(k2Pioq);
    mpfr_init(Pi);
    mpfr_init(cosk2Pioq);
    mpfr_init(sink2Pioq);
    
    fenv_t fe;
    feholdexcept(&fe);
    ulong N=((ulong) 0x1)<<n;
    ulong No2= N>>1;
    
    mpfr_const_pi (Pi, MPFR_RNDN);   /* Pi <= (1+up)exact(Pi) */
    
    omega_re[0] = 1.;
    omega_im[0] = 0.;
    for( ulong k = 1; k<No2; k++ ){
        mpfr_set_ui(k2oq, k, MPFR_RNDN);          /* is exact */
        mpfr_neg(   k2oq, k2oq, MPFR_RNDN); 
        mpfr_div_2ui(k2oq, k2oq, n, MPFR_RNDN);   /* is exact */
        mpfr_mul(    k2Pioq, k2oq, Pi, MPFR_RNDN);/*k2Pioq <= (1+up)*exact( k2oq*Pi )        */
                                                  /*       <= (1+up)^2*(k/2^n)*exact(Pi) */
        mpfr_sin_cos(sink2Pioq, cosk2Pioq, k2Pioq, MPFR_RNDN); /* sink2Pioq <= (1+8up)sin(exact( k2oq*Pi )) from Lemma above */
                                                               /* cosk2Pioq <= (1+8up)cos(exact( k2oq*Pi )) from Lemma above */
        
        omega_re[k] = mpfr_get_d(cosk2Pioq, MPFR_RNDN);
        omega_im[k] = mpfr_get_d(sink2Pioq, MPFR_RNDN);
        omega_re[N-k] = -omega_re[k];
        omega_im[N-k] = omega_im[k];
    }
    omega_re[No2] = 0.;
    omega_im[No2] = -1.;
    *omega_ae    = ldexp(1, -BEFFT_PREC);
    
    feclearexcept (FE_UNDERFLOW);
    feupdateenv(&fe);
    
    mpfr_clear(k2oq);
    mpfr_clear(k2Pioq);
    mpfr_clear(Pi);
    mpfr_clear(cosk2Pioq);
    mpfr_clear(sink2Pioq);
    
    mpfr_free_cache ();
}

int main(int argc, char* argv[]) {
    
    uint n = 4;
    ulong k = 1;
    if (argc >= 2)
        sscanf(argv[1], "%u", &n);
    
    if (argc >= 3)
        sscanf(argv[2], "%lu", &k);
    
    ulong q = ((ulong) 0x1)<<n;
    printf("test mpfr for n=%u, q=%lu, k=%lu\n", n, q, k);
    
    mpfr_prec_t prec = BEFFT_PREC+10;
    /* let u  = 2^-BEFFT_PREC */
    /* let up = 2^-prec */
    /* ensures all the integers between 0 and 2^n are exactly represented by floating points with prec-bits mantissas*/
    if (n>=prec) {
        prec = n+10;
    }
    mpfr_set_default_prec (prec);
    
    mpfr_t k2oq;                 /* represents 2k/q = 2k/2^(n) = k/2^(n-1) */
    mpfr_t k2Pioq;               /* represents 2kPi/q */
    mpfr_t cosk2Pioq, sink2Pioq; /* represent  cos( 2kPi/q ) and sin( 2kPi/q ) */
    mpfr_t Pi;
    mpfr_init(k2oq);
    mpfr_init(k2Pioq);
    mpfr_init(Pi);
    mpfr_init(cosk2Pioq);
    mpfr_init(sink2Pioq);
    mpfr_const_pi (Pi, MPFR_RNDN);   /* Pi <= (1+up)exact(Pi) */
    
    mpfr_set_ui(k2oq, k, MPFR_RNDN); /* is exact */
    printf("k2oq in double  : %.16e \n", mpfr_get_d(k2oq, MPFR_RNDN) );
    
    mpfr_div_2ui(k2oq, k2oq, n-1, MPFR_RNDN); /* is exact */
    printf("k2oq in double  : %.16e \n", mpfr_get_d(k2oq, MPFR_RNDN) );
    
    mpfr_mul(k2Pioq, k2oq, Pi, MPFR_RNDN); /*k2Pioq <= (1+up)*exact( k2oq*Pi )        */
                                           /*       <= (1+up)^2*(k/2^(n-1))*exact(Pi) */
    printf("k2Pioq in double: %.16e \n", mpfr_get_d(k2Pioq, MPFR_RNDN) );                                       
    
    mpfr_sin_cos(sink2Pioq, cosk2Pioq, k2Pioq, MPFR_RNDN); /* sink2Pioq <= (1+up)*exact( sin(k2Pioq) ) */
                                                           /* Lemma: when a <= pi/2, sin(a(1+b))<=(1+2b)sin(a) */
    printf("cosk2Pioq in double: %.16e \n", mpfr_get_d(cosk2Pioq, MPFR_RNDN) ); 
    printf("sink2Pioq in double: %.16e \n", mpfr_get_d(sink2Pioq, MPFR_RNDN) ); 
    
    mpfr_clear(k2oq);
    mpfr_clear(k2Pioq);
    mpfr_clear(Pi);
    mpfr_clear(cosk2Pioq);
    mpfr_clear(sink2Pioq);
    
    mpfr_free_cache ();
    
    be_vec_t omegas;
    be_vec_init(omegas, q/2);
    double *omega_re = be_vec_realref(omegas);
    double *omega_im = be_vec_imagref(omegas);
    
    be_vec_omega_uint( omegas, n-1 );
    
    printf("-%lu-th order %lu root of unity computed in befft   : %.20e + I %.20e\n", k, q, omega_re[k], omega_im[k] );
    printf("                              max of relative errors: % .20e         \n", be_vec_aberref(omegas));
    
    omega_uint_with_mpfr( omega_re, omega_im, &(be_vec_aberref(omegas)), n-1);
    
    printf("-%lu-th order %lu root of unity computed with mpfr  : %.20e + I %.20e\n", k, q, omega_re[k], omega_im[k] );
    printf("                              max of relative errors: % .20e         \n", be_vec_aberref(omegas));
    
    be_vec_clear(omegas);
    
    return 0;
}

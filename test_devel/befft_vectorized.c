/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"
#include <float.h>

#ifdef BEFFT_HAS_ARB
#include "acb_dft.h"
#include "flint/flint.h"
#include "arf.h"
#endif

#define BLACK "\e[0m"
#define SET_BLACK printf(BLACK)
#define GREEN "\e[1;32m"
#define SET_GREEN printf(GREEN)
#define RED "\e[1;31m"
#define SET_RED printf(RED)

void print_table( double * xre, double * xim, ulong size ){
    printf("[ \n");
    for (ulong j=0; j<size; j++) {
        printf("  <%.5e + I%.5e>, \n", xre[j], xim[j]);
    }
    printf("] \n");
}

void print_acb_vec( acb_ptr v, ulong size ){
    printf("[ \n");
    for (ulong j=0; j<size; j++) {
        acb_printd(v+j, 5); printf("\n");
    }
    printf("] \n");
}

int _be_center_is_in_acb( const double yre, const double yim, acb_t z ){
    arf_t re,im;
    arf_init(re);
    arf_init(im);
    
    arf_set_d(re, yre);
    arf_set_d(im, yim);
    int res = arb_contains_arf( acb_realref(z), re ) && arb_contains_arf( acb_imagref(z), im );
    
    arf_clear(re);
    arf_clear(im);
    
    return res;
}

int _be_overlaps_acb( const double yre, const double yim, const double yab, acb_t z ){
    acb_t y;
    acb_init(y);
    
    arf_set_d( arb_midref(acb_realref(y)), yre);
    arf_set_d( arb_midref(acb_imagref(y)), yim);
    mag_set_d( arb_radref(acb_realref(y)), yab);
    mag_set_d( arb_radref(acb_imagref(y)), yab);
//     printf("y: "); acb_printd(y,10);printf("\n");
//     printf("z: "); acb_printd(z,10);printf("\n");
    int res = acb_overlaps(y,z);
//     printf("res: %d\n", res);
    
    acb_clear(y);
    
    return res;
}

int _be_vec_overlaps_acb_vec( double * yre, double * yim, const double yab, ulong size, acb_ptr z ){
    for (ulong j=0; j<size; j++) {
        if ( _be_overlaps_acb(yre[j],yim[j],yab,z+j)==0 )
            return 0;
    }
    return 1;
}

int _be_vec_aber_overlaps_acb_vec( double * yre, double * yim, double * yab, ulong size, acb_ptr z ){
    for (ulong j=0; j<size; j++) {
        if ( _be_overlaps_acb(yre[j],yim[j],yab[j],z+j)==0 )
            return 0;
    }
    return 1;
}

int _be_vec_center_is_in_acb_vec( double * yre, double * yim, ulong size, acb_ptr z ){
    for (ulong j=0; j<size; j++) {
        if ( _be_center_is_in_acb( yre[j], yim[j], z+j )==0 )
            return 0;
    }
    return 1;
}

int _be_vec_centers_equal( double * yre, double * yim, double * xre, double * xim, ulong size){
    for (ulong j=0; j<size; j++) {
        if ( (yre[j]!=xre[j])||(yim[j]!=xim[j]) )
            return 0;
    }
    return 1;
}

void _be_center_max_dist_to_acb( arf_t dest, const double yre, const double yim, acb_t z ){
    arf_t re,im;
    arf_init(re);
    arf_init(im);
    
    arf_set_d(re, yre);
    arf_set_d(im, yim);
    arf_sub(re, re, arb_midref(acb_realref(z)), BEFFT_PREC, ARF_RND_UP );
    arf_sub(im, im, arb_midref(acb_imagref(z)), BEFFT_PREC, ARF_RND_UP  );
    arf_abs(re, re );
    arf_abs(im, im );
    arf_max(dest, re, im);
    
    arf_clear(re);
    arf_clear(im);
}

double _be_vec_center_max_dist_to_acb_vec( double * yre, double * yim, ulong size, acb_ptr z ){
    arf_t dist, temp;
    arf_init(dist);
    arf_init(temp);
    _be_center_max_dist_to_acb( dist, yre[0], yim[0], z+0 );
    for (ulong j=1; j<size; j++) {
        _be_center_max_dist_to_acb( temp, yre[j], yim[j], z+j );
        arf_max(dist, dist, temp);
    }
    double res = arf_get_d( dist, ARF_RND_UP );
    arf_clear(dist);
    arf_clear(temp);
    return res;
}

double _acb_vec_max_error( acb_ptr z, ulong size ) {
    mag_t max;
    mag_init(max);
    mag_max(max, arb_radref( acb_realref(z+0) ), arb_radref( acb_imagref(z+0) ) );
    for (ulong j=1; j<size; j++) {
        mag_max(max, max, arb_radref( acb_realref(z+j) ) );
        mag_max(max, max, arb_radref( acb_imagref(z+j) ) );
    }
    double res = mag_get_d(max);
    mag_clear(max);
    return res;
}

void generate_random_vector( double * xre, double * xim, double * xab, double * xab_max, int coeff_error, ulong size ){
    *xab_max = 0;
    double coeff_err = pow(10, coeff_error);
    for (ulong j=0; j<size; j++) {
//         xre[j] = DBL_MIN*((double) rand())/((double) rand());
        xre[j] = ((double) rand())/((double) rand());
        xim[j] = ((double) rand())/((double) rand());
//         xab[j] = 0.;
        xab[j] = coeff_err*((double) rand())/RAND_MAX;
        *xab_max = BEFFT_MAX(*xab_max, xab[j]);
    }
}

int main(int argc, char* argv[]) {
    
    uint nbCalls = 100;
    uint n = 4;
    uint nbPols = 1;
    int  c = -5;
    int  verbosity=1;
    
    if (argc >= 2)
        sscanf(argv[1], "%u", &n);
    
    if (argc >= 3)
        sscanf(argv[2], "%u", &nbPols);
    
    if (argc >= 4)
        sscanf(argv[3], "%u", &nbCalls);
    
    if (argc >= 5)
        sscanf(argv[4], "%d", &c);
    
    if (argc >= 6)
        sscanf(argv[5], "%d", &verbosity);
    
    printf("test fft for %u random vectors with log2 of size %u, and max of componentwise error %.1e\n", nbPols, n, pow(10,c));
    
    ulong N = ((ulong) 0x1)<<n;
    srand (0);
    
    double * xre   = (double *) befft_malloc (N*sizeof(double));
    double * xim   = (double *) befft_malloc (N*sizeof(double));
    double * xab   = (double *) befft_malloc (N*sizeof(double));
    double * yre_s = (double *) befft_malloc (N*sizeof(double));
    double * yim_s = (double *) befft_malloc (N*sizeof(double));
    double * yab_s = (double *) befft_malloc (N*sizeof(double));
    double * yre_sv = (double *) befft_malloc (N*sizeof(double));
    double * yim_sv = (double *) befft_malloc (N*sizeof(double));
    double * yab_sv = (double *) befft_malloc (N*sizeof(double));
//     double * yre_e = (double *) befft_malloc (N*sizeof(double));
//     double * yim_e = (double *) befft_malloc (N*sizeof(double));
//     double * yab_e = (double *) befft_malloc (N*sizeof(double));
//     double * yre_d = (double *) befft_malloc (N*sizeof(double));
//     double * yim_d = (double *) befft_malloc (N*sizeof(double));
//     double * yab_d = (double *) befft_malloc (N*sizeof(double));
//     double * yre_d2 = (double *) befft_malloc (N*sizeof(double));
//     double * yim_d2 = (double *) befft_malloc (N*sizeof(double));
//     double * yab_d2 = (double *) befft_malloc (N*sizeof(double));
    double xab_max; /* max of the absolute error on x = inf morm of xab*/
    
//     acb_ptr zacb = _acb_vec_init(N);
    
    double total_time_1s = 0.;
    double total_time_1sv = 0.;
    double total_time_2s = 0.;
    double total_time_2sv = 0.;
//     double total_time_3s = 0.;
//     double total_time_3sv = 0.;
    double * total_time_ks  = (double *) befft_malloc (n*sizeof(double));
    double * total_time_ksv = (double *) befft_malloc (n*sizeof(double));
    for (uint k=1; k<=n; k++) {
        total_time_ks[k-1]=0.;
        total_time_ksv[k-1]=0.;
    }
    
    double total_time_s = 0.;
    double total_time_sv = 0.;
    double total_time_s2 = 0.;
    double total_time_sv2 = 0.;
    
    int    all_same_centers = 1;
    
    /* precomputation for befft */
    befft_fft_rad2_t rad2;
    clock_t start_befft_pre = clock();
    befft_fft_rad2_init(rad2, n);
    double befft_precomp_time = (double) (clock() - start_befft_pre)/CLOCKS_PER_SEC;
    
//     /* precomputation for acb_dft */
//     acb_dft_rad2_t dftrad2;
//     clock_t start_acb_pre = clock();
//     acb_dft_rad2_init( dftrad2, n, BEFFT_PREC );
//     double acb_precomp_time = (double) (clock() - start_acb_pre)/CLOCKS_PER_SEC;
    uint j;
    for (uint i = 0; i<nbPols; i++) {
        
//         int underflow = 0;
        
        if (verbosity)
            printf("&&& vec %u\n", i);
    
        generate_random_vector( xre, xim, xab, &xab_max, c, N );
//         for (ulong j=0; j<N; j++){
//             acb_set_d_d(zacb+j, xre[j], xim[j]);
//             mag_set_d( arb_radref( acb_realref(zacb+j) ), xab[j] );
//             mag_set_d( arb_radref( acb_imagref(zacb+j) ), xab[j] );
//         }
        int center_equal_sv;
        
        double * errors = (double *) befft_malloc (nbCalls*sizeof(double));
        
        if (n>=1) {
            clock_t start_1s = clock();
            for (j = 0; j<nbCalls; j++)
                errors[j] = befft_FirstStep_static( yre_s, yim_s, xre, xim, n);
            double time_1s     = (double) (clock() - start_1s)/CLOCKS_PER_SEC;
            total_time_1s+=(time_1s/j);
            total_time_s2+=(time_1s/j);
            if (verbosity) {
                printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft first  step: %.10f\n", time_1s );
            }
            
            clock_t start_1sv = clock();
            for (j = 0; j<nbCalls; j++)
                errors[j] = befft_FirstStep_static_vect( yre_sv, yim_sv, xre, xim, rad2);
            double time_1sv     = (double) (clock() - start_1sv)/CLOCKS_PER_SEC;
            center_equal_sv = _be_vec_centers_equal(yre_sv, yim_sv, yre_s, yim_s, N);
//             all_same_centers&=center_equal_sv;
            total_time_1sv+=(time_1sv/j);
            total_time_sv2+=(time_1sv/j);
            if (verbosity) {
                printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft first  step v: %.10f\n", time_1sv );
                printf("      same centers?                                    : %d\n", center_equal_sv );
            }
        }
        
        if (n>=2) {
            clock_t start_2s = clock();
            for (j = 0; j<nbCalls; j++)
                errors[j] = befft_SecondStep_static( yre_s, yim_s, xre, xim, n);
            double time_2s     = (double) (clock() - start_2s)/CLOCKS_PER_SEC;
            total_time_2s+=(time_2s/j);
            total_time_s2+=(time_2s/j);
            if (verbosity) {
                printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft second step: %.10f\n", time_2s );
            }
            
            clock_t start_2sv = clock();
            for (j = 0; j<nbCalls; j++)
                errors[j] = befft_SecondStep_static_vect( yre_sv, yim_sv, xre, xim, n);
            double time_2sv     = (double) (clock() - start_2sv)/CLOCKS_PER_SEC;
            center_equal_sv = _be_vec_centers_equal(yre_sv, yim_sv, yre_s, yim_s, N);
//             all_same_centers&=center_equal_sv;
            total_time_2sv+=(time_2sv/j);
            total_time_sv2+=(time_2sv/j);
            if (verbosity) {
                printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft second step v:  %.10f\n", time_2sv );
                printf("      same centers?                                    : %d\n", center_equal_sv );
            }
        }
        
//         if (n>=3) {
//             clock_t start_3s = clock();
//             for (j = 0; j<nbCalls; j++)
//                 errors[j] = befft_OneStep_static( yre_s, yim_s, xre, xim, rad2, 3);
//             double time_3s     = (double) (clock() - start_3s)/CLOCKS_PER_SEC;
//             total_time_3s+=(time_3s/j);
//             total_time_s2+=(time_3s/j);
//             if (verbosity) {
//                 printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft third step:  %.10f\n", time_3s );
//             }
//             
//             clock_t start_3sv = clock();
//             for (j = 0; j<nbCalls; j++)
//                 errors[j] = befft_OneStep_static_vect( yre_sv, yim_sv, xre, xim, rad2, 3);
//             double time_3sv     = (double) (clock() - start_3sv)/CLOCKS_PER_SEC;
//             center_equal_sv = _be_vec_centers_equal(yre_sv, yim_sv, yre_s, yim_s, N);
// //             all_same_centers&=center_equal_sv;
//             total_time_3sv+=(time_3sv/j);
//             total_time_sv2+=(time_3sv/j);
//             if (verbosity) {
//                 printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft third step v: %.10f\n", time_3sv );
//                 printf("      same centers?                                    : %d\n", center_equal_sv );
//             }
//         }
        
        for (uint k = 1; k<=n; k++) {
            clock_t start_ks = clock();
            for (j = 0; j<nbCalls; j++)
                errors[j] = befft_OneStep_static( yre_s, yim_s, xre, xim, rad2, k);
            double time_ks     = (double) (clock() - start_ks)/CLOCKS_PER_SEC;
            total_time_ks[k-1]+=(time_ks/j);
            total_time_s2+=(time_ks/j);
            if (verbosity) {
                printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft %u-th step:  %.10f\n", k, time_ks );
            }
            
            clock_t start_ksv = clock();
            for (j = 0; j<nbCalls; j++)
                errors[j] = befft_OneStep_static_vect( yre_sv, yim_sv, xre, xim, rad2, k);
            double time_ksv     = (double) (clock() - start_ksv)/CLOCKS_PER_SEC;
//             center_equal_sv = _be_vec_centers_equal(yre_sv, yim_sv, yre_s, yim_s, N);
//             all_same_centers&=center_equal_sv;
            total_time_ksv[k-1]+=(time_ksv/j);
            total_time_sv2+=(time_ksv/j);
            if (verbosity) {
                printf("      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& time in befft %u-th step v: %.10f\n", k, time_ksv );
                printf("      same centers?                                    : %d\n", center_equal_sv );
            }
        }
        
        clock_t start_s = clock();
        double * abserror_s = (double *) befft_malloc( nbCalls * sizeof(double));
        for (j = 0; j<nbCalls; j++)
            befft_fft_rad2_exact_input_precomp(yre_s, yim_s, abserror_s + j, xre, xim, rad2);
        double time_s     = (double) (clock() - start_s)/CLOCKS_PER_SEC;
        total_time_s+=(time_s/j);
        
        clock_t start_sv = clock();
        double * abserror_sv = (double *) befft_malloc( nbCalls * sizeof(double));
        for (j = 0; j<nbCalls; j++)
            befft_fft_rad2_exact_input_vect_precomp(yre_sv, yim_sv, abserror_sv + j, xre, xim, rad2);
        double time_sv     = (double) (clock() - start_sv)/CLOCKS_PER_SEC;
        center_equal_sv = _be_vec_centers_equal(yre_sv, yim_sv, yre_s, yim_s, N);
        all_same_centers&=center_equal_sv;
        total_time_sv+=(time_sv/j);
        
        befft_free(abserror_s);
        befft_free(abserror_sv);
        befft_free(errors);
    }
    
    printf("&&&&&&&&&&&& time in befft_fft precomp                : %.10f\n", befft_precomp_time );
//     printf("&&&&&&&&&&&& time in acb_dft_rad2 precomp                                    : %f\n", acb_precomp_time );
    
    if (n>=1) {
        printf("&&&&&&&&&&&& average time in befft  first step    : %.10f\n", total_time_1s/nbPols );
        printf("&&&&&&&&&&&& average time in befft  first step v  : %.10f\n", total_time_1sv/nbPols );
    }
    if (n>=2) {
        printf("&&&&&&&&&&&& average time in befft second step    : %.10f\n", total_time_2s/nbPols );
        printf("&&&&&&&&&&&& average time in befft second step v  : %.10f\n", total_time_2sv/nbPols );
    }
//     if (n>=3) {
//         printf("&&&&&&&&&&&& average time in befft  third step    : %.10f\n", total_time_3s/nbPols );
//         printf("&&&&&&&&&&&& average time in befft  third step v  : %.10f\n", total_time_3sv/nbPols );
//     }
    for (uint k = 1; k<=n; k++) {
        printf("&&&&&&&&&&&& average time in befft %3u-th step    : %.10f\n", k, total_time_ks[k-1]/nbPols );
        printf("&&&&&&&&&&&& average time in befft %3u-th step v  : %.10f\n", k, total_time_ksv[k-1]/nbPols );
    }
    printf("&&&&&&&&&&&& average time in befft                : %.10f\n", total_time_s/nbPols );
    printf("&&&&&&&&&&&& average time in all steps            : %.10f\n", total_time_s2/nbPols );
    printf("&&&&&&&&&&&& average time in befft v              : %.10f\n", total_time_sv/nbPols );
    printf("&&&&&&&&&&&& average time in all steps v          : %.10f\n", total_time_sv2/nbPols );
    
    printf("&&&&&&&&&&&& for each vector, all beffts have same centers                   : ");
    if (all_same_centers) { SET_GREEN; printf(" YES "); } else { SET_RED; printf(" NO"); } SET_BLACK; printf("\n");
    
//     _acb_vec_clear(zacb, N);
    
//     acb_dft_rad2_clear( dftrad2 );
    befft_fft_rad2_clear(rad2);
    
    flint_cleanup();
    
    befft_free(xre);
    befft_free(xim);
    befft_free(xab);
    befft_free(yre_s);
    befft_free(yim_s);
    befft_free(yab_s);
    befft_free(yre_sv);
    befft_free(yim_sv);
    befft_free(yab_sv);
    befft_free(total_time_ks);
    befft_free(total_time_ksv);
//     befft_free(yre_e);
//     befft_free(yim_e);
//     befft_free(yab_e);
//     befft_free(yre_d);
//     befft_free(yim_d);
//     befft_free(yab_d);
//     befft_free(yre_d2);
//     befft_free(yim_d2);
//     befft_free(yab_d2);
    return 0;
}

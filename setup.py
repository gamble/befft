#****************************************************************************
#        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
#                                Remi Imbach     <remi.imbach@laposte.net>
# 
#    This file is part of befft.
#
#    befft is free software: you can redistribute it and/or modify 
#    it under the terms of the GNU Lesser General Public License as 
#    published by the Free Software Foundation, either version 3 of 
#    the License, or (at your option) any later version.
#
#    befft is distributed in the hope that it will be useful, but 
#    WITHOUT ANY WARRANTY; without even the implied warranty of 
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#    See the GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public 
#    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
#*****************************************************************************

from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext
import sys
import numpy
import glob
import os

c_directories = ['base',
                 'be',
                 'be_vec',
                 'fft']

c_files = [os.sep.join(['befft', 'interface.c'])]

for d in c_directories:
    c_pattern = os.sep.join(['src',d, '*.c'])
    c_files += glob.glob(c_pattern)
	
cargs = ['-DBEFFT_HAS_ARB', '-fno-wrapv', '-g0', '-O2',          # to cancel python flags
         '-march=native', '-ftree-vectorize', '-funroll-loops',  # for optimization
         '-frounding-math',                                      # for correct fp exception detection
         '-Wno-unknown-pragmas']
llibs = ['-larb', '-lm']
include_dirs = ['src', numpy.get_include(), sys.path]

# cargs_msvc = ['/DBEFFT_HAS_MPFR', '/Wall', '/Qspectre', '/fp:strict',
              # '/wd4710', '/wd4711', '/wd5045', '/wd4820']
# llibs_msvc = ['mpfr']
# cargs_msvc = ['/DBEFFT_HAS_ARB','/Wall', '/Qspectre', '/fp:strict',
              # '/wd4710', '/wd4711', '/wd5045', '/wd4820']
# llibs_msvc = ['arb.lib']
# include_dirs_msvc = ['c:\\Users\\imbach\\vcpkg\\installed\\x64-windows\\include',
					 # 'src', numpy.get_include(), sys.path]
# library_dirs_msvc = ['c:\\Users\\imbach\\vcpkg\\installed\\x64-windows\\lib']
# libraries = ["arb", "flint", "mpfr", "gmp"]
cargs_msvc = ['/Wall', '/Qspectre', '/fp:strict',
              '/wd4710', '/wd4711', '/wd5045', '/wd4820']
llibs_msvc = []
include_dirs_msvc = ['src', numpy.get_include(), sys.path]
library_dirs_msvc = []
libraries = []

class build_ext_flags_link(build_ext):
	def build_extension(self, ext):
		print(self.compiler.compiler_type)
		if self.compiler.compiler_type == 'msvc':
			ext.extra_compile_args = cargs_msvc
			ext.extra_link_args = llibs_msvc
			#ext.include_dirs=include_dirs_msvc
			#ext.library_dirs=library_dirs_msvc
		else:
			ext.extra_compile_args = cargs
			ext.extra_link_args = llibs
		build_ext.build_extension(self, ext)
    
libbefft = Extension( name="befft._interface", sources = c_files,
					  # libraries=libraries,
                      include_dirs = ['src', numpy.get_include(), sys.path],
					  )

setup(name = 'befft',
      version = '0.0',
      description = 'radix 2 fft in double precision with error bound',
      author='Remi Imbach and Guillaume Moroz',
      author_email='guillaume.moroz@inria.fr',
      python_requires = '>=3',
      install_requires = ['numpy'],
      url='https://gitlab.inria.fr/gamble/befft',
      ext_modules = [libbefft],
      cmdclass = {'build_ext': build_ext_flags_link},
      packages    = ['befft'])

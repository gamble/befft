/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"
#include <float.h>

#ifdef BEFFT_HAS_ARB
#include "acb_poly.h"
#include "acb_dft.h"
#endif

/******************** The following functions comes from EARoots *****/
// simd parameters
//   Rbits size of registers
//   Nr number of registers
// #ifdef __AVX512CD__
// #define Rbits 512
// #define Nr 32
// #elif __AVX__
// #define Rbits 256
// #define Nr 16
// #elif __SSE__
// #define Rbits 128
// #define Nr 16
// #else
#define Rbits 64
#define Nr 16
// #endif
#define Rbytes (Rbits/8)
#define Nd (Rbytes/8)
#define DBlock (Nd*Nr/2)
#define CSBlock (Nd*Nr/2)
#define CDBlock (Nd*Nr/4)

// This function provides a smaller bound on the error of the absolute
// value of horner evaluation of p on z
// The value represented by the input are in the balls p +- |p|e_p and
// z +- |z|e_z
// Algorithm derived from Algorithm 6.4 of
// Handbook of Floating-Point Arithmetic, Muller
// Error bounds based on Lemma 2.3 in 
// Verified solutions of linear systems without directed rounding
// Ogita, Rump, Oishi
void ea_horner_abs_with_error_bound(const double* p_r, const double* p_i, int n, double* aep_u,
                                    const double* z_r, const double* z_i, int m, double e_z_u,
                                    double* results_r, double* results_i, double* results_abs, double* error)
{
    double eu = ldexp(1,-53);
    double umin = ldexp(1,-1074);
    double delta = 1 + sqrt(5) + e_z_u; // 3 u
    if(n==1){
        for(int i=0; i<m; i++) {
            results_r[i] = p_r[0];
            results_i[i] = p_i[0];
            results_abs[i] = hypot(p_r[0], p_i[0]);
            double b = aep_u[0] + 4*results_abs[0];
            int ne = 4;
            error[i] = b*eu/(1-(ne+1)*eu); 
        }
    } else {
        for(int i=0; i < m; i+=CDBlock) {
            double x[CDBlock] = {0};
            double y[CDBlock] = {0};
            double u[CDBlock] = {0};
            double v[CDBlock] = {0};
            double w[CDBlock] = {0};
            double e[CDBlock] = {0};
            int p = (i+CDBlock<=m) ? CDBlock : (m%CDBlock);
            for(int k=0; k<p; k++) {
                u[k] = z_r[i+k];
                v[k] = z_i[i+k];
                w[k] = hypot(u[k], v[k]);                  // 4 + e_z u
                x[k] = p_r[n-1];
                y[k] = p_i[n-1];
                e[k] = (hypot(p_r[n-1], p_i[n-1]) + aep_u[n-1])/delta; // (4 + 2) + 3 + 1  = 10 u
            }
            for(int j=1; j<n-1; j++) {
                double a = p_r[n-1-j];
                double b = p_i[n-1-j];
                double c = aep_u[n-1-j] / delta + 5*umin; // 0 + 3 + 1 + 1 u
                for(int k=0; k<CDBlock; k++) {
//                     #pragma STDC FP_CONTRACT ON
                    double s,t;
                    s = a + u[k]*x[k] - v[k]*y[k];
                    t = b + u[k]*y[k] + v[k]*x[k];
                    x[k] = s;
                    y[k] = t;
                    e[k] = c + fabs(s) + fabs(t) + w[k]*e[k]; // max(ec, 0, 0, ew + e_k + 1) + 3  = ew + e_k + 4  = e_k + 8 + e_z u
                }
            }
            for(int k=0; k<p; k++) {
                double s, t, b, c, d;
                s = p_r[0] + u[k]*x[k] - v[k]*y[k];
                t = p_i[0] + u[k]*y[k] + v[k]*x[k];
                results_r[i+k] = s;
                results_i[i+k] = t;
                results_abs[i+k] = hypot(s, t);      // 4 u
                // last error corrections
                c = aep_u[0] + (4+delta)*umin; // 2 u
                d = sqrt(5) + e_z_u; // 2 u
                b = c + d*hypot(s, t) + delta*w[k]*e[k]; // max(ec, 5, 3 + ew + e_k + 2) + 2 = e_k + 11 + e_z ulp;
                int ne = 17 + (n-2)*(8 + (int)ceil(e_z_u)); //10 + (n-3)*(8 + e_z) + 11 + e_z + 4 = 17 + (n-2)*(8+e_z) 
                error[i+k] = eu*b / (1 - eu*(ne+1));
            }
        }
    }
}

#undef CDBlock
#undef CSBlock
#undef DBlock
#undef Nd
#undef Rbytes
#undef Nr
#undef Rbits
/**************************************************************/

/* generate random vectors with random errors */
void generate_random_vector( double * xre, double * xim, double * xab, double * xab_max, int coeff_error, be_size_t size ){
    *xab_max = 0;
    double coeff_err = pow(10, coeff_error);
    for (be_size_t j=0; j<size; j++) {
        xre[j] = ((double) rand())/((double) rand());
        xim[j] = ((double) rand())/((double) rand());
		xab[j] = ((double) rand());
		while( !( isnormal(xre[j])&& isnormal(xim[j])&&isnormal(xab[j]) ) ) {
			xre[j] = ((double) rand())/((double) rand());
			xim[j] = ((double) rand())/((double) rand());
			xab[j] = ((double) rand());
		}
        xab[j] = coeff_err*xab[j]/RAND_MAX;
        *xab_max = BEFFT_MAX(*xab_max, xab[j]);
    }
}

/* returns 1 => the two vectors surely overlaps        */
/*         0 => the two vectors surely do not overlaps */
/*        -1 => can not decide                         */
int check_overlaps(  const double xre[], const double xim[], const double xab[],
                     const double yre[], const double yim[], const double yab[], be_size_t size ) {
    int res = 1;
    for( be_size_t i = 0; (i<size)&&(res!=0); i++ ) {
        int rest = _be_overlaps ( xre[i], xim[i], xab[i], yre[i], yim[i], yab[i] );
        if ( !( rest==1  ) )
            res = rest;
    }
    return res;
}

#ifdef BEFFT_COLORS
#define BLACK "\e[0m"
#define SET_BLACK printf(BLACK)
#define GREEN "\e[1;32m"
#define SET_GREEN printf(GREEN)
#define RED "\e[1;31m"
#define SET_RED printf(RED)
#else
#define BLACK 
#define SET_BLACK
#define GREEN 
#define SET_GREEN 
#define RED 
#define SET_RED 
#endif

#if _MSC_VER
#define BEFFT_SSCANF sscanf_s
#else
#define BEFFT_SSCANF sscanf
#endif

int main( int argc, char* argv[] ) {
    uint kmax = 12;
    uint nbvec= 10;
    int verbose = 1;
    int nbdigits= 17;
    
    if (argc >= 2)
        BEFFT_SSCANF(argv[1], "%d", &verbose);
    
    if (argc >= 3)
        BEFFT_SSCANF(argv[2], "%u", &kmax);
    
    if (argc >= 4)
        BEFFT_SSCANF(argv[3], "%u", &nbvec);
    
    if (argc >= 5)
        BEFFT_SSCANF(argv[4], "%d", &nbdigits);
    
    int checkOK = 1;
    
#if !defined(BEFFT_HAS_ARB) && !defined(BEFFT_HAS_MPFR)
    kmax = BEFFT_MIN( 12, kmax );
#endif
    
    for (uint k = 1; (k < kmax)&&(checkOK==1); k++ ) {
        
        be_size_t K = ((be_size_t) 0x1) << k;
        be_vec_t omegas;
        be_vec_init(omegas, K);
        be_vec_omega_uint(omegas, k-1);
        slong KO2 = (K>>1);
        for (be_size_t i = KO2; i<K; i++) {
            be_vec_realref(omegas)[i] = -be_vec_realref(omegas)[i-KO2];
            be_vec_imagref(omegas)[i] = -be_vec_imagref(omegas)[i-KO2];
        }
        double rel_err_in_ulp = 1;
        
        befft_fft_rad2_t rad2;
        befft_fft_rad2_init( rad2, k );
        
        /* input polynomial */
        double *p = (double *) befft_malloc (4*K*sizeof(double));
        double pab_max = 0;
        /* output evaluation with horner */
        double *h = (double *) befft_malloc (4*K*sizeof(double));
        /* output evaluation with fft */
        double *f = (double *) befft_malloc (3*K*sizeof(double));
        
// #ifdef BEFFT_HAS_ARB
//         double u = BEFFT_U;
//         double rel_err = BEFFT_U;
//         acb_ptr pacb = _acb_vec_init(K);
//         acb_ptr oacb = _acb_vec_init(K);
//         acb_ptr hacb = _acb_vec_init(K);
//         acb_ptr facb = _acb_vec_init(K);
//         acb_dft_rad2_t dftrad2;
//         acb_dft_rad2_init( dftrad2, k, BEFFT_PREC );
// #endif
        
        if (verbose>=2) {
                printf("----- k = %u, K = %zu -----\n", k, K);
        }
        
        for (uint j = 0; (j < nbvec)&&(checkOK==1); j++ ) {
            /* input polynomial */
            generate_random_vector( p, p+K, p+2*K, &pab_max, -10, K );
            /* relative error in ulp */
            for (be_size_t i = 0; i<K; i++) 
                p[3*K+i] = ldexp( p[2*K+i], 53 );
// #ifdef BEFFT_HAS_ARB
//             for (be_size_t i = 0; i<K; i++){
//                 acb_set_d_d(pacb+i, p[i], p[K+i]);
//                 mag_set_d( arb_radref( acb_realref(pacb+i) ), p[2*K+i] );
//                 mag_set_d( arb_radref( acb_imagref(pacb+i) ), p[2*K+i] );
//                 acb_set_d_d(oacb+i, be_vec_realref(omegas)[i], be_vec_imagref(omegas)[i]);
//                 double abs_err = 0, abs_err_aber;
//                 _be_abs_aber( &abs_err, &abs_err_aber, be_vec_realref(omegas)[i], be_vec_imagref(omegas)[i], 0. );
//                 abs_err = abs_err + abs_err_aber;
//                 abs_err = abs_err*rel_err;
//                 abs_err = abs_err/(1-3*u);
//                 mag_set_d( arb_radref( acb_realref(oacb+i) ), abs_err );
//                 mag_set_d( arb_radref( acb_imagref(oacb+i) ), abs_err );
//             }
//             for (be_size_t i = 0; i<K; i++)
//                 _acb_poly_evaluate_horner(hacb+i, pacb, K, oacb+i, BEFFT_PREC);
//             
//             acb_dft_rad2_precomp( facb, pacb, dftrad2, BEFFT_PREC);
// #endif            
            /* output evaluation with horner */
            ea_horner_abs_with_error_bound(p, p+K, (int)K, p+3*K,
                                        be_vec_realref(omegas), be_vec_imagref(omegas), (int)K, rel_err_in_ulp,
                                        h, h+K, h+3*K, h+2*K);
            /* output evaluation with fft */
        //     befft_fft_rad2_init_with_omegas( rad2, be_vec_realref(omegas), be_vec_imagref(omegas), be_vec_aberref(omegas), k );
            int underflow = befft_fft_rad2_dynamic_precomp(f, f+K, f+2*K, p, p+K, p+2*K, rad2);
            
            int overlaps = check_overlaps( h, h+K, h+2*K, f, f+K, f+2*K, K );
            
            checkOK = checkOK && (underflow || (overlaps!=0) );
            
            if (verbose>=2) {
                printf("-%u-th vector, befft underflow?  %d\n", j, underflow );
                printf("               h and y overlaps? %d\n", overlaps );
                printf("\n");
            }
            if (verbose>=3) {
                printf("Input poly: \n");
                for (be_size_t i=0; i<K; i++) {
                    _be_printd( p[i], p[K+i], p[2*K+i], nbdigits ); printf("\n");
                }
                printf("\n");
                
                printf("Roots of unity: \n");
                for (be_size_t i=0; i<K; i++) {
                    _be_printd( be_vec_realref(omegas)[i], be_vec_imagref(omegas)[i], 0., nbdigits ); printf("\n");
                }
                printf("\n");
                
                printf("Evaluations at roots of unity with Horner: \n");
                for (be_size_t i=0; i<K; i++) {
                    _be_printd( h[i], h[K+i], h[2*K+i], nbdigits ); printf("\n");
                }
                printf("\n");
// #ifdef BEFFT_HAS_ARB
//                 printf("Evaluations at roots of unity with Horner ARB: \n");
//                 for (be_size_t i=0; i<K; i++) {
//                     acb_printd( hacb+i, nbdigits ); printf("\n");
//                 }
//                 printf("\n");
// #endif
                
                printf("Evaluations at roots of unity with BEFFT : \n");
                for (be_size_t i=0; i<K; i++) {
                    _be_printd( f[i], f[K+i], f[2*K+i], nbdigits ); printf("\n");
                }
                printf("\n");
// #ifdef BEFFT_HAS_ARB                
//                 printf("Evaluations at roots of unity with fft ARB : \n");
//                 for (be_size_t i=0; i<K; i++) {
//                     acb_printd( facb+i, nbdigits ); printf("\n");
//                 }
//                 printf("\n");
// #endif                
            }
            
            if (verbose>=2) {
                printf("-----------------------------------------\n\n");
            }
//             checkOK=1;
        }
        
// #ifdef BEFFT_HAS_ARB
//         acb_dft_rad2_clear( dftrad2 );
//         _acb_vec_clear(facb, K);
//         _acb_vec_clear(hacb, K);
//         _acb_vec_clear(oacb, K);
//         _acb_vec_clear(pacb, K);
// #endif
        
        befft_free(f);
        befft_free(h);
        befft_free(p);
        befft_fft_rad2_clear(rad2);
        be_vec_clear(omegas);
    }
    
    if (verbose) {
        printf(" test %d ffts on random vectors of size 2^0 to 2^%d : ", nbvec*(kmax), kmax );
        if (checkOK) { 
            SET_GREEN; printf(" PASS "); 
        } else { 
            SET_RED; printf(" FAIL "); 
        } 
        SET_BLACK; 
        printf("\n");
    }
    return !checkOK;
//     return checkOK;
    
// #if defined(BEFFT_HAS_ARB) && defined(BEFFT_HAS_MPFR)
//     
//     for (uint k = 0; k < kmax; k++ ) {
//         be_size_t K = ((be_size_t) 0x1) << k;
//         be_vec_t omegas_prec, omegas_mpfr, omegas_arb;
//         be_vec_init(omegas_prec, K);
//         be_vec_init(omegas_mpfr, K);
//         be_vec_init(omegas_arb,  K);
//         
//         int precompOK = be_vec_omega_precomp( omegas_prec, K );
//         be_vec_omega_uint_with_mpfr( omegas_mpfr, k );
//         be_vec_omega_uint_with_arb( omegas_arb, k, 2*BEFFT_PREC );
//         
//         int precEQmpfr = 1, precEQarb = 1, mpfrEQarb=1;
//         for (be_size_t i=0; i<K; i++) {
//             
//             int precEQmpfr_t = precompOK && ( be_vec_realref( omegas_prec )[i] == be_vec_realref( omegas_mpfr )[i] ) &&
//                                             ( be_vec_imagref( omegas_prec )[i] == be_vec_imagref( omegas_mpfr )[i] ) ;
//             int precEQarb_t  = precompOK && ( be_vec_realref( omegas_prec )[i] == be_vec_realref( omegas_arb  )[i] ) &&
//                                             ( be_vec_imagref( omegas_prec )[i] == be_vec_imagref( omegas_arb  )[i] ) ;
//             int mpfrEQarb_t  = ( be_vec_realref( omegas_mpfr )[i] == be_vec_realref( omegas_arb  )[i] ) &&
//                                ( be_vec_imagref( omegas_mpfr )[i] == be_vec_imagref( omegas_arb  )[i] ) ;
//                                        
//             precEQmpfr = precEQmpfr && precEQmpfr_t;
//             precEQarb  = precEQarb  && precEQarb_t;
//             mpfrEQarb  = mpfrEQarb  && mpfrEQarb_t;
//                 
//             if (verbose) {
//                 if (precompOK && (!precEQmpfr_t) ) {
//                     printf("i: %zu, precEQmpfr: %d\n", i, precEQmpfr);
//                     printf("precomp: "); _be_printd( be_vec_realref( omegas_prec )[i], be_vec_imagref( omegas_prec )[i], be_vec_aberref( omegas_prec ), nbdigits ); printf("\n");
//                     printf("mpfr   : "); _be_printd( be_vec_realref( omegas_mpfr )[i], be_vec_imagref( omegas_mpfr )[i], be_vec_aberref( omegas_mpfr ), nbdigits ); printf("\n");
//                 }
//                 if (precompOK && (!precEQarb_t) ) {
//                     printf("i: %zu, precEQarb: %d\n", i, precEQarb);
//                     printf("precomp: "); _be_printd( be_vec_realref( omegas_prec )[i], be_vec_imagref( omegas_prec )[i], be_vec_aberref( omegas_prec ), nbdigits ); printf("\n");
//                     printf("arb    : "); _be_printd( be_vec_realref( omegas_arb  )[i], be_vec_imagref( omegas_arb  )[i], be_vec_aberref( omegas_arb ), nbdigits ); printf("\n");
//                 }
//                 if (!mpfrEQarb_t) {
//                     printf("i: %zu, precEQarb: %d\n", i, mpfrEQarb);
//                     printf("mpfr   : "); _be_printd( be_vec_realref( omegas_mpfr )[i], be_vec_imagref( omegas_mpfr )[i], be_vec_aberref( omegas_mpfr ), nbdigits ); printf("\n");
//                     printf("arb    : "); _be_printd( be_vec_realref( omegas_arb  )[i], be_vec_imagref( omegas_arb  )[i], be_vec_aberref( omegas_arb ), nbdigits ); printf("\n");
//                 }
//             }
//         }
//         
//         checkOK = checkOK && mpfrEQarb && 
//                   ( (!precompOK) || precEQmpfr ) && ( (!precompOK) || precEQarb ) ;
//         
//         if (verbose) {
//             printf("----- k = %u, K = %zu -----\n", k, K);
//             if (precompOK){
//                 printf(" precEQmpfr: %d\n", precEQmpfr );
//                 printf(" precEQarb : %d\n", precEQarb );
//             }
//             printf(" mpfrEQarb : %d\n", mpfrEQarb );
//         
//     //         printf("precompiled order K roots of unit:\n");
//     //         be_vec_print( omegas_prec );
//     //         printf("\n");
//     //         
//     //         printf("mpfr order K roots of unit:\n");
//     //         be_vec_print( omegas_mpfr );
//     //         printf("\n");
//     //         
//     //         printf("arb order K roots of unit:\n");
//     //         be_vec_print( omegas_arb );
//     //         printf("\n");
//         
//             printf("---------------------------\n");
//             printf("\n");
//         }
//         
//         be_vec_clear(omegas_arb);
//         be_vec_clear(omegas_mpfr);
//         be_vec_clear(omegas_prec);
//     }
//     if (verbose)
//         printf(" checkOK : %d\n", checkOK );
//     return !checkOK;
// #else
//     checkOK = 0;
//     if (verbose)
//         printf(" checkOK : %d\n", checkOK );
//     return 1;
// #endif
}

/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef BEFFT_BASE_H
#define BEFFT_BASE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <fenv.h>
#include <string.h>

// #define BEFFT_TRACK_EXCEPT

// #define BEFFT_TIMINGS
#ifdef BEFFT_TIMINGS
#include <time.h>
#endif

#ifdef BEFFT_HAS_MPFR
#include "mpfr.h"
#endif

#ifdef BEFFT_HAS_ARB
#include "acb.h"
#endif

/* this will never happen...*/
#ifdef BEFFT_HAS_CRLIBM
#include "crlibm.h"
#endif

#define BEFFT_PREC 53
#define BEFFT_EMIN -1022
#define BEFFT_MIN(A,B) (A<=B? A : B)
#define BEFFT_MAX(A,B) (A>=B? A : B)
#define BEFFT_ABS(A) (A<=0? -A : A)
#define BEFFT_SQRTTWO  1.42
#define BEFFT_SQRTFIVE 2.24

/* if rounding mode is nearest, u = (1/2) 2^(1-BEFFT_PREC) = 2^-BEFFT_PREC */
/* below we assume rounding mode is nearest */
#define BEFFT_LOG2U -BEFFT_PREC /* assume round to nearest */
#define BEFFT_U (ldexp(0x1, BEFFT_LOG2U))
/* underflow unit : log2 is BEFFT_EMIN - BEFFT_PREC + 1 */
#define BEFFT_LOG2A (BEFFT_EMIN - BEFFT_PREC + 1)
#define BEFFT_A (ldexp(0x1, BEFFT_LOG2A))

#define BEFFT_NEXT(A) nextafter(A, INFINITY)
#define BEFFT_PREV(A) nextafter(A, -INFINITY)

#ifdef BEFFT_NO_INLINE
#define BEFFT_INLINE static
#else
#define BEFFT_INLINE static inline
#endif

#ifdef __cplusplus
extern "C" {
#endif
 
typedef size_t be_size_t;

#ifndef uint 
typedef unsigned int uint;
#endif

#ifndef ulong 
typedef size_t ulong;
#endif

#ifndef slong 
typedef int64_t slong;
#endif

BEFFT_INLINE void * befft_malloc (size_t size) { return malloc(size); }
BEFFT_INLINE void * befft_calloc (size_t num, size_t size) { return calloc(num, size); }
BEFFT_INLINE void   befft_free   (void * ptr)  { free(ptr); }

/* implemented in src/base/track_except.c */
int befft_test_and_print_exception(int verbose, const char * preamble);
int befft_test_and_print_all_exception(int verbose, const char * preamble);

#ifdef __cplusplus
}
#endif

#endif

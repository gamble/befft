/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft_base.h"

int befft_test_and_print_exception(int verbose, const char * preamble) {
    int double_exception = fetestexcept(FE_UNDERFLOW | FE_OVERFLOW | FE_INVALID);
    if (verbose && double_exception) {
        printf("%s: ", preamble);
        if ( double_exception & FE_UNDERFLOW )
            printf("FE_UNDERFLOW ");
        if ( double_exception & FE_OVERFLOW )
            printf("FE_OVERFLOW ");
        if ( double_exception & FE_INVALID )
            printf("FE_INVALID ");
        printf("\n");
    }
    return double_exception;
}

int befft_test_and_print_all_exception(int verbose, const char * preamble) {
    int double_exception = fetestexcept(FE_ALL_EXCEPT);
    if (verbose && double_exception) {
        printf("%s: ", preamble);
		if ( double_exception & FE_DIVBYZERO )
            printf("FE_DIVBYZERO ");
		if ( double_exception & FE_INEXACT )
            printf("FE_INEXACT ");
		if ( double_exception & FE_INVALID )
            printf("FE_INVALID ");
		if ( double_exception & FE_OVERFLOW )
            printf("FE_OVERFLOW ");
        if ( double_exception & FE_UNDERFLOW )
            printf("FE_UNDERFLOW ");
      
        printf("\n");
    }
    return double_exception;
}

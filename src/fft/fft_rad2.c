/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"

/* assume j<= 2^n-1 */
be_size_t reverse ( be_size_t j, uint n ) {
    be_size_t res = 0;
    for (uint k=0; k<n; k++) {
        res += BEFFT_KTHBIT(j,k)<<(n-1-k);
    }
    return res;
}

void befft_fft_rad2_init( befft_fft_rad2_t rad2, uint log2size ){
    be_size_t N   = ((be_size_t) 0x1) << log2size;
    be_size_t No2 = N>>1;
    
    befft_fft_rad2_log2sizeref(rad2) = log2size;
    befft_fft_rad2_fft_treeref(rad2) = (be_size_t *) befft_malloc (N*sizeof(be_size_t));
    for (be_size_t j=0; j<N; j++)
        befft_fft_rad2_fft_treeref(rad2)[j] = reverse(j,log2size);
    
    be_vec_init(befft_fft_rad2_omegasref(rad2), No2);
    be_vec_omega_uint( befft_fft_rad2_omegasref(rad2), log2size-1 ); /* get the No2-th order roots of units */
    
    befft_fft_rad2_omegassref(rad2) = (be_vec_ptr) befft_malloc (log2size*sizeof(be_vec_struct));
    for (uint k=1; k<=log2size; k++) {
        be_size_t Ko2   = ((be_size_t) 0x1)<<(k-1);
        be_size_t shift = ((be_size_t) 0x1)<<(log2size-k); 
        be_vec_init( befft_fft_rad2_omegassref(rad2) + (k-1), Ko2);
        for (be_size_t j = 0; j < Ko2; j++){
            be_size_t jshifted = j*shift;
            be_vec_realref(befft_fft_rad2_omegassref(rad2) + (k-1))[j] = be_vec_realref(befft_fft_rad2_omegasref(rad2))[jshifted];
            be_vec_imagref(befft_fft_rad2_omegassref(rad2) + (k-1))[j] = be_vec_imagref(befft_fft_rad2_omegasref(rad2))[jshifted];
        }
        be_vec_aberref(befft_fft_rad2_omegassref(rad2) + (k-1)) = be_vec_aberref(befft_fft_rad2_omegasref(rad2));
    }
}

/* let omegas be the vector in \C^(2^(log2size-1)) defined as:                                              */
/* omegas = [ omega(0, 2^(log2size)), ..., omega(2^(log2size-1)-1, 2^(log2size)) ]                          */
/* where omega(j, k) = e^(-j*2*Pi/k)                                                                        */
/* let omegas_re contain double approx of the real parts of omegas,                                         */
/* let omegas_im contain double approx of the imaginary parts of omegas                                     */
/* and omegas_ae the maximum absolute/relative error:                                                       */
/* forall j\in[0,2^(log2size-1)-1],                                                                         */
/*            (omegas_re[j] + Iomega_im[j])  = omega(j, k) + \delta with \delta\in\C and |\delta|<=omega_ae */
/*        <=> (omegas_re[j] + Iomega_im[j]) <= (1+omega_ae)*omega(j, k)                                     */
void befft_fft_rad2_init_with_omegas( befft_fft_rad2_t rad2, 
                                      double omegas_re[], double omegas_im[], double omegas_ae, 
                                      uint log2size ){
    be_size_t N   = ((be_size_t) 0x1) << log2size;
    be_size_t No2 = N>>1;
    
    befft_fft_rad2_log2sizeref(rad2) = log2size;
    befft_fft_rad2_fft_treeref(rad2) = (be_size_t *) befft_malloc (N*sizeof(be_size_t));
    for (be_size_t j=0; j<N; j++)
        befft_fft_rad2_fft_treeref(rad2)[j] = reverse(j,log2size);
    
    be_vec_init(befft_fft_rad2_omegasref(rad2), No2);
    
    befft_fft_rad2_omegassref(rad2) = (be_vec_ptr) befft_malloc (log2size*sizeof(be_vec_struct));
    for (uint k=1; k<=log2size; k++) {
        be_size_t Ko2   = ((be_size_t) 0x1)<<(k-1);
        be_size_t shift = ((be_size_t) 0x1)<<(log2size-k); 
        be_vec_init( befft_fft_rad2_omegassref(rad2) + (k-1), Ko2);
        for (be_size_t j = 0; j < Ko2; j++){
            be_size_t jshifted = j*shift;
            be_vec_realref(befft_fft_rad2_omegassref(rad2) + (k-1))[j] = omegas_re[jshifted];
            be_vec_imagref(befft_fft_rad2_omegassref(rad2) + (k-1))[j] = omegas_im[jshifted];
        }
        be_vec_aberref(befft_fft_rad2_omegassref(rad2) + (k-1)) = omegas_ae;
    }
}

void befft_fft_rad2_clear( befft_fft_rad2_t rad2 ){
    befft_free(befft_fft_rad2_fft_treeref(rad2));
    be_vec_clear(befft_fft_rad2_omegasref(rad2));
    
    for (uint k=1; k<=befft_fft_rad2_log2sizeref(rad2); k++)
        be_vec_clear( befft_fft_rad2_omegassref(rad2) + (k-1) );
    befft_free( befft_fft_rad2_omegassref(rad2));
}

void fprint_basis2(FILE *file, be_size_t j){
    uint nbbits = 8*sizeof(be_size_t);
    fprintf(file, "length: %u ", nbbits);
    for (uint k=0; k<nbbits; k++) {
        fprintf(file, "%zu", BEFFT_KTHBIT(j,k) );
    }
}

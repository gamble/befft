/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"
/* Preconditions: exact(ar+Iai) = (ar+Iai) + epsa with epsa\in C and ||epsa||<=exact(ae), ae=exact(ae)(1+u)^ak */ 
/*              : exact(br+Ibr) = (br+Ibi) + epsb with epsb\in C and ||epsb||<=exact(be), be=exact(be)(1+u)^bk */ 
/* Postcondition: returns ck, cr, ci, ce such that exact(cr+Icr) = exact( exact(ar+Iai) * exact(br+Ibr) ) and  */
/*                exact(cr+Icr) = (cr+Ici) + epsc with epsc\in C and ||epsc||<=exact(ce), ce=exact(ce)(1+u)^ck */
/*                and ck = MAX( (max(2,bk+1)+ak+3), (max(2,ak+1) + bk +2), (ak+bk+2) ) */
void _be_mul_aber_fast_without_corr( double * cr, double * ci, double * ce, 
                                     double   ar, double   ai, double   ae, /*uint ak,*/
                                     double   br, double   bi, double   be  /*uint bk */){
    _be_mul( cr, ci,    // let d = (ar+Iai)*(br+Ibi)
             ar, ai,    // then c = (1+f)d with f\in\C and ||f||<=sqrt(5)*u
             br, bi );
    double u = BEFFT_U;                                    /* is exact */
    double twonorm_ub = _be_twonorm_ub_fast( *cr, *ci );   /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast      */
                                                           /* ie twonorm_ub <= (1+u)exact(twonorm_ub)           */
    *ce = BEFFT_RELER_be_mul*u;                            /* mult by u is exact, thus ce = exact(ce)           */
    *ce = (*ce)*twonorm_ub;                                /* ce = (1+u)*exact( ce*twonorm_ub )                 */
    /* ce <= (1+u)*exact( ce*twonorm_ub ) <= (1+u)^2*exact(ce)*exact(twonorm_ub) */
    double normb   = _be_twonorm_ub_fast( br, bi );        /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast      */
                                                           /* ie normb <= (1+u)exact(normb)                     */
           normb   = normb + be;                           /*    normb <= (1+u)(normb + be) <= (1+u)^2normb + (1+u)^(bk+1)exact(be)  */
                                                           /*          <= (1+u)^max(2,bk+1) (exact(normb)+exact(be)) */
    double normbae = normb*ae;                             /* normbae  <= (1+u)exact(normb*ae)                  */
//     normbae = ( ||b|| + be )*ae = ||b||*ae + be*ae
    *ce = (*ce) + normbae;                                 /* ce       <= (1+u)exact(ce + normbae) */
    /* ce <= (1+u)exact(ce + normbae)                                   */
    /*    <= (1+u)ce                             + (1+u)normbae         */
    /*    <= (1+u)^3exact(ce)*exact(twonorm_ub)  + (1+u)^2exact(normb*ae) */
    /*    <= (1+u)^3exact(ce)*exact(twonorm_ub)  + (1+u)^2normb*ae        */
    /*    <= (1+u)^3exact(ce)*exact(twonorm_ub)  + (1+u)^(2+max(2,bk+1))(exact(normb)+exact(be))*ae */
    /*    <= (1+u)^3exact(ce)*exact(twonorm_ub)  + (1+u)^(max(2,bk+1)+ak+2)(exact(normb)+exact(be))*exact(ae) */
    /*    <= (1+u)^(max(2,bk+1)+ak+2) * ( exact(ce)*exact(twonorm_ub) + (exact(normb)+exact(be))*exact(ae)    */
    double norma   = _be_twonorm_ub_fast( ar, ai );        /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast */
                                                           /* ie norma <= (1+u)exact(norma)                */
           norma   = norma + ae;                           /*    norma <= (1+u)exact(norma + ae) <= (1+u)^2norma + (1+u)^(ak+1)exact(ae) */
                                                           /*          <= (1+u)^max(2,ak+1) (exact(normb) + exact(ae)) */
    double normabe = norma*be;                             /* normabe  <= (1+u)exact(norma*be)             */
//     normabe = ( ||a|| + ae )*be = ||a||*be + be*ae
    *ce = (*ce) + normabe;                                 /* ce       <= (1+u)exact(ce + normabe)         */
    /* ce <= (1+u)exact(ce + normabe)         */
    /*    <= (1+u)ce + (1+u)normabe          */
    /*    <= (1+u)ce + (1+u)^2exact(norma*be) */
    /*    <= (1+u)ce + (1+u)^2norma*be         */
    /*    <= (1+u)ce + (1+u)^(2+max(2,ak+1)(exact(normb) + exact(ae))*be */
    /*    <= (1+u)ce + (1+u)^(max(2,ak+1) + bk +2)(exact(normb) + exact(ae))*exact(be) */
    /*    <= (1+u)^(max(2,bk+1)+ak+3) * ( exact(ce)*exact(twonorm_ub) + (exact(normb)+exact(be))*exact(ae) */
    /*     + (1+u)^(max(2,ak+1) + bk +2)(exact(normb) + exact(ae))*exact(be) */
    /*    <= (1+u)^MAX( (max(2,bk+1)+ak+3), (max(2,ak+1) + bk +2) )                */
    /*     * ( exact(ce)*exact(twonorm_ub) + (exact(normb)+exact(be))*exact(ae) + (exact(normb) + exact(ae))*exact(be) ) */
    
    double aebe = ae*be;                                   /* aebe  <= (1+u)exact(ae*be) */
                                                           /*       <= (1+u)^(ak+bk+1)*exact(ae)*exact(be) */
    *ce = (*ce) + aebe;                                    /* ce    <= (1+u)exact(ce + aebe) */
    /* ce <= (1+u)exact(ce + aebe) */
    /*    <= (1+u)ce + (1+u)aebe   */
    /*    <= (1+u)ce + (1+u)^(ak+bk+2)*exact(ae)*exact(be) */
    /*    <= (1+u)^MAX( (max(2,bk+1)+ak+3), (max(2,ak+1) + bk +2), (ak+bk+2) )     */
    /*    * ( exact(ce)*exact(twonorm_ub) + (exact(normb)+exact(be))*exact(ae) + (exact(normb) + exact(ae))*exact(be)   */
    /*                                    + exact(ae)*exact(be) */
//     uint res1 = BEFFT_MAX(2, bk+1) + ak + 3;
//     uint res2 = BEFFT_MAX(2, ak+1) + bk + 2;
//     uint res  = BEFFT_MAX(res1, res2);
//     res = BEFFT_MAX( res, ak + bk + 2);
//     return res;
}

/* Preconditions: exact(ar+Iai) = (ar+Iai) + dela with dela\in C and ||dela||<=exact(ae), ae=exact(ae)(1+u)^ak */ 
/*              : exact(br+Ibr) = (br+Ibi) + delb with delb\in C and ||delb||<=exact(be), be=exact(be)(1+u)^bk */
/*                |exact(ar+Iai)|=1                                                                            */
/* Postcondition: returns ck, cr, ci, ce such that exact(cr+Icr) = exact( exact(ar+Iai) * exact(br+Ibr) ) and  */
/*                exact(cr+Icr) = (cr+Ici) + delc with delc\in C and ||delc||<=exact(ce), ce=exact(ce)(1+u)^ck */
/*                and ck = (ak + max(3,bk) + 3 ), ie ak+bk+3 when bk>=3                                           */
/* Formula:       write a = ar+Iai, b = br+Ibi, c = cr+Ici, ex for exact                                          */
/*                (1) ex(a) = a + dela <=> a = ex(a) + dela' => a = (1+dela'/ex(a))*ex(a)                         */
/*                    => a = (1+epsa)*ex(a) with epsa\in C and |epsa|<=ex(ae)                                     */
/*                (2) ex(b) = b + delb <=> b = ex(b) + delb' with delb'\in C and |delb'|<=ex(be)                  */
/*                (3) |ex(b)| <= |b| + |delb| <= |b| + be                                                         */
/*                (4) |ab - ex(ab)| <= sqrt(5)*u*|ab| <= sqrt(5)*u*|a|*|b|                                        */
/*                                  <= sqrt(5)*u*|1+epsa||b| <= sqrt(5)*u*(1+ae)*|b|                              */
/*                (5) |ex(ab) - ex(ex(a)*ex(b))| = |(1+epsa)*ex(a)*(ex(b)+delb') - ex(a)*ex(b) | (use (1) and (2))*/
/*                                              <= |delb'| + |ex(b)||epsa|+|delb'||epsa|                          */
/*                                              <= |delb'| + |b||epsa|+2*|delb'||epsa| (use (3) )                 */
/*                (6) |ab - ex(ex(a)*ex(b))| = |ab - ex(ab) + ex(ab) - ex(ex(a)*ex(b))|                           */
/*                                           <= |ab - ex(ab)| + |ex(ab) - ex(ex(a)*ex(b))|                        */
/*                                           <= sqrt(5)*u*(1+ae)|b| + |delb'| + |b||epsa|+2*|delb'||epsa|         */
/*                                              (use (4) and (5) )                                                */
/*                                           <= sqrt(5)*u*(1+ae)|b| + be + |b|ae + 2*ae*be                        */
/*                                           <= (sqrt(5)*u*(1+ae) + ae)*|b| + (1+2*ae)*be                         */
void _be_mul_mod1_aber_fast_without_corr( double * cr, double * ci, double * ce, 
                                          double   ar, double   ai, double   ae, /*uint ak,*/
                                          double   br, double   bi, double   be  /*uint bk */){
    _be_mul( cr, ci,    // let d = (ar+Iai)*(br+Ibi)
             ar, ai,    // then c = (1+f)d with f\in\C and ||f||<=sqrt(5)*u
             br, bi );
    double sqrt5u = BEFFT_RELER_be_mul*BEFFT_U;    /* mult by u is exact, thus sqrt5u <= (1+u)exact(sqrt(5)*u)  */
    double onepae = 1 + ae;                        /* onepae <= (1+u)exact(1 + ae) <= (1+u)^(ak+1)(1+exact(ae)) */
    double facto1 = sqrt5u*onepae;                 /* facto1 <= (1+u)exact(facto1) <= (1+u)sqrt5u*onepae        */
                                                   /*        <= (1+u)^(ak+2)exact(sqrt(5)*u*(1+exact(ae)))      */
           facto1 = facto1 + ae;                   /* facto1 <= (1+u)facto1 + (1+u)ae                           */
                                                   /*        <= (1+u)^(ak+3)exact(facto1)+(1+u)^(ak+1)exact(ae) */
                                                   /*        <= (1+u)^(ak+3)exact( sqrt(5)*u*(1+exact(ae)) + exact(ae) */                   
    double facto2 = 1 + 2*ae;                      /* facto2 <= (1+u)exact( 1 + 2*ae)                           */
                                                   /*        <= (1+u)^(ak+1)(1+exact(ae))                       */
    double unormb = _be_twonorm_ub_fast( br, bi ); /* unormb <= (1+u)|b|                                        */
    double uterm1 = facto1*unormb;                 /* uterm1 <= (1+u)exact(facto1*unormb)                       */
                                                   /*        <= (1+u)^(ak+5) * exact(facto1)*exact(unormb)      */
    double uterm2 = facto2*be;                     /* uterm2 <= (1+u)^(ak+bk+2)*exact(facto2)*exact(be)         */
    *ce = uterm1 + uterm2;                          /*     ce <= (1+u)^(max(ak+6 , ak+bk+3))*exact(ce)           */
                                                   /*     ce <= (1+u)^(ak + max(3,bk) + 3 )*exact(ce)           */
}

/* Preconditions: exact(ar+Iai) = (ar+Iai) + epsa with epsa\in C and ||epsa||<=exact(ae), ae=exact(ae)(1+u)^ak */ 
/*              : exact(br+Ibr) = (br+Ibi) + epsb with epsb\in C and ||epsb||<=exact(be), be=exact(be)(1+u)^bk */ 
/* Postcondition: returns ck, cr, ci, ce such that exact(cr+Icr) = exact( exact(ar+Iai) + exact(br+Ibr) ) and  */
/*                exact(cr+Icr) = (cr+Ici) + epsc with epsc\in C and ||epsc||<=exact(ce), ce=exact(ce)(1+u)^ck */
/*                and ck = MAX( 3, ak+2, bk+1 ) */
void _be_add_aber_fast_without_corr( double * cr, double * ci, double * ce, 
                                     double   ar, double   ai, double   ae, /*uint ak,*/
                                     double   br, double   bi, double   be  /*uint bk */){
    _be_add( cr, ci,      // cr = (1+e1)(ar+br) with |e1|<=u
             ar, ai,      // ci = (1+e2)(ai+bi) with |e2|<=u
             br, bi );    // thus letting d = (ar+br)+I(ai+bi), one has
                          // c  = (1+f)(d) with f\in\C and ||f||<=u 
                          // relative error is u*BEFFT_RELER_be_add i.e. u
    
    double u = BEFFT_U;                                  /* is exact */
    double twonorm_ub = _be_twonorm_ub_fast( *cr, *ci ); /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast */
                                                         /* ie twonorm_ub < (1+u)exact(twonorm_ub)       */
    *ce = twonorm_ub*BEFFT_RELER_be_add*u;               /* mult by u is exact, thus ce <= (1+u)exact(ce)*/  
    *ce=(*ce) + ae;                                      /* ce <= (1+u)exact(ce + ae) */
    /* ce <= (1+u)ce + (1+u)ae */
    /*    <= (1+u)^2exact(ce) + (1+u)^(ak+1)exact(ae)  */
    *ce=(*ce) + be;                                      /* ce <= (1+u)exact(ce + be) */
    /* ce <= (1+u)ce + (1+u)be */
    /*    <= (1+u)ce + (1+u)^(bk+1)exact(be)           */
    /*    <= (1+u)^3exact(ce) + (1+u)^(ak+2)exact(ae) + (1+u)^(bk+1)exact(be) */
    /*    <= (1+u)^( MAX( 3, ak+2, bk+1 ) )*(exact(ce) + exact(ae) + exact(be) ) */
//     uint res = BEFFT_MAX( ak+2, bk+1);
//     res = BEFFT_MAX( res, 3 );
//     return res;
}

void _be_sub_aber_fast_without_corr( double * cr, double * ci, double * ce, 
                                     double   ar, double   ai, double   ae, /*uint ak,*/
                                     double   br, double   bi, double   be  /*uint bk */){
    _be_sub( cr, ci,      // cr = (1+e1)(ar+br) with |e1|<=u
             ar, ai,      // ci = (1+e2)(ai+bi) with |e2|<=u
             br, bi );    // thus letting d = (ar+br)+I(ai+bi), one has
                          // c  = (1+f)(d) with f\in\C and ||f||<=u 
                          // relative error is u*BEFFT_RELER_be_add i.e. u
    
    double u = BEFFT_U;                                  /* is exact */
    double twonorm_ub = _be_twonorm_ub_fast( *cr, *ci ); /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast */
                                                         /* ie twonorm_ub < (1+u)exact(twonorm_ub)       */
    *ce = twonorm_ub*BEFFT_RELER_be_add*u;               /* mult by u is exact, thus ce <= (1+u)exact(ce)*/  
    *ce=(*ce) + ae;                                      /* ce <= (1+u)exact(ce + ae) */
    /* ce <= (1+u)ce + (1+u)ae */
    /*    <= (1+u)^2exact(ce) + (1+u)^(ak+1)exact(ae)  */
    *ce=(*ce) + be;                                      /* ce <= (1+u)exact(ce + be) */
    /* ce <= (1+u)ce + (1+u)be */
    /*    <= (1+u)ce + (1+u)^(bk+1)exact(be)           */
    /*    <= (1+u)^3exact(ce) + (1+u)^(ak+2)exact(ae) + (1+u)^(bk+1)exact(be) */
    /*    <= (1+u)^( MAX( 3, ak+2, bk+1 ) )*(exact(ce) + exact(ae) + exact(be) ) */
//     uint res = BEFFT_MAX( ak+2, bk+1);
//     res = BEFFT_MAX( res, 3 );
//     return res;
}

/* special case where k=1: only mults by 1 */
void befft_FirstStep_dynamic ( double * restrict y_real, double * restrict y_imag, double * restrict y_aber,
                               double * restrict x_real, double * restrict x_imag, double * restrict x_aber,
                               uint n){
    ulong N   = ((ulong) 0x1)<<n;
    double u = BEFFT_U;
    for (ulong j = 0; j<N; j+=2) {
//         _be_add_aber_fast_without_corr( y_real + j,     y_imag + j,     y_aber + j,
//                                         x_real[j],      x_imag[j],      x_aber[j],     
//                                         x_real[j+1],    x_imag[j+1],    x_aber[j+1]   );
        y_real[j  ] = x_real[j] + x_real[j+1];
        y_imag[j  ] = x_imag[j] + x_imag[j+1];
        double twonorm_ub = _be_twonorm_ub_fast( y_real[j  ], y_imag[j  ] );
        y_aber[j  ] = twonorm_ub*BEFFT_RELER_be_add*u;
        double temp = x_aber[j] + x_aber[j+1];
        y_aber[j  ] += temp;
//         _be_sub_aber_fast_without_corr( y_real + (j+1), y_imag + (j+1), y_aber + (j+1),
//                                         x_real[j],      x_imag[j],      x_aber[j],     
//                                         x_real[j+1],    x_imag[j+1],    x_aber[j+1]   );
        y_real[j+1] = x_real[j] - x_real[j+1];
        y_imag[j+1] = x_imag[j] - x_imag[j+1];
               twonorm_ub = _be_twonorm_ub_fast( y_real[j+1], y_imag[j+1] );
        y_aber[j+1] = twonorm_ub*BEFFT_RELER_be_add*u;
        y_aber[j+1] += temp;
        /* one has exact(y_aber[j])<=y_aber[j]*(1+u)^MAX( ak+2, bk+1, 3) = 3 when ak=bk=0       */
        /* the same for exact(y_aber[j+1]) */
    }
}

/* special case where k=2: only mults by 1 and -i */
void befft_SecondStep_dynamic ( double * restrict y_real, double * restrict y_imag, double * restrict y_aber,
                                double * restrict x_real, double * restrict x_imag, double * restrict x_aber,
                                uint n){
    ulong N   = ((ulong) 0x1)<<n;
    double u = BEFFT_U;
    for (ulong j = 0; j<N; j+=4) {
//         _be_add_aber_fast_without_corr( y_real + j,     y_imag + j,     y_aber + j,
//                                         x_real[j],      x_imag[j],      x_aber[j],     
//                                         x_real[j+2],    x_imag[j+2],    x_aber[j+2]   );
        y_real[j  ] = x_real[j] + x_real[j+2];
        y_imag[j  ] = x_imag[j] + x_imag[j+2];
        double twonorm_ub = _be_twonorm_ub_fast( y_real[j  ], y_imag[j  ] );
        y_aber[j  ] = twonorm_ub*BEFFT_RELER_be_add*u;
        double temp1 = x_aber[j] + x_aber[j+2];
        y_aber[j  ] += temp1;
        
//         _be_add_aber_fast_without_corr( y_real + (j+1), y_imag + (j+1), y_aber + (j+1),
//                                         x_real[j+1],    x_imag[j+1],    x_aber[j+1],   
//                                         x_imag[j+3],   -x_real[j+3],    x_aber[j+3]   );
        y_real[j+1] = x_real[j+1] + x_imag[j+3];
        y_imag[j+1] = x_imag[j+1] - x_real[j+3];
               twonorm_ub = _be_twonorm_ub_fast( y_real[j+1], y_imag[j+1] );
        y_aber[j+1] = twonorm_ub*BEFFT_RELER_be_add*u;
        double temp2 = x_aber[j+1] + x_aber[j+3];
        y_aber[j+1] += temp2;
        
//         _be_sub_aber_fast_without_corr( y_real + (j+2), y_imag + (j+2), y_aber + (j+2),
//                                         x_real[j],      x_imag[j],      x_aber[j],     
//                                         x_real[j+2],    x_imag[j+2],    x_aber[j+2]   );
        y_real[j+2] = x_real[j  ] - x_real[j+2];
        y_imag[j+2] = x_imag[j  ] - x_imag[j+2];
               twonorm_ub = _be_twonorm_ub_fast( y_real[j+2], y_imag[j+2] );
        y_aber[j+2] = twonorm_ub*BEFFT_RELER_be_add*u;
        y_aber[j+2] += temp1;
        
//         _be_sub_aber_fast_without_corr( y_real + (j+3), y_imag + (j+3), y_aber + (j+3),
//                                         x_real[j+1],    x_imag[j+1],    x_aber[j+1],   
//                                         x_imag[j+3],   -x_real[j+3],    x_aber[j+3]   );
        y_real[j+3] = x_real[j+1] - x_imag[j+3];
        y_imag[j+3] = x_imag[j+1] + x_real[j+3];
               twonorm_ub = _be_twonorm_ub_fast( y_real[j+3], y_imag[j+3] );
        y_aber[j+3] = twonorm_ub*BEFFT_RELER_be_add*u;
        y_aber[j+3] += temp2;
        
        /* one has exact(y_aber[j])<=(1+u)^MAX( ak+2, bk+1, 3) = 5 when ak=bk=3       */
    }
}

void befft_OneStep_dynamic(  double * restrict y_real, double * restrict y_imag, double * restrict y_aber,
                             double * restrict x_real, double * restrict x_imag, double * restrict x_aber,
                             befft_fft_rad2_t rad2,
                             uint k){
    uint n    = befft_fft_rad2_log2sizeref(rad2);
    ulong Ko2 = ((ulong) 0x1)<<(k-1);
    ulong K   = ((ulong) 0x1)<<k;     /* the size of each independent fft */
    ulong Nb  = ((ulong) 0x1)<<(n-k); /* = N/K := number of independent order K fft */
    double *omega_re = be_vec_realref(befft_fft_rad2_omegassref(rad2) + (k-1));
    double *omega_im = be_vec_imagref(befft_fft_rad2_omegassref(rad2) + (k-1));
    double  omega_ae = be_vec_aberref(befft_fft_rad2_omegassref(rad2) + (k-1)); /*the max of abs errors on root of units */
    double mul_re, mul_im, mul_aber;
    double u = BEFFT_U;
    double sqrt5u = BEFFT_RELER_be_mul*u;
    double onepae = 1 + omega_ae;
    double facto1 = sqrt5u*onepae;
           facto1 = facto1 + omega_ae;
    double facto2 = 1 + 2*omega_ae;
    for (ulong nb = 0; nb < Nb; nb++){
        ulong fi = nb*K;
    
        for (ulong j = 0; j < Ko2; j++){
            ulong j1 = j + fi;
            ulong j2 = j1 + Ko2;
//             ulong j3 = j*Nb;
            
//             _be_mul_aber_fast_without_corr( &mul_re, &mul_im, &mul_aber,
//                                             omega_re[j], omega_im[j], omega_ae,
//                                             x_real[j2], x_imag[j2], x_aber[j2] );
            _be_mul( &mul_re, &mul_im,
                     omega_re[j], omega_im[j],
                     x_real[j2], x_imag[j2] );

            double unormb = _be_twonorm_ub_fast( x_real[j2], x_imag[j2] );
            double uterm1 = facto1*unormb;
            double uterm2 = facto2*x_aber[j2];
                 mul_aber = uterm1 + uterm2;
//             _be_add_aber_fast_without_corr( y_real + j1, y_imag + j1, y_aber + j1,
//                                             x_real[j1], x_imag[j1], x_aber[j1],
//                                             mul_re, mul_im, mul_aber );
            _be_add( y_real + j1, y_imag + j1,
                     x_real[j1], x_imag[j1],
                     mul_re, mul_im );
            double normd = _be_twonorm_ub_fast( y_real[j1  ], y_imag[j1  ] );
            double temp  = x_aber[j1] + mul_aber;
            y_aber[j1  ] = normd*BEFFT_RELER_be_add*u;
            y_aber[j1  ] += temp;
            
//             _be_sub_aber_fast_without_corr( y_real + j2, y_imag + j2, y_aber + j2,
//                                             x_real[j1], x_imag[j1], x_aber[j1],
//                                             mul_re, mul_im, mul_aber );
            
            _be_sub( y_real + j2, y_imag + j2,
                     x_real[j1], x_imag[j1],
                     mul_re, mul_im );
            double norme = _be_twonorm_ub_fast( y_real[j2  ], y_imag[j2  ] );
            y_aber[j2  ] = norme*BEFFT_RELER_be_add*u;
            y_aber[j2  ] += temp;
            
            /* NEW analysis: */
            /* one has kmul = ak + max(3,bk) + 3 */
            /* when k=3, ak = 0 and bk = 5,  thus kmul = 8 */
            /*                               then ck=MAX( 3, bk+2, kmul+1 ) = 9 */
            /* when k=4, ak = 0 and bk = 9,  thus kmul = 12*/
            /*                               then ck=MAX( 3, bk+2, kmul+1 ) = 13 */
            /* when k>2,                         = error in ulp on x_aber[j] + 4 */ 
        }
    }
}

int befft_fft_rad2_dynamic_precomp     ( double * restrict y_real, double * restrict y_imag, double * restrict y_aber,
                                         double * restrict x_real, double * restrict x_imag, double * restrict x_aber,
                                         befft_fft_rad2_t rad2 ){
    
    uint  n = befft_fft_rad2_log2sizeref(rad2);
    be_size_t * fft_tree = befft_fft_rad2_fft_treeref(rad2);
//     be_vec_ptr omegajNo2s = befft_fft_rad2_omegasref(rad2);
    
    double u = BEFFT_U;
    be_size_t N = ((be_size_t) 0x1) << n;
    
//     uint * y_corr = (uint *) befft_malloc (N*sizeof(uint));
    for (be_size_t j=0; j<N; j++) {
        y_real[j] = x_real[fft_tree[j]];
        y_imag[j] = x_imag[fft_tree[j]];
        y_aber[j] = x_aber[fft_tree[j]];
//         y_corr[j] = 0;
    }
    
    double * z_real = (double *) befft_malloc (N*sizeof(double));
    double * z_imag = (double *) befft_malloc (N*sizeof(double));
    double * z_aber = (double *) befft_malloc (N*sizeof(double));
//     uint   * z_corr = (uint *)   befft_malloc (N*sizeof(uint));
    
    int underflow = 0;
    feclearexcept (FE_ALL_EXCEPT);
    
    if (n>=1)
        befft_FirstStep_dynamic(z_real, z_imag, z_aber, y_real, y_imag, y_aber, n);
    if (n>=2)
        befft_SecondStep_dynamic(y_real, y_imag, y_aber, z_real, z_imag, z_aber, n);
    
    for (uint k=3; k<=n; k++){
        if (k%2)
            befft_OneStep_dynamic(z_real, z_imag, z_aber, y_real, y_imag, y_aber, rad2, k);
        else
            befft_OneStep_dynamic(y_real, y_imag, y_aber, z_real, z_imag, z_aber, rad2, k);
    }
    if (n%2) { 
        memcpy(y_real, z_real, N*sizeof(double));
        memcpy(y_imag, z_imag, N*sizeof(double));
        memcpy(y_aber, z_aber, N*sizeof(double));
//         memcpy(y_corr, z_corr, N*sizeof(uint));
    }
    /* correct the errors */
    /* general formula for corr[n]: corr[0] = 0, corr[1] = 3, corr[2] = 5, corr[k+1] = corr[k] + 4 for k>=2 */
    /*                     corr[n] = 5 + (n-2)*4 for n>=2 */
    uint corr = 5 + (n-2)*4;
    if (n==0)
        corr = 0;
    if (n==1)
        corr = 3;
    /* if (n>=2) */
    /*    corr = 5 + (n-2)*4; */
    for (ulong j=0; j<N; j++) {
//         printf("y_corr[%lu]: %u, should be %u\n", j, y_corr[j], corr);
//         y_aber[j] = y_aber[j]/(1-(y_corr[j]+1)*u);
        y_aber[j] = y_aber[j]/(1-(corr+1)*u);
    }
    underflow = !!fetestexcept(FE_UNDERFLOW);
    
//     befft_free(y_corr);
    befft_free(z_real);
    befft_free(z_imag);
    befft_free(z_aber);
//     befft_free(z_corr);
    
    return underflow;
    
}

int befft_fft_rad2_dynamic    ( double * restrict y_real, double * restrict y_imag, double * restrict y_aber,
                                 double * restrict x_real, double * restrict x_imag, double * restrict x_aber, 
                                 uint n) {
    befft_fft_rad2_t rad2;
    befft_fft_rad2_init(rad2, n);
    
    int underflow = befft_fft_rad2_dynamic_precomp( y_real, y_imag, y_aber, x_real, x_imag, x_aber, rad2);
    
    befft_fft_rad2_clear(rad2);
    return underflow;
}

int befft_fft_rad2_dynamic_gathered    ( double * restrict y,
                                         double * restrict x, 
                                         uint n) {
    befft_fft_rad2_t rad2;
    befft_fft_rad2_init(rad2, n);
    
    int underflow = befft_fft_rad2_dynamic_precomp_gathered( y, x, rad2);
    
    befft_fft_rad2_clear(rad2);
    return underflow;
}

int befft_fft_rad2_dynamic_precomp_gathered     ( double * restrict y,
                                                  double * restrict x,
                                                  befft_fft_rad2_t rad2 ){
    
    uint  n = befft_fft_rad2_log2sizeref(rad2);
    be_size_t * fft_tree = befft_fft_rad2_fft_treeref(rad2);
//     be_vec_ptr omegajNo2s = befft_fft_rad2_omegasref(rad2);
    
    double u = BEFFT_U;
    be_size_t N = ((be_size_t) 0x1) << n;
    
    double * xx = (double *) befft_malloc ( 3*N*sizeof(double) );
    double * yy = (double *) befft_malloc ( 3*N*sizeof(double) );
    double * x_real = xx + 0;
    double * x_imag = xx + N;
    double * x_aber = xx + 2*N;
    double * y_real = yy + 0;
    double * y_imag = yy + N;
    double * y_aber = yy + 2*N;
        
//     uint * y_corr = (uint *) befft_malloc (N*sizeof(uint));
    for (be_size_t j=0; j<N; j++) {
        x_real[j] = x[3*j];
        x_imag[j] = x[3*j+1];
        x_aber[j] = x[3*j+2];
        y_real[j] = x[3*fft_tree[j]];
        y_imag[j] = x[3*fft_tree[j]+1];
        y_aber[j] = x[3*fft_tree[j]+2];
//         y_corr[j] = 0;
    }
    
    double * z_real = (double *) befft_malloc (N*sizeof(double));
    double * z_imag = (double *) befft_malloc (N*sizeof(double));
    double * z_aber = (double *) befft_malloc (N*sizeof(double));
//     uint   * z_corr = (uint *)   befft_malloc (N*sizeof(uint));
    
    int underflow = 0;
    feclearexcept (FE_ALL_EXCEPT);
    
    if (n>=1)
        befft_FirstStep_dynamic(z_real, z_imag, z_aber, y_real, y_imag, y_aber, n);
    if (n>=2)
        befft_SecondStep_dynamic(y_real, y_imag, y_aber, z_real, z_imag, z_aber, n);
    
    for (uint k=3; k<=n; k++){
        if (k%2)
            befft_OneStep_dynamic(z_real, z_imag, z_aber, y_real, y_imag, y_aber, rad2, k);
        else
            befft_OneStep_dynamic(y_real, y_imag, y_aber, z_real, z_imag, z_aber, rad2, k);
    }
    if (n%2) { 
        memcpy(y_real, z_real, N*sizeof(double));
        memcpy(y_imag, z_imag, N*sizeof(double));
        memcpy(y_aber, z_aber, N*sizeof(double));
    }
    /* correct the errors */
    /* general formula for corr[n]: corr[0] = 0, corr[1] = 3, corr[2] = 5, corr[k+1] = corr[k] + 4 for k>=2 */
    /*                     corr[n] = 5 + (n-2)*4 for n>=2 */
    uint corr = 5 + (n-2)*4;
    if (n==0)
        corr = 0;
    if (n==1)
        corr = 3;
    /*if (n>=2) */
    /*    corr = 5 + (n-2)*4; */
    for (be_size_t j=0; j<N; j++) {
//         printf("y_corr[%lu]: %u, should be %u\n", j, y_corr[j], corr);
//         y_aber[j] = y_aber[j]/(1-(y_corr[j]+1)*u);
        y_aber[j] = y_aber[j]/(1-(corr+1)*u);
    }
    underflow = !!fetestexcept(FE_UNDERFLOW);
    
    for (be_size_t j=0; j<N; j++) {
         y[3*j]   = y_real[j];
         y[3*j+1] = y_imag[j];
         y[3*j+2] = y_aber[j];
    }
    
//     befft_free(y_corr);
    befft_free(z_real);
    befft_free(z_imag);
    befft_free(z_aber);
//     befft_free(z_corr);
    
    befft_free(xx);
    befft_free(yy);
    
    return underflow;
    
}

/* For memory */

/* special case where k=1: only mults by 1 */
void befft_FirstStep_dynamic_old( double * restrict y_real, double * restrict y_imag, double * restrict y_aber,
                              double * restrict x_real, double * restrict x_imag, double * restrict x_aber,
                              uint n){
    be_size_t N   = ((be_size_t) 0x1)<<n;
    double u = BEFFT_U;
    for (be_size_t j = 0; j<N; j+=2) {
//         _be_add_aber( y_real + j,     y_imag + j,     y_aber + j,
//                       x_real[j],      x_imag[j],      x_aber[j],
//                       x_real[j+1],    x_imag[j+1],    x_aber[j+1] );
        _be_add( y_real + j,     y_imag + j,
                 x_real[j],      x_imag[j],
                 x_real[j+1],    x_imag[j+1] );
        double twonorm_ub1 = _be_twonorm_ub_fast( y_real[j], y_imag[j] );
        y_aber[j] = twonorm_ub1*BEFFT_RELER_be_add*u;
        y_aber[j] = y_aber[j] + x_aber[j];
        y_aber[j] = y_aber[j] + x_aber[j+1];
        y_aber[j] = y_aber[j]/(1-4*u);
//         _be_sub_aber( y_real + (j+1), y_imag + (j+1), y_aber + (j+1),
//                       x_real[j],      x_imag[j],      x_aber[j],
//                       x_real[j+1],    x_imag[j+1],    x_aber[j+1] );
        _be_sub( y_real + (j+1), y_imag + (j+1),
                 x_real[j],      x_imag[j],
                 x_real[j+1],    x_imag[j+1] );
        double twonorm_ub2 = _be_twonorm_ub_fast( y_real[j+1], y_imag[j+1] );
        y_aber[j+1] = twonorm_ub2*BEFFT_RELER_be_add*u;
        y_aber[j+1] = y_aber[j+1] + x_aber[j];
        y_aber[j+1] = y_aber[j+1] + x_aber[j+1];
        y_aber[j+1] = y_aber[j+1]/(1-4*u);
    }
}

/* special case where k=2: only mults by 1 and -i */
void befft_SecondStep_dynamic_old( double * restrict y_real, double * restrict y_imag, double * restrict y_aber,
                               double * restrict x_real, double * restrict x_imag, double * restrict x_aber, 
                               uint n){
    be_size_t N   = ((be_size_t) 0x1)<<n;
    double u = BEFFT_U;
    for (be_size_t j = 0; j<N; j+=4) {
//         _be_add_aber( y_real + j,     y_imag + j,     y_aber + j,
//                       x_real[j],      x_imag[j],      x_aber[j],
//                       x_real[j+2],    x_imag[j+2],    x_aber[j+2] );
        _be_add( y_real + j,     y_imag + j,
                 x_real[j],      x_imag[j],
                 x_real[j+2],    x_imag[j+2] );
        double twonorm_ub1 = _be_twonorm_ub_fast( y_real[j], y_imag[j] );
        y_aber[j] = twonorm_ub1*BEFFT_RELER_be_add*u;
        y_aber[j] = y_aber[j] + x_aber[j];
        y_aber[j] = y_aber[j] + x_aber[j+2];
        y_aber[j] = y_aber[j]/(1-4*u);
//         _be_add_aber( y_real + (j+1), y_imag + (j+1), y_aber + (j+1),
//                       x_real[j+1],    x_imag[j+1],    x_aber[j+1],
//                       x_imag[j+3],   -x_real[j+3],     x_aber[j+3] );
        _be_add( y_real + (j+1), y_imag + (j+1),
                 x_real[j+1],    x_imag[j+1],
                 x_imag[j+3],   -x_real[j+3] );
        double twonorm_ub2 = _be_twonorm_ub_fast( y_real[j+1], y_imag[j+1] );
        y_aber[j+1] = twonorm_ub2*BEFFT_RELER_be_add*u;
        y_aber[j+1] = y_aber[j+1] + x_aber[j+1];
        y_aber[j+1] = y_aber[j+1] + x_aber[j+3];
        y_aber[j+1] = y_aber[j+1]/(1-4*u);
//         _be_sub_aber( y_real + (j+2), y_imag + (j+2), y_aber + (j+2),
//                       x_real[j],      x_imag[j],      x_aber[j],
//                       x_real[j+2],    x_imag[j+2],    x_aber[j+2] );
        _be_sub( y_real + (j+2), y_imag + (j+2),
                 x_real[j],      x_imag[j],
                 x_real[j+2],    x_imag[j+2] );
        double twonorm_ub3 = _be_twonorm_ub_fast( y_real[j+2], y_imag[j+2] );
        y_aber[j+2] = twonorm_ub3*BEFFT_RELER_be_add*u;
        y_aber[j+2] = y_aber[j+2] + x_aber[j];
        y_aber[j+2] = y_aber[j+2] + x_aber[j+2];
        y_aber[j+2] = y_aber[j+2]/(1-4*u);
//         _be_sub_aber( y_real + (j+3), y_imag + (j+3), y_aber + (j+3),
//                       x_real[j+1],    x_imag[j+1],    x_aber[j+1],
//                       x_imag[j+3],    -x_real[j+3],   x_aber[j+3] );
        _be_sub( y_real + (j+3), y_imag + (j+3),
                 x_real[j+1],    x_imag[j+1],
                 x_imag[j+3],   -x_real[j+3] );
        double twonorm_ub4 = _be_twonorm_ub_fast( y_real[j+3], y_imag[j+3] );
        y_aber[j+3] = twonorm_ub4*BEFFT_RELER_be_add*u;
        y_aber[j+3] = y_aber[j+3] + x_aber[j+1];
        y_aber[j+3] = y_aber[j+3] + x_aber[j+3];
        y_aber[j+3] = y_aber[j+3]/(1-4*u);
    }
}

void befft_OneStep_dynamic_old( double y_real[], double y_imag[], double y_aber[],
                            double x_real[], double x_imag[], double x_aber[],
                            be_vec_t omegajNo2s,
                            uint k, uint n){
    be_size_t Ko2 = ((be_size_t) 0x1)<<(k-1);
    be_size_t K   = ((be_size_t) 0x1)<<k;     /* the size of each independent fft */
    be_size_t Nb  = ((be_size_t) 0x1)<<(n-k); /* = N/K := number of independent order K fft */
    double mul_re, mul_im, mul_aber;
    for (be_size_t nb = 0; nb < Nb; nb++){
        be_size_t fi = nb*K;
        for (be_size_t j = 0; j < Ko2; j++){
            be_size_t j1 = j + fi;
            be_size_t j2 = j1 + Ko2;
            be_size_t j3 = j*Nb;
            
            _be_mul_aber( &mul_re, &mul_im, &mul_aber,
                           be_vec_realref(omegajNo2s)[j3], be_vec_imagref(omegajNo2s)[j3], be_vec_aberref(omegajNo2s),
                           x_real[j2], x_imag[j2], x_aber[j2] );
            _be_add_aber( y_real + j1, y_imag + j1, y_aber + j1,
                          x_real[j1], x_imag[j1], x_aber[j1],
                          mul_re, mul_im, mul_aber );
            _be_sub_aber( y_real + j2, y_imag + j2, y_aber + j2,
                          x_real[j1], x_imag[j1], x_aber[j1],
                          mul_re, mul_im, mul_aber );
            
        }
    }
}

int befft_fft_rad2_dynamic_old_precomp    ( double y_real[], double y_imag[], double y_aber[],
                                            double x_real[], double x_imag[], double x_aber[], befft_fft_rad2_t rad2 ){
    
    uint  n = befft_fft_rad2_log2sizeref(rad2);
    be_size_t * fft_tree = befft_fft_rad2_fft_treeref(rad2);
    be_vec_ptr omegajNo2s = befft_fft_rad2_omegasref(rad2);
    
    be_size_t N = ((be_size_t) 0x1) << n;
    
    for (be_size_t j=0; j<N; j++) {
        y_real[j] = x_real[fft_tree[j]];
        y_imag[j] = x_imag[fft_tree[j]];
        y_aber[j] = x_aber[fft_tree[j]];
    }
    
    double * z_real = (double *) befft_malloc (N*sizeof(double));
    double * z_imag = (double *) befft_malloc (N*sizeof(double));
    double * z_aber = (double *) befft_malloc (N*sizeof(double));
    
    int underflow = 0;
    feclearexcept (FE_ALL_EXCEPT);
    if (n>=1)
        befft_FirstStep_dynamic_old(z_real, z_imag, z_aber, y_real, y_imag, y_aber, n);
    if (n>=2)
        befft_SecondStep_dynamic_old(y_real, y_imag, y_aber, z_real, z_imag, z_aber, n);
    
    for (uint k=3; k<=n; k++){
        if (k%2)
            befft_OneStep_dynamic_old(z_real, z_imag, z_aber, y_real, y_imag, y_aber, omegajNo2s, k, n);
        else
            befft_OneStep_dynamic_old(y_real, y_imag, y_aber, z_real, z_imag, z_aber, omegajNo2s, k, n);
    }
    if (n%2) { 
        memcpy(y_real, z_real, N*sizeof(double));
        memcpy(y_imag, z_imag, N*sizeof(double));
        memcpy(y_aber, z_aber, N*sizeof(double));
    }
    underflow = !!fetestexcept(FE_UNDERFLOW);
    
    befft_free(z_real);
    befft_free(z_imag);
    befft_free(z_aber);
    
    return underflow;
    
}

int befft_fft_rad2_dynamic_old    ( double y_real[], double y_imag[], double y_aber[],
                                double x_real[], double x_imag[], double x_aber[], uint n) {
    befft_fft_rad2_t rad2;
    befft_fft_rad2_init(rad2, n);
    
    int underflow = befft_fft_rad2_dynamic_old_precomp( y_real, y_imag, y_aber, x_real, x_imag, x_aber, rad2);
    
    befft_fft_rad2_clear(rad2);
    return underflow;
}

/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"

/* special case where k=1: only mults by 1 */
double befft_FirstStep_static_vect( double * restrict y_real, double * restrict y_imag,
                                    double * restrict x_real, double * restrict x_imag,
                                    befft_fft_rad2_t rad2){
    uint n = befft_fft_rad2_log2sizeref(rad2);
    be_size_t No2  = ((be_size_t) 0x1)<<(n-1);
    double * x_real_1 = x_real;
    double * x_imag_1 = x_imag;
    double * x_real_2 = x_real + No2;
    double * x_imag_2 = x_imag + No2;
    double * y_real_1 = y_real;
    double * y_imag_1 = y_imag;
//     double * y_real_2 = y_real + No2;
//     double * y_imag_2 = y_imag + No2;
    double * y_real_2 = (double *) befft_malloc (No2*sizeof(double));
    double * y_imag_2 = (double *) befft_malloc (No2*sizeof(double));
    for (be_size_t j = 0; j<No2; j++) {
        y_real_1[j] = x_real_1[j] + x_real_2[j];
        y_imag_1[j] = x_imag_1[j] + x_imag_2[j];
        
        y_real_2[j] = x_real_1[j] - x_real_2[j];
        y_imag_2[j] = x_imag_1[j] - x_imag_2[j];
    }
    memcpy(y_real + No2, y_real_2, No2*sizeof(double));
    memcpy(y_imag + No2, y_imag_2, No2*sizeof(double));
    befft_free(y_real_2);
    befft_free(y_imag_2);
    double u   = ldexp(1, -BEFFT_PREC); /* is exact */
    return u;
}

/* special case where k=2: only mults by 1 and -i */
double befft_SecondStep_static_vect( double * restrict y_real, double * restrict y_imag,
                                     double * restrict x_real, double * restrict x_imag, 
                                     uint n){
    be_size_t No4   = ((be_size_t) 0x1)<<(n-2);
    double * x_real_1 = x_real;
    double * x_imag_1 = x_imag;
    double * x_real_2 = x_real + No4;
    double * x_imag_2 = x_imag + No4;
    double * x_real_3 = x_real + 2*No4;
    double * x_imag_3 = x_imag + 2*No4;
    double * x_real_4 = x_real + 3*No4;
    double * x_imag_4 = x_imag + 3*No4;
    double * y_real_1 = y_real;
    double * y_imag_1 = y_imag;
//     double * y_real_2 = y_real + No4;
//     double * y_imag_2 = y_imag + No4;
//     double * y_real_3 = y_real + 2*No4;
//     double * y_imag_3 = y_imag + 2*No4;
//     double * y_real_4 = y_real + 3*No4;
//     double * y_imag_4 = y_imag + 3*No4;
    double * y_real_2 = (double *) befft_malloc (No4*sizeof(double));
    double * y_imag_2 = (double *) befft_malloc (No4*sizeof(double));
    double * y_real_3 = (double *) befft_malloc (No4*sizeof(double));
    double * y_imag_3 = (double *) befft_malloc (No4*sizeof(double));
    double * y_real_4 = (double *) befft_malloc (No4*sizeof(double));
    double * y_imag_4 = (double *) befft_malloc (No4*sizeof(double));
    for (be_size_t j = 0; j<No4; j++) {
        y_real_1[j] = x_real_1[j] + x_real_2[j];
        y_imag_1[j] = x_imag_1[j] + x_imag_2[j];
        
        y_real_2[j] = x_real_1[j] - x_real_2[j];
        y_imag_2[j] = x_imag_1[j] - x_imag_2[j];
        
        y_real_3[j] = x_real_3[j] + x_imag_4[j];
        y_imag_3[j] = x_imag_3[j] - x_real_4[j];
        
        y_real_4[j] = x_real_3[j] - x_imag_4[j];
        y_imag_4[j] = x_imag_3[j] + x_real_4[j];
    }
    memcpy(y_real +   No4, y_real_2, No4*sizeof(double));
    memcpy(y_imag +   No4, y_imag_2, No4*sizeof(double));
    memcpy(y_real + 2*No4, y_real_3, No4*sizeof(double));
    memcpy(y_imag + 2*No4, y_imag_3, No4*sizeof(double));
    memcpy(y_real + 3*No4, y_real_4, No4*sizeof(double));
    memcpy(y_imag + 3*No4, y_imag_4, No4*sizeof(double));
    befft_free(y_real_2);
    befft_free(y_imag_2);
    befft_free(y_real_3);
    befft_free(y_imag_3);
    befft_free(y_real_4);
    befft_free(y_imag_4);
    
    double u   = ldexp(1, -BEFFT_PREC); /* is exact */
    return u;
}

double befft_OneStep_static_vect( double * restrict y_real, double * restrict y_imag,
                                  double * restrict x_real, double * restrict x_imag,
                                  befft_fft_rad2_t rad2, 
                                  uint k){
    
    uint n    = befft_fft_rad2_log2sizeref(rad2);
    be_size_t Ko2 = ((be_size_t) 0x1)<<(k-1);
    be_size_t K   = ((be_size_t) 0x1)<<k;     /* the size of each independent fft */
    be_size_t Nb  = ((be_size_t) 0x1)<<(n-k); /* = N/K := number of independent order K fft */
    double * y_real_2 = (double *) befft_malloc (Ko2*sizeof(double));
    double * y_imag_2 = (double *) befft_malloc (Ko2*sizeof(double));
    double mul_re, mul_im;
    double * omega_re = be_vec_realref( befft_fft_rad2_omegassref(rad2) + (k-1));
    double * omega_im = be_vec_imagref( befft_fft_rad2_omegassref(rad2) + (k-1));
    /* loop on independent fft */
    for (be_size_t nb = 0; nb < Nb; nb++){
        be_size_t nbK = nb*K; /* index of begin of nb-th independent fft */
        /* split x and y in 2 tables of same size */
        double * x_real_1 = x_real + nbK;
        double * x_imag_1 = x_imag + nbK;
        double * x_real_2 = x_real + nbK + Ko2;
        double * x_imag_2 = x_imag + nbK + Ko2;
        double * y_real_1 = y_real + nbK;
        double * y_imag_1 = y_imag + nbK;
//         double * y_real_2 = y_real + nbK + Ko2;
//         double * y_imag_2 = y_imag + nbK + Ko2;
        /* computes the products and sums and subs */
        for (be_size_t j = 0; j < Ko2; j++){
            mul_re = omega_re[j]*x_real_2[j] - omega_im[j]*x_imag_2[j];
            mul_im = omega_re[j]*x_imag_2[j] + omega_im[j]*x_real_2[j];
            y_real_1[j] = x_real_1[j] + mul_re;
            y_imag_1[j] = x_imag_1[j] + mul_im;
            y_real_2[j] = x_real_1[j] - mul_re;
            y_imag_2[j] = x_imag_1[j] - mul_im;
        }
        memcpy(y_real + nbK + Ko2, y_real_2, Ko2*sizeof(double));
        memcpy(y_imag + nbK + Ko2, y_imag_2, Ko2*sizeof(double));
    }
    befft_free(y_real_2);
    befft_free(y_imag_2);
    
    double error;
    double u   = ldexp(1, -BEFFT_PREC); /* is exact */
    if (k<=2)
        error = u;
    else {
        double Delta = be_vec_aberref(befft_fft_rad2_omegassref(rad2) + (k-1));     /*the max of abs errors on root of units */
        double rho   = BEFFT_SQRTFIVE*u ; /* rho <= (1+u)exact(rho)*/ /*the relative error of each complex mul */
        double g     = 1 + Delta ;        /*   g <= (1+u)exact(g)  */
               g     = rho*g ;            /*   g <= (1+u)exact(rho*g)<=(1+u)^3exact(rho)*exact(g)  */
               g     = Delta + g ;        /*   g <= (1+u)exact(Delta + g) <= (1+u)exact(Delta) + (1+u)exact(g) */
                                          /*   g <= (1+u)^4(exact(Delta)+exact(g)) */
           error     = 1 + u ;            /* error <= (1+u)exact(error) */
           error     = g*error ;          /* error <= (1+u)exact(g*error) <= (1+u)*g*error <= (1+u)^6*exact(g)*exact(error) */
           error     = u + error ;        /* error <= (1+u)exact(u+error) <= (1+u)^7*exact(error) */
           error     = error/(1-8*u);
    }
    
    return error;
}

int befft_fft_rad2_exact_input_vect_precomp ( double * restrict y_real, double * restrict y_imag, double * y_ub_abs_error,
                                              double * restrict x_real, double * restrict x_imag, 
                                              befft_fft_rad2_t rad2 ){
    uint  n = befft_fft_rad2_log2sizeref(rad2);
    be_size_t * fft_tree = befft_fft_rad2_fft_treeref(rad2);
    
    be_size_t N = ((be_size_t) 0x1) << n;
    
    double * z_real = (double *) befft_malloc (N*sizeof(double));
    double * z_imag = (double *) befft_malloc (N*sizeof(double));
    
    double y_ub_rel_error = 1;
    double relertemp;
    int underflow = 0;
    feclearexcept (FE_ALL_EXCEPT);
    
    if (n>=1) {
        relertemp = befft_FirstStep_static_vect(y_real, y_imag, x_real, x_imag, rad2);
        relertemp = BEFFT_NEXT( 1 + relertemp );
        y_ub_rel_error = BEFFT_NEXT( y_ub_rel_error*relertemp );
//         relertemp = 1 + relertemp ;
//         y_ub_rel_error = y_ub_rel_error*relertemp;
    }
    
    if (n>=2) {
        relertemp = befft_SecondStep_static_vect(z_real, z_imag, y_real, y_imag, n);
        relertemp = BEFFT_NEXT( 1 + relertemp );
        y_ub_rel_error = BEFFT_NEXT( y_ub_rel_error*relertemp );
//         relertemp = 1 + relertemp ;
//         y_ub_rel_error = y_ub_rel_error*relertemp;
    }
    
    for (ulong j=0; j<N; j++) {
        y_real[j] = z_real[fft_tree[j]];
        y_imag[j] = z_imag[fft_tree[j]];
    }
    
    for (uint k=3; k<=n; k++){
        if (k%2)
            relertemp = befft_OneStep_static_vect(z_real, z_imag, y_real, y_imag, rad2, k);
        else
            relertemp = befft_OneStep_static_vect(y_real, y_imag, z_real, z_imag, rad2, k);
        relertemp = BEFFT_NEXT( 1 + relertemp );
        y_ub_rel_error = BEFFT_NEXT( y_ub_rel_error*relertemp );
//         relertemp = 1 + relertemp ;
//         y_ub_rel_error = y_ub_rel_error*relertemp;
    }
    if (n%2) { 
        memcpy(y_real, z_real, N*sizeof(double));
        memcpy(y_imag, z_imag, N*sizeof(double));
    }
    
    y_ub_rel_error = BEFFT_NEXT( y_ub_rel_error - 1 );
    /* relative error ||Y-exact(Y)||_2 <= y_ub_rel_error * ||exact(Y)||_2                         */
    /* => Y        = exact(Y) + delta with ||delta||_2/||exact(Y)||_2 <= y_ub_rel_error           */
    /* => Y        = exact(Y)       *  (1 + delta/exact(Y))                                       */
    /* => ||Y||_2  = ||exact(Y)||_2 * || 1 + delta/exact(Y) ||_2                                  */
    /* => ||Y||_2 >= ||exact(Y)||_2 * | 1 - ||delta/exact(Y)||_2 |                                */
    /* => ||Y||_2 >= ||exact(Y)||_2 * | 1 -   y_ub_rel_error |                                    */
    /* => ||exact(Y)||_2 <= ||Y||_2 / | 1 -   y_ub_rel_error |                                    */
    /* => relative error ||Y-exact(Y)||_2 <= (y_ub_rel_error/|1-y_ub_rel_error|) * ||exact(Y)||_2 */
    double den     = BEFFT_PREV( BEFFT_ABS(1 - (*y_ub_abs_error)) );
    y_ub_rel_error = BEFFT_NEXT( y_ub_rel_error / den );
    double twonorm = _be_vec_twonorm_ub( y_real, y_imag, N );
    *y_ub_abs_error= BEFFT_NEXT( twonorm * y_ub_rel_error );
    
//     printf("relative error: %.5e, absolute error: %.5e, twonorm: %.5e\n", y_ub_rel_error, *y_ub_abs_error, twonorm );
    
    underflow = !!fetestexcept(FE_UNDERFLOW);
    
    befft_free(z_real);
    befft_free(z_imag);
    
    return underflow;
}

int befft_fft_rad2_ub_abs_error_vect_precomp ( double y_real[], double y_imag[], double * y_max_abs_error,
                                               double x_real[], double x_imag[], double x_max_abs_error, befft_fft_rad2_t rad2 ){
    
    int underflow = befft_fft_rad2_exact_input_vect_precomp ( y_real, y_imag, y_max_abs_error, x_real, x_imag, rad2 );
    
    /* take into account the max of absolute error x_max_abs_error on x_real + Ix_imag: */
    /* let A be the fft matrix, with ||A|| = 2^n = N                                    */
    /* || A(x+e) - Ae || = ||Ae|| = ||A||*||e|| <= ||A||*sqrt(N)*||e||_\infty           */
    /*                                          <= sqrt(N)*N*||e||_\infty               */
    if (x_max_abs_error > 0) {
        uint  n = befft_fft_rad2_log2sizeref(rad2);
        /* be_size_t N = ((be_size_t) 0x1) << n; */
		double N = ldexp(0x1, n);
        double errinput = BEFFT_NEXT(sqrt(N));
               errinput = BEFFT_NEXT(N*errinput);
               errinput = BEFFT_NEXT(x_max_abs_error*errinput);
       *y_max_abs_error = BEFFT_NEXT((*y_max_abs_error) + errinput);
    } else if (x_max_abs_error <0) {
        *y_max_abs_error = -1.;
    }
    
    underflow = !!fetestexcept(FE_UNDERFLOW); /* exceptions flags have been cleared in befft_fft_rad2_exact_input */
    return underflow;
    
}

int befft_fft_rad2_vec_abs_error_vect_precomp ( double y_real[], double y_imag[], double y_aber[],
                                                double x_real[], double x_imag[], double x_aber[], befft_fft_rad2_t rad2){
   
    uint  n = befft_fft_rad2_log2sizeref(rad2);
    be_size_t N = ((be_size_t) 0x1) << n;
    
    /* compute the fft on exact input: y = Ax */
    double abserrorexact;
    int underflow = befft_fft_rad2_exact_input_vect_precomp ( y_real, y_imag, &abserrorexact, x_real, x_imag, rad2 );
    
    /* compute the fft on vector of errors: e' = Ae */
    double * zeroin = (double *) befft_calloc (N, sizeof(double));
    double * errout = (double *) befft_malloc (N*sizeof(double));
//     for (ulong j=0; j<N; j++)
//         zeroin[j]=0.;
    double abserrorerror;
    int underflow2 = befft_fft_rad2_exact_input_vect_precomp ( y_aber, errout, &abserrorerror, x_aber, zeroin, rad2 );
    
    /* compute the error on each component */
    double u   = ldexp(1, -BEFFT_PREC); /* is exact */
    for (be_size_t j=0; j<N; j++) {
        y_aber[j] = BEFFT_MAX( BEFFT_ABS(y_aber[j]), BEFFT_ABS(errout[j]) );
        y_aber[j] = BEFFT_SQRTTWO*y_aber[j];   /* y_aber[j] <= (1+u)exact(y_aber[j]) */
        y_aber[j] = y_aber[j] + abserrorerror; /* y_aber[j] <= (1+u)^2exact(y_aber[j]) + (1+u)abserrorerror */
        y_aber[j] = y_aber[j] + abserrorexact; /* y_aber[j] <= (1+u)^3exact(y_aber[j]) + (1+u)^2abserrorerror + (1+u)abserrorexact*/
        y_aber[j] = y_aber[j]/(1-4*u);
    }
    underflow2 = !!fetestexcept(FE_UNDERFLOW); /* exceptions flags have been cleared in befft_fft_rad2_exact_input */
    
    befft_free(zeroin);
    befft_free(errout);
    
    return (underflow || underflow2);
    
}

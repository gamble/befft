/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

/* use formula: |RN(ab)-(ab)| <= (sqrt(5)u)*|RN(ab)| + |b|epsa + |a|epsb + epsa*epsb */
void _be_mul_aber( double * cr, double * ci, double * ce, 
                   double   ar, double   ai, double   ae,
                   double   br, double   bi, double   be){

    double normb   = _be_twonorm_ub_fast( br, bi );        /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast      */
                                                           /* ie normb <= (1+u)exact(normb)                     */
           normb   = normb + be;                           /*    normb <= (1+u)^2exact(normb)                   */
    double normbae = normb*ae;                             /* normbae  <= (1+u)^3exact(exact(normb)*ae)         */
    double norma   = _be_twonorm_ub_fast( ar, ai );        /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast */
                                                           /* ie norma <= (1+u)exact(norma)                */
           norma   = norma + ae;                           /*    norma <= (1+u)^2exact(norma)              */
    double normabe = norma*be;                             /* normabe  <= (1+u)^3exact(exact(norma)*be)             */
    double aebe = ae*be;                                   /* aebe  <= (1+u)exact(ae*be) */
    double right   = normbae + normabe;                    /* right <=  (1+u)^4 exact(right)              */
           right   = right + aebe;                         /*       <=  (1+u)^5 exact(right)              */
    _be_mul( cr, ci,    // let d = (ar+Iai)*(br+Ibi)
             ar, ai,    // then c = (1+f)d with f\in\C and ||f||<=sqrt(5)*u
             br, bi );
    double u = BEFFT_U;                                    /* is exact */
    double twonorm_ub = _be_twonorm_ub_fast( *cr, *ci );   /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast      */
                                                           /* ie twonorm_ub <= (1+u)exact(twonorm_ub)           */
    *ce = BEFFT_RELER_be_mul*u;                            /* mult by u is exact, thus ce = exact(ce)           */
    *ce = (*ce)*twonorm_ub;                                /* ce = (1+u)*exact( ce*twonorm_ub )                 */
    /* ce <= (1+u)*exact( ce*twonorm_ub ) <= (1+u)^2*exact(ce)*exact(twonorm_ub) */
    
    *ce = (*ce) + right;                 /* ce <= (1+u)^6 exact(ce) */
    *ce = (*ce)/(1-7*u); /* use Property 16 of Muller book to correct error: (1+u)^{k}|ce|<=RN(|ce|/(1-(k+1)*u))*/
}

void _be_mul_double_aber( double * cr, double * ci, double * ce, 
                          double   ar, double   ai, double   ae,
                          double   br                           ) {
    double normbae = BEFFT_ABS(br)*ae;                     /* normbae  <= (1+u)exact(exact(normb)*ae)         */
    _be_mul_be_real( cr, ci,    // let d = (ar+Iai)*(br)
                     ar, ai,    // then c = (1+f)d with f\in\C and ||f||<=u
                     br     );
    double u = BEFFT_U;                                    /* is exact */
    double twonorm_ub = _be_twonorm_ub_fast( *cr, *ci );   /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast      */
                                                           /* ie twonorm_ub <= (1+u)exact(twonorm_ub)           */
    *ce = BEFFT_RELER_be_mul_be_real*u;                    /* mult by u is exact, thus ce = exact(ce)           */
    *ce = (*ce)*twonorm_ub;                                /* ce = (1+u)*exact( ce*twonorm_ub )                 */
    /* ce <= (1+u)*exact( ce*twonorm_ub ) <= (1+u)^2*exact(ce)*exact(twonorm_ub) */
    
    *ce = (*ce) + normbae;                 /* ce <= (1+u)^3 exact(ce) */
    *ce = (*ce)/(1-4*u); /* use Property 16 of Muller book to correct error: (1+u)^{k}|ce|<=RN(|ce|/(1-(k+1)*u))*/
}

void _be_mul_be_real_aber( double * cr, double * ci, double * ce, 
                           double   ar, double   ai, double   ae,
                           double   br,              double   be) {
    
    if (be==0) {
        _be_mul_double_aber(cr, ci, ce, ar, ai, ae, br);
        return;
    }
    
    double normb   = BEFFT_ABS(br);                        /* error is 0                     */
                                                           /* ie normb = exact(normb)        */
           normb   = normb + be;                           /*    normb <= (1+u)exact(normb)  */
    double normbae = normb*ae;                             /* normbae  <= (1+u)^2exact(exact(normb)*ae)         */
    double norma   = _be_twonorm_ub_fast( ar, ai );        /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast */
                                                           /* ie norma <= (1+u)exact(norma)                */
           norma   = norma + ae;                           /*    norma <= (1+u)^2exact(norma)              */
    double normabe = norma*be;                             /* normabe  <= (1+u)^3exact(exact(norma)*be)             */
    double aebe = ae*be;                                   /* aebe  <= (1+u)exact(ae*be) */
    double right   = normbae + normabe;                    /* right <=  (1+u)^4 exact(right)              */
           right   = right + aebe;                         /*       <=  (1+u)^5 exact(right)              */
    _be_mul_be_real( cr, ci,    // let d = (ar+Iai)*(br)
                     ar, ai,    // then c = (1+f)d with f\in\C and ||f||<=u
                     br     );
    double u = BEFFT_U;                                    /* is exact */
    double twonorm_ub = _be_twonorm_ub_fast( *cr, *ci );   /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast      */
                                                           /* ie twonorm_ub <= (1+u)exact(twonorm_ub)           */
    *ce = BEFFT_RELER_be_mul_be_real*u;                    /* mult by u is exact, thus ce = exact(ce)           */
    *ce = (*ce)*twonorm_ub;                                /* ce = (1+u)*exact( ce*twonorm_ub )                 */
    /* ce <= (1+u)*exact( ce*twonorm_ub ) <= (1+u)^2*exact(ce)*exact(twonorm_ub) */
    
    *ce = (*ce) + right;                 /* ce <= (1+u)^6 exact(ce) */
    *ce = (*ce)/(1-7*u); /* use Property 16 of Muller book to correct error: (1+u)^{k}|ce|<=RN(|ce|/(1-(k+1)*u))*/
}

void _be_mul_si_aber( double * dest_re, double * dest_im, double * dest_aber, 
                      double   srca_re, double   srca_im, double   srca_aber,
                      slong    srcb                                           ) {
    double t, t_aber;
    _be_real_set_si_aber( &t, &t_aber, srcb );
    _be_mul_be_real_aber( dest_re, dest_im, dest_aber, 
                          srca_re, srca_im, srca_aber,
                          t,                t_aber);
}

/* use formula: |RN(ab)-(ab)| <= (u)*|RN(ab)| + |b|epsa + |a|epsb + epsa*epsb */
void _be_real_mul_aber( double * c, double *ce,
                        double   a, double  ae,
                        double   b, double  be) {     
    
    double normb   = BEFFT_ABS(b);        /* no error */
           normb   = normb + be;          /*    normb <= (1+u)exact(normb) */
    double normbae = normb*ae;            /*  normbae <= (1+u)^2exact(exact(normb)*ae) */
    double norma   = BEFFT_ABS(a);        /* no error */
           norma   = norma + ae;          /*    norma <= (1+u)exact(norma) */
    double normabe = norma*be;            /*  normabe <= (1+u)^2exact(exact(norma)*be) */
    double aebe = ae*be;                  /*     aebe <= (1+u)exact(ae*be) */
    double right   = normbae + normabe;   /*    right <= (1+u)^3 exact(right) */
           right   = right + aebe;        /*          <= (1+u)^4 exact(right) */
    _be_real_mul( c, a, b );                   /* c = (1+f)ab with f\in\C and ||f||<=u */
    double u = BEFFT_U;                   /* no error */
    double normc   = BEFFT_ABS(*c);       /* no error */
    *ce = BEFFT_RELER_be_real_mul*u;      /* no error */
    *ce = (*ce)*normc;                    /* ce = (1+u)*exact( ce*normc ) */
    *ce = (*ce) + right;                  /* ce <= (1+u)^5 exact(ce) */
    *ce = (*ce)/(1-6*u); /* use Property 16 of Muller book to correct error: (1+u)^{k}|ce|<=RN(|ce|/(1-(k+1)*u))*/
}

void _be_real_mul_si_aber( double * dest_re, double * dest_aber, 
                           double   srca_re, double   srca_aber,
                           slong    srcb                        ){
    double t, t_aber;
    _be_real_set_si_aber( &t, &t_aber, srcb );
    _be_real_mul_aber( dest_re, dest_aber, 
                       srca_re, srca_aber,
                          t,    t_aber);
}

// void _be_real_mul_aber( double * dest, double *dest_aber,
//                         double   srca, double  srca_aber,
//                         double   srcb, double  srcb_aber) {     
//     double t;
//     _be_mul_aber( dest, &t, dest_aber,
//                   srca, .0, srca_aber,
//                   srcb, .0, srcb_aber);
// }

/* not used but kept for record */
/* use formula: |RN(ab)-(ab)| <= (sqrt(5)u)*|RN(ab)| + |b|epsa + |a|epsb + epsa*epsb */
// void _be_mul_aber( double * cr, double * ci, double * ce, 
//                    double   ar, double   ai, double   ae,
//                    double   br, double   bi, double   be){
//     _be_mul( cr, ci,    // let d = (ar+Iai)*(br+Ibi)
//              ar, ai,    // then c = (1+f)d with f\in\C and ||f||<=sqrt(5)*u
//              br, bi );
//     
//     if ( (ae < 0) || (be < 0) ) { /* error is unknown */
//         *ce = -1;
//     } else {
//         double u = BEFFT_U;                                                 /* is exact */
//         int    k = 0;
//         
//         if ( ( ( ar==0 )&&( ai==0 ) ) || (( br==0 )&&( bi==0 )) /* mul by zero is exact */
//            ||( ( ar==1 )&&( ai==0 ) ) || (( br==1 )&&( bi==0 )) /* mul by 1 is exact */
//            ||( ( ar==0 )&&( ai==1 ) ) || (( br==0 )&&( bi==1 )) /* mul by I is exact */
//            ) {
//             *ce=0;
//         } else {
//             double twonorm_ub = _be_twonorm_ub_fast( *cr, *ci );   /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast         */
//                                                                    /* ie twonorm_ub = (1+e3)exact(twonorm_ub) with |e3|<=u */
//             *ce = BEFFT_RELER_be_mul*u;                            /* mult by u is exact, thus ce = exact(ce)              */
//             *ce = (*ce)*twonorm_ub;                                /* ce = (1+e4)*exact( ce*twonorm_ub ) with |e4|<=u      */
//             /* ce = (1+e4)*exact( ce*twonorm_ub ) = (1+e4)*(1+e3)*exact(ce)*exact(twonorm_ub) */
//             k=k+2;
//             
//             /* old version */
// //             *ce=_be_twonorm_ub_fast( *cr, *ci );
// //             *ce=BEFFT_NEXT( ldexp(1,BEFFT_LOG2U)*(*ce) );
// //             *ce=BEFFT_NEXT( BEFFT_SQRTFIVE*(*ce) );
//         }
//         if (ae > 0) {
//             double normb   = _be_twonorm_ub_fast( br, bi );      /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast */
//                                                                  /* ie normb = (1+e5)exact(normb) with |e5|<=u */
//             double normbae = normb*ae;                           /* normbae  = (1+e6)exact(normb*ae) with |e6|<=u */
//             *ce = (*ce) + normbae;                               /* ce       = (a+e7)exact(ce + normbae) */
//             /* ce = (1+e7)exact(ce + normbae) = (1+e7)ce + (1+e7)*(1+e6)exact(normb*ae) */
//             /*    = (1+e7)ce + (1+e7)*(1+e6)*(1+e5)exact(normb)*ae */
//             /*    = (1+e7)*(1+e4)*(1+e3)*exact(ce)*exact(twonorm_ub) + (1+e7)*(1+e6)*(1+e5)*exact(normb)*ae */
//             k=BEFFT_MAX( 3, k+1 );
// //             k=k+3;
//             /* old version */
// //             double normb = _be_twonorm_ub_fast( br, bi );
// //                    normb = BEFFT_NEXT( normb*ae );
// //                    *ce = BEFFT_NEXT( (*ce) + normb );
//         }
//         
//         if (be > 0) {
//             double norma   = _be_twonorm_ub_fast( ar, ai );        /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast */
//                                                                    /* ie norma = (1+e8)exact(norma) with |e8|<=u */
//             double normabe = norma*be;                             /* normabe  = (1+e9)exact(norma*be) with |e9|<=u */
//             *ce = (*ce) + normabe;                                 /* ce       = (1+e10)exact(ce + normabe) */
//             /* ce = (1+e10)exact(ce + normabe) = (1+e10)ce + (1+e10)*(1+e9)exact(norma*be) */
//             /*    = (1+e10)ce + (1+e10)*(1+e9)*(1+e8)exact(norma)*be */
//             /*    = (1+e10)*(1+e7)*(1+e4)*(1+e3)*exact(ce)*exact(twonorm_ub) */
//             /*    + (1+e10)*(1+e7)*(1+e6)*(1+e5)*exact(normb)*ae             */
//             /*    + (1+e10)*(1+e9)*(1+e8)exact(norma)*be */
//             k=BEFFT_MAX( 3, k+1 );
// //             k = k+3;
//             /* old version */
// //             double norma = _be_twonorm_ub_fast( ar, ai );
// //                    norma = BEFFT_NEXT( norma*be );
// //                    *ce = BEFFT_NEXT( (*ce) + norma );
//         }
//         
//         if ( (ae > 0) && (be > 0) ) {
//             double aebe = ae*be;                                   /* aebe  = (1+e11)exact(ae*be) with |e11|<=u */
//             *ce = (*ce) + aebe;                                    /* ce    = (1+e12)exact(ce + aebe) */
//             /* ce    = (1+e12)exact(ce + aebe) =  (1+e12)ce + (1+e12)*(1+e11)exact(ae*be) */
//             /* ce    = (1+e12)*(1+e10)*(1+e7)*(1+e4)*(1+e3)*exact(ce)*exact(twonorm_ub) */
//             /*       + (1+e12)*(1+e10)*(1+e7)*(1+e6)*(1+e5)*exact(normb)*ae             */
//             /*       + (1+e12)*(1+e10)*(1+e9)*(1+e8)exact(norma)*be                     */
//             /*       + (1+e12)*(1+e11)exact(ae*be) */
//             k=BEFFT_MAX( 2, k+1 );
// //             k = k+2;
//             /* old version */
// //             double temp = BEFFT_NEXT( ae*be );
// //             *ce = BEFFT_NEXT( (*ce) + temp );
//         }
//         *ce = (*ce)/(1-(k+1)*u); /* use Property 16 of Muller book to correct error: (1+u)^{k}|ce|<=RN(|ce|/(1-(k+1)*u))*/
//         
//     }
//     
// }

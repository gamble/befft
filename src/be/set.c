/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

/* sets real and imag parts with unknown error */
void be_set_double( be_t dest, double real, double imag ){
    be_realref(dest) = real;
    be_imagref(dest) = imag;
    be_aberref(dest) = -1.;
}
/* sets real and imag parts from exact numbers */
void be_set_double_exact( be_t dest, double real, double imag){
    be_realref(dest) = real;
    be_imagref(dest) = imag;
    be_aberref(dest) = 0.;
}
/* sets real and imag parts from unexact unmbers with known errors */
void be_set_double_error( be_t dest, double real, double imag, double aber){
    be_realref(dest) = real;
    be_imagref(dest) = imag;
    be_aberref(dest) = aber;
}

void _be_real_set_si_aber( double * dest, double * dest_aber, slong src ){
    _be_real_set_si( dest, src );
    *dest_aber = 0.;
    uint64_t abssrc = (uint64_t)BEFFT_ABS(src);
    uint64_t shiftedabssrc = abssrc >> BEFFT_PREC;
    if (shiftedabssrc > 0) {
        *dest_aber = floor(ldexp( *dest, BEFFT_LOG2U ));
        int diflog2=0;
        while (shiftedabssrc > 0) {
            shiftedabssrc = shiftedabssrc >> 1;
            diflog2++;
        }
        slong p2diflog2= (slong)0x1 << diflog2;
        if ( abssrc%p2diflog2 == 0 )
            *dest_aber = 0.;
    }
}

#ifdef BEFFT_HAS_ARB
int _be_real_set_arb( double * dest, double * dest_aber, const arb_t ball ){
    *dest_aber = 0.;
    if (arb_is_zero(ball)){
        *dest = 0.;
    } else if (arb_is_one(ball)) {
        *dest = 1.;
    } else if (arb_equal_si(ball, -1)) {
        *dest = -1.;
    } else if (arb_is_exact(ball)) {
        *dest = arf_get_d( arb_midref( ball ), ARF_RND_NEAR );
        if (arb_can_round_arf( ball, BEFFT_PREC, ARF_RND_NEAR ) == 0 ) {
            *dest_aber = ldexp( BEFFT_ABS( *dest ), BEFFT_LOG2U );
        }
    }
      else {
        *dest = arf_get_d( arb_midref( ball ), ARF_RND_NEAR );
        if (arb_can_round_arf( ball, BEFFT_PREC, ARF_RND_NEAR ) == 0) {
            *dest_aber = ldexp( BEFFT_ABS( *dest ), BEFFT_LOG2U );
            *dest_aber = (*dest_aber) + mag_get_d( arb_radref( ball ) );
            double u = BEFFT_U;
            *dest_aber = (*dest_aber)/(1-2*u);
        } else {
            *dest_aber = mag_get_d( arb_radref( ball ) );
        }
    }
    return (isfinite(*dest)&&isfinite(*dest_aber));
}

int _be_set_acb( double * dest_real, double * dest_imag, double * dest_aber, const acb_t ball ){
    double aber_real, aber_imag;
    int resr = _be_real_set_arb( dest_real, &aber_real, acb_realref(ball) );
    int resi = _be_real_set_arb( dest_imag, &aber_imag, acb_imagref(ball) );
    if (aber_imag==0.)
        *dest_aber = aber_real;
    else if (aber_real==0.)
        *dest_aber = aber_imag;
    else {
        *dest_aber = BEFFT_SQRTTWO*BEFFT_MAX( aber_real, aber_imag );
        double u = BEFFT_U;
        *dest_aber = (*dest_aber)/(1-2*u);
    }
    return (resr&&resi);
}

int be_set_acb( be_t dest, const acb_t ball ){
    return _be_set_acb( &be_realref(dest), &be_imagref(dest), &be_aberref(dest), ball );
}

#endif

/* OLD */
// void _be_real_set_arb( double * dest, double * dest_aber, const arb_t ball ){
//     arb_t mball;
//     arb_init(mball);
//     arb_neg(mball, ball);
//     *dest_aber = 0.;
//     if (arb_is_zero(ball)){
//         *dest = 0.;
//     } else if (arb_is_one(ball)) {
//         *dest = 1.;
//     } else if (arb_is_one(mball)) {
//         *dest = -1.;
//     } else if (arb_is_exact(ball)) {
//         *dest = arf_get_d( arb_midref( ball ), ARF_RND_NEAR );
//         if (arb_can_round_arf( ball, BEFFT_PREC, ARF_RND_NEAR ) == 0 ) {
//             *dest_aber = ldexp( BEFFT_ABS( *dest ), BEFFT_LOG2U );
//         }
//     }
//       else {
//         *dest = arf_get_d( arb_midref( ball ), ARF_RND_NEAR );
//         *dest_aber = ldexp( BEFFT_ABS( *dest ), BEFFT_LOG2U );
//         if (arb_can_round_arf( ball, BEFFT_PREC, ARF_RND_NEAR ) == 0) {
//             double u = BEFFT_U;
//             *dest_aber = (*dest_aber) + mag_get_d( arb_radref( ball ) );
//             *dest_aber = (*dest_aber)/(1-2*u);
//         }
//     }
//     arb_clear(mball);
// }
// 
// void _be_set_acb( double * dest_real, double * dest_imag, double * dest_aber, const acb_t ball ){
//     double aber_real, aber_imag;
//     _be_set_arb( dest_real, &aber_real, acb_realref(ball) );
//     _be_set_arb( dest_imag, &aber_imag, acb_imagref(ball) );
//     if (aber_imag==0.)
//         *dest_aber = aber_real;
//     else if (aber_real==0.)
//         *dest_aber = aber_imag;
//     else {
// //         *dest_aber = BEFFT_NEXT(BEFFT_SQRTTWO*BEFFT_MAX( aber_real, aber_imag ));
//         *dest_aber = BEFFT_SQRTTWO*BEFFT_MAX( aber_real, aber_imag );
//         double u = BEFFT_U;
//         *dest_aber = (*dest_aber)/(1-2*u);
//     }
// }
// 
// void be_set_acb( be_t dest, const acb_t ball ){
//     _be_set_acb( &be_realref(dest), &be_imagref(dest), &be_aberref(dest), ball );
// }

/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"
/* if no underflow happens */
/* 1  => surely contains zero       */
/* 0  => surely separated from zero */
/* -1 => can not decide             */
int _be_contains_zero( const double real, const double imag, const double aber ){
    
    double ra = BEFFT_ABS(real);
    double ia = BEFFT_ABS(imag);
    if (imag==0) 
        return (ra<=aber);
    
    if (real==0) 
        return (ia<=aber);
    
    /* already done above */
//     if ( (real==0)&&(imag==0) )
//         return 1;
    
    if (aber==0) 
        return (real==0)&&(imag==0);
    
    /* here real, imag, aber are non zero */
    
    /* the disk D:=D(real+Iimag, aber) is in                                 */
    /* the box  B1:=[real - aber, real + aber]\times[imag - aber, imag + aber] */
    /* thus 0\notin B1 => 0\notin D */
    if ( (ra>aber) || (ia>aber) )
        return 0;
    
    /* the box B2:=[real - aber/2, real + aber/2]\times[imag - aber/2, imag + aber/2] */
    /* is strictly in D */
    /* thus 0 \in B2 => 0\in D */
    /* dividing by 2 is always exact, except when underflow happens */
    double ao2 = aber/2;
    if ( (ra<=ao2) && (ia<=ao2) ) {
        return 1;
    }
    
    /* compute |real + Iimag| */
    double mod, moderr;
    _be_abs_aber( &mod, &moderr, real, imag, 0. );
    if ( _be_real_pos_gt( mod, moderr, aber, 0. ) )
        return 0;
    if ( _be_real_pos_gt( aber, 0., mod, moderr ) )
        return 1;
    
    return -1;
    
}

/* if no underflow happens */
/* 1  => surely contains zero       */
/* 0  => surely separated from zero */
/* -1 => can not decide             */
int _be_is_non_zero( const double real, const double imag, const double aber ){
    int res = _be_contains_zero( real, imag, aber );
    if (res==1)
        return 0;
    if (res==0)
        return 1;
    return -1;
}

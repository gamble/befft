/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

/* not used but kept for record */
/* see Handbook of floating-point arithmetic, formula (6.1.b) */
void _be_add_aber( double * cr, double * ci, double * ce, 
                   double   ar, double   ai, double   ae,
                   double   br, double   bi, double   be){
    
    double aepbe = ae+be; /* aepbe <= (1+u) exact(ae+be) */
    
    _be_add( cr, ci,      
             ar, ai,      
             br, bi );  // relative error is u*BEFFT_RELER_be_add i.e. u
    
    double u = BEFFT_U;                                  /* is exact */
    double twonorm_ub = _be_twonorm_ub_fast( *cr, *ci ); /* twonorm_ub <= (1+u)*exact(twonorm_ub) */
                                                         
    *ce = twonorm_ub*BEFFT_RELER_be_add*u;               /* ce <= (1+u)*exact(ce)                 */ 
    *ce=(*ce) + aepbe;                                   /* ce <= (1+u)*exact( ce + aepbe )            */
                                                         /*    <= (1+u)^2 exact(ce) + (1+u)^2 exact(ae+be) */
    *ce = (*ce)/(1-3*u); /* use Property 16 of Muller book to correct error: (1+u)^{k}|ce|<=RN(|ce|/(1-(k+1)*u))*/
}

void _be_real_add_aber( double *dest, double *dest_aber,
                        double  srca, double  srca_aber,
                        double  srcb, double  srcb_aber) {   
    double t;
    _be_add_aber( dest, &t, dest_aber,
                  srca, .0, srca_aber,
                  srcb, .0, srcb_aber);
}

void _be_real_sub_aber( double *dest, double *dest_aber,
                        double  srca, double  srca_aber,
                        double  srcb, double  srcb_aber) {    
    double t;
    _be_add_aber( dest,  &t, dest_aber,
                  srca, .0, srca_aber,
                  -srcb, .0, srcb_aber);
}

/* not used but kept for record */
/* see Handbook of floating-point arithmetic, formula (6.1.b) */
// void _be_add_aber( double * cr, double * ci, double * ce, 
//                    double   ar, double   ai, double   ae,
//                    double   br, double   bi, double   be){
//     _be_add( cr, ci,      // cr = (1+e1)(ar+br) with |e1|<=u
//              ar, ai,      // ci = (1+e2)(ai+bi) with |e2|<=u
//              br, bi );    // thus letting d = (ar+br)+I(ai+bi), one has
//                           // c  = (1+f)(d) with f\in\C and ||f||<=u 
//                           // relative error is u*BEFFT_RELER_be_add i.e. u
//     if ( (ae < 0) || (be < 0) ) { /* error is unknown */
//         *ce = -1;
//     } else {
//         double u = BEFFT_U;                                                 /* is exact */
//         int    k = 0;
//         if ( ( ( ar==0 )&&( ai==0 ) ) || (( br==0 )&&( bi==0 )) ) {
//             *ce = 0;                                                  /* addition with zero is exact */
//         } else {
//             double twonorm_ub = _be_twonorm_ub_fast( *cr, *ci );   /* error is <= u*BEFFT_RELER_be_twonorm_ub_fast */
//                                                                    /* ie twonorm_ub = (1+e3)exact(twonorm_ub) with |e3|<=u */
//             *ce = twonorm_ub*BEFFT_RELER_be_add*u;                 /* mult by u is exact, thus ce = (1+e3)exact(ce) with |e3|<=u */
//             k++;
//         }
//         
//         if (ae > 0) {
//             *ce=(*ce) + ae ;                                       /* ce = (1+e4)exact(ce + ae) with |e4|<=u */
//             k++;
//         }
//         if (be > 0){
//             *ce=(*ce) + be;                                        /* ce = (1+e5)exact(ce + be) with |e5|<=u */
//             k++;
//         }
//                                  /* ce = (1+e5)exact(ce + be) with |e5|<=u */
//                                  /* ce = (1+e5)((1+e4)exact(ce + ae) + be) with |e4|,|e5|<=u */
//                                  /* ce = (1+e5)((1+e4)((1+e3)exact(ce) + ae) + be) with |e3|,|e4|,|e5|<=u */
//                                  /* ce = (1+e5)*(1+e4)*(1+e3)exact(ce) + (1+e5)*be + (1+e5)*(1+e4)*ae */
//                                  /* ce <= (1+u)^3*(exact(ce) + be + ce ) */
//         *ce = (*ce)/(1-(k+1)*u); /* use Property 16 of Muller book to correct error: (1+u)^{k}|ce|<=RN(|ce|/(1-(k+1)*u))*/
//                                 
//     }
// }

/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

int _be_real_overlaps ( double a, double ae,
                        double b, double be ) {
//     if (a==b)
//         return 1;
//     if (a>b)
//         return _be_real_overlaps ( b, be,
//                                    a, ae );
    
    double dist, dist_aber, rad, rad_aber;
    _be_real_sub_aber( &dist, &dist_aber, a, 0., b, 0. );
    dist = BEFFT_ABS(dist);
    _be_real_add_aber( &rad, &rad_aber, ae, 0., be, 0.);
    
//     printf("dist: %.16f +/- %.16f\n", dist, dist_aber);
//     printf("rad : %.16f +/- %.16f\n", rad , rad_aber);
    
    double u   = BEFFT_U;
    /* there is surely an intersection if dist+dist_aber <= rad-rad_aber */
    /* <=> dist + dist_aber + rad_aber <= rad */
    double left = dist + dist_aber + rad_aber;
           left = left / (1-3*u);
    if (left <= rad)
        return 1;
    /* there is surely no intersection if dist-dist_aber > rad+rad_aber */
    /* <=> dist > rad + rad_aber + dist_aber */
    double right = rad + rad_aber + dist_aber;
           right = right / (1-3*u);
    if (dist > right)
        return 0;
    
    return -1;
}

int _be_overlaps ( double ar, double ai, double ae,
                   double br, double bi, double be ) {
    
//     double dist = sqrt( (ar-br)*(ar-br) + (ai-bi)*(ai-bi) ); /* |ar-br|   <= (1+u)|exact(ar-br)| */
//                                                              /* (ar-br)^2 <= (1+u)^3 exact( (ar-br)^2 ) */  
//                                                              /* (ar-br)^2 + (ai-bi)^2  <= (1+u)^4 exact( (ar-br)^2 + (ai-bi)^2 ) */
//                                                              /* sqrt((ar-br)^2 + (ai-bi)^2)  <= (1+u)^5 sqrt(exact( (ar-br)^2 + (ai-bi)^2 )) */
    double dist, dist_aber, distr, distr_aber, disti, disti_aber, rad, rad_aber;
    _be_real_sub_aber( &distr, &distr_aber, ar, 0., br, 0. );
    _be_real_sub_aber( &disti, &disti_aber, ai, 0., bi, 0. );
    _be_real_mul_aber( &distr, &distr_aber, distr, distr_aber, distr, distr_aber );
    _be_real_mul_aber( &disti, &disti_aber, disti, disti_aber, disti, disti_aber );
    _be_real_add_aber( &dist,  &dist_aber,  distr, distr_aber, disti, disti_aber );
//     printf("dist: %.16f +/- %.16f\n", dist, dist_aber);
    _be_real_sqrt_aber( &dist,  &dist_aber, dist, dist_aber );
    
    _be_real_add_aber(&rad, &rad_aber, ae, 0., be, 0.);
    
//     printf("dist: %.16f +/- %.16f\n", dist, dist_aber);
//     printf("rad : %.16f +/- %.16f\n", rad , rad_aber);
    
    double u   = BEFFT_U;
    /* there is surely an intersection if dist+dist_aber <= rad-rad_aber */
    /* <=> dist + dist_aber + rad_aber <= rad */
    double left = dist + dist_aber + rad_aber;
           left = left / (1-3*u);
    if (left <= rad)
        return 1;
    /* there is surely no intersection if dist-dist_aber > rad+rad_aber */
    /* <=> dist > rad + rad_aber + dist_aber */
    double right = rad + rad_aber + dist_aber;
           right = right / (1-3*u);
    if (dist > right)
        return 0;
    
    return -1;
    
}

/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

void _be_abs_aber( double * dest, double * dest_aber, 
                   double   src_re, double   src_im, double   src_aber ) {
    
    double t1, t2, aber1, aber2;
    _be_real_mul_aber( &t1, &aber1,
                       src_re, src_aber,
                       src_re, src_aber );
    _be_real_mul_aber( &t2, &aber2,
                       src_im, src_aber,
                       src_im, src_aber );
    _be_real_add_aber( dest, dest_aber,
                       t1,   aber1,
                       t2,   aber2);
    _be_real_sqrt_aber( dest, dest_aber,
                        *dest, *dest_aber );
                      
}

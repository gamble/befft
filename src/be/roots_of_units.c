/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

#ifdef BEFFT_HAS_ARB
void _be_omega_slong_ulong( double * dest_real, double * dest_imag, double * dest_aber,
                            slong j, ulong k ){

    slong prec = BEFFT_PREC;
    acb_t ball;
    acb_init(ball);
    acb_set_si(ball, -j);
    acb_div_ui(ball, ball, k, prec);
    acb_exp_pi_i(ball, ball, prec);
    
    _be_set_acb(dest_real, dest_imag, dest_aber, ball);
    
    acb_clear(ball);
   
}

void be_omega_slong_ulong( be_t dest, slong j, ulong k ){
    _be_omega_slong_ulong( &be_realref(dest), &be_imagref(dest), &be_aberref(dest), j, k );   
}

#endif 

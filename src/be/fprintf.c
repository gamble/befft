/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

void _be_fprint(FILE * file, const double real, const double imag, const double aber){
    fprintf(file, "< (%.5e + I%.5e)", real, imag);
    if (aber>=0)
        fprintf(file, " [abs err: (%.5e)] ", aber );
//     if (reer>=0)
//         fprintf(file, " [rel err: (%.5e)] ", reer );
    fprintf(file, ">");
}

void be_fprint(FILE * file, const be_t x){
    _be_fprint(file, be_realref(x), be_imagref(x), be_aberref(x));
}

void _be_fprintd(FILE * file, const double real, const double imag, const double aber, const int d){
    fprintf(file, "< (%.*e + I%.*e)", d, real, d, imag);
//     if (aber>=0)
        fprintf(file, " [abs err: (%.*e)] ", d, aber );
//     if (reer>=0)
//         fprintf(file, " [rel err: (%.5e)] ", reer );
    fprintf(file, ">");
}

void be_fprintd(FILE * file, const be_t x, const int d){
    _be_fprintd(file, be_realref(x), be_imagref(x), be_aberref(x), d);
}

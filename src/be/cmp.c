/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

int _be_real_pos_gt( double x, double x_aber, double y, double y_aber ) {
    
    double left, right;
    double u = BEFFT_U; 
    left = x;
    right  = y_aber+x_aber;
    right  = y+right; // right <= (1+u)^2 exact(y+y_aber+x_aber)
    right  = right/(1-3*u);
    
    return left > right;
}

int _be_real_pos_lt_2exp_si( double x, double x_aber, int e ) {
    
    double left, right;
    double u = BEFFT_U;
    left = x+x_aber;
    left = left/(1-2*u);
    right= ldexp(0x1, e);
    
    return left < right;
    
}

/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

void _be_add_error_be_real_pos( double * dest_re, double * dest_im, double * dest_aber,
                                double   srca_re, double   srca_im, double   srca_aber,
                                double   srcb,    double srcb_aber                     ) {
    *dest_re = srca_re;
    *dest_im = srca_im;
    *dest_aber = srca_aber;
    *dest_aber = *dest_aber + srcb_aber;
    *dest_aber = *dest_aber + srcb;
    double u = BEFFT_U;
    *dest_aber = *dest_aber * (1-3*u);
}

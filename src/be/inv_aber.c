/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

/*     1/(1-u) <= (1+u)^2                   */
/* <=> 1 <= (1+u) - u^2(1+u)                */
/* <=> u >= u^2(1+u)         OK for small u */  

/*     (1/(1-u))^n <= (1+u)^(n+1) for small u and n */

/* | 1/(a + ae) - 1/a | <= | (a - (a+ae))/( a(a+ae) ) | <= | ae / a(a+ae) | */
/*                      <= |ae|/( |a|(|a|-|ae|)                             */
/* | RN(1/a) - 1/a |    <= u*|RN(1/a)| + |ae|/( |a|(|a|-|ae|)               */ 
void _be_real_inv_aber( double * c, double *ce,
                        double   a, double ae  ) {
    
    double norma  = BEFFT_ABS(a);  /* no error */
    double normae = BEFFT_ABS(ae); /* no error */
    if ( normae >= norma ) {
        _be_real_inv( c, a);
        *ce= (double)INFINITY;
        return;
    }
    double right = norma - normae; /*    right >= (1-u)  exact(norma - normae) */
           right = norma*right;    /*    right >= (1-u)  exact(norma*RN(norma - normae)) */
                                   /*          >= (1-u)  exact(norma*(1-u)exact(norma - normae)) */
                                   /*          >= (1-u)^2exact(norma*(norma - normae)) */
                                   /* thus 1/right <= (1/(1-u)^2)*exact(1/(norma*(norma - normae))) */
                                   /* thus 1/right <= (1+u)^3*exact(1/(norma*(norma - normae))) */
           right = normae/right;   /*    right <= (1+u)  exact(normae/RN(norma*(norma - normae))) */
                                   /*    right <= (1+u)^4exact(right) */
    _be_real_inv( c, a);           /* c = (1+f)(1/a) with f\in\C and ||f||<=u */
    double u = BEFFT_U;                   /* no error */
    double normc   = BEFFT_ABS(*c);       /* no error */
    *ce = BEFFT_RELER_be_real_inv*u;      /* no error */
    *ce = (*ce)*normc;                    /* ce = (1+u)*exact( ce*normc ) */
    *ce = (*ce) + right;                  /* ce <= (1+u)^5 exact(ce) */
    *ce = (*ce)/(1-6*u); /* use Property 16 of Muller book to correct error: (1+u)^{k}|ce|<=RN(|ce|/(1-(k+1)*u))*/
}

/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

/*     1/(1-u) <= (1+u)^2                   */
/* <=> 1 <= (1+u) - u^2(1+u)                */
/* <=> u >= u^2(1+u)         OK for small u */  

/*     (1/(1-u))^n <= (1+u)^(n+1) for small u and n */

/* | (b + be)/(a + ae) - b/a | <= | (a(b+be)-b(a+ae))/( a(a+ae) ) | <= | (a*be-b*ae) / a(a+ae) | */
/*                      <= (|a||be| + |b||be|)/( |a|(|a|-|ae|)                                   */
/* | RN(1/a) - 1/a |    <= u*|RN(a/b)| + (|a||be| + |b||be|)/( |a|(|a|-|ae|)                     */
void _be_real_div_aber( double * c, double *ce,
                        double   a, double  ae,
                        double   b, double  be) {     
    
    double normb  = BEFFT_ABS(b);  /* no error */
    double normbe = BEFFT_ABS(be); /* no error */
    if ( normbe >= normb ) {
        _be_real_div( c, a, b);
        *ce= (double)INFINITY;
        return;
    }
    double norma  = BEFFT_ABS(a);  /* no error */
    double normae = BEFFT_ABS(ae); /* no error */
    double normabe= norma*normbe;  /*  normabe <= (1+u)  exact(normabe) */
    double normbae= normb*normae;  /*  normbae <= (1+u)  exact(normbae) */
    double plus   = normabe + normbae;  /*plus <= (1+u)  (normabe + normbae) */
                                        /*plus <= (1+u)^2exact(plus) */
    double right = normb - normbe; /*    right >= (1-u)  exact(right) */
           right = normb*right;    /*    right >= (1-u)^2exact(right) */
                                   /* thus 1/right <= (1+u)^3exact(right) */
           right = plus/right;     /*    right <= (1+u)^6exact(plus/right)   */
    
    _be_real_div( c, a, b );       /* c = (1+f)ab with f\in\C and ||f||<=u */
    double u = BEFFT_U;                   /* no error */
    double normc   = BEFFT_ABS(*c);       /* no error */
    *ce = BEFFT_RELER_be_real_div*u;      /* no error */
    *ce = (*ce)*normc;                    /* ce = (1+u)*exact( ce*normc ) */
    *ce = (*ce) + right;                  /* ce <= (1+u)^7 exact(ce) */
    *ce = (*ce)/(1-8*u); /* use Property 16 of Muller book to correct error: (1+u)^{k}|ce|<=RN(|ce|/(1-(k+1)*u))*/
}

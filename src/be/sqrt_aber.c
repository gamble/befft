/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be.h"

/* if no underflow , */
/* for all double x, | evact(sqrt(x)) - computed(sqrt(x)) | <  u*computed(sqrt(x)) */ 
/* see for instance HAL Id: hal-00934443
https://hal.inria.fr/hal-00934443v2
On relative errors of floating-point operations: optimal
bounds and applications                                 
Jeannerod and Rump    */

/* for error, use |sqrt(x+/-a) - sqrt(x)| <= sqrt(x) ( 1- sqrt( 1-|a|/x ) ) */
/* valid for 0 <= |a| <= x */
void _be_real_sqrt_aber( double *dest, double *dest_aber,
                         double  srca, double  srca_aber ) {
    
    if (srca<0) {
        *dest = NAN;
        *dest_aber = INFINITY;
        return;
    } else if ( (srca_aber>srca) ) {    
        *dest = sqrt( srca + srca_aber )/2; 
        *dest_aber = *dest;                 /* *dest <= (1+u)^2 exact ( sqrt( srca + srca_aber )/2 ) */
        *dest_aber = (*dest_aber)/(1-3*BEFFT_U);
        return;
    }
    double u   = BEFFT_U;
    double dest_relerr = srca_aber/srca ;   /* dest_relerr <= (1+u)  exact ( srca_aber/srca ) */
           dest_relerr = 1 - dest_relerr;   /* dest_relerr <= (1+u)^2exact ( 1-srca_aber/srca ) */
           dest_relerr = sqrt(dest_relerr); /* dest_relerr <= (1+u)^3exact ( sqrt(1-srca_aber/srca) ) */
           dest_relerr = 1 - dest_relerr;   /* dest_relerr <= (1+u)^4exact ( 1-sqrt(1-srca_aber/srca) ) */
    
    *dest = sqrt(srca);                     /* relative error is u*BEFFT_RELER_be_real_sqrt     */
                                            /* i.e. *dest <= (1+u) exact ( sqrt( *dest ) ) */
    *dest_aber = (*dest)*dest_relerr;       /* dest_aber <= (1+u)^6exact ( sqrt(*dest) * (1-sqrt(1-srca_aber/srca)) ) */
    double err = BEFFT_ABS(*dest)*BEFFT_RELER_be_real_sqrt*u; /* err <= (1+u)*exact(err) */ 
    *dest_aber = *dest_aber + err;                         /* (*dest_aber) <= (1+u)exact(*dest_aber + err) */
                                                           /* (*dest_aber) <= (1+u)^7exact( sqrt(*dest) * (1-sqrt(1-srca_aber/srca))+err ) */
    *dest_aber = (*dest_aber)/(1-8*u);
    
//     double dest_relerr = srca_aber/srca ;   /* dest_relerr <= (1+u)  exact ( srca_aber/srca ) */
//            dest_relerr = 1 - dest_relerr;   /* dest_relerr <= (1+u)^2exact ( 1- srca_aber/srca ) */
//            dest_relerr = sqrt(dest_relerr); /* dest_relerr <= (1+u)^3exact ( sqrt(1- srca_aber/srca) ) */
//            dest_relerr = 1 - dest_relerr;   /* dest_relerr <= (1+u)^4exact ( sqrt(1- srca_aber/srca) ) */
//     
//     *dest = sqrt(srca);                     /* relative error is u*BEFFT_RELER_be_real_sqrt     */
//     double u   = BEFFT_U;                   /* i.e. *dest <= (1+u) exact ( sqrt( *dest ) ) */
//     double dest_corr = *dest/(1-u);
//     *dest_aber = dest_corr*dest_relerr/(1-2*u);
//     double err = BEFFT_ABS(*dest)*BEFFT_RELER_be_real_sqrt*u; /* err <= (1+u)*exact(err) */ 
//     *dest_aber = *dest_aber + err;                         /* (*dest_aber) <= (1+u)exact(*dest_aber + err) */
//                                                            /* (*dest_aber) <= (1+u)^2exact(sqrt(srca_aber)) + (1+u)^2*exact(err) */
//     *dest_aber = (*dest_aber)/(1-3*u);
    
}

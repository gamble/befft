/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef BE_VEC_H
#define BE_VEC_H

#include "befft_base.h"
#include "be.h"


#ifdef __cplusplus
extern "C" {
#endif
  
typedef struct {
    double *   _real; /* the real parts */
    double *   _imag; /* the imag parts */
    double     _aber; /* the max of the absolute errors, <0 if unknown*/
    be_size_t  _size; /* the length of vectors */
} be_vec_struct;

typedef be_vec_struct be_vec_t[1];
typedef be_vec_struct * be_vec_ptr;

#define be_vec_realref(X) ( (X)->_real )
#define be_vec_imagref(X) ( (X)->_imag )
#define be_vec_aberref(X) ( (X)->_aber )
#define be_vec_sizeref(X) ( (X)->_size )

void be_vec_init( be_vec_t dest, be_size_t size );
void be_vec_clear( be_vec_t dest );

/* printing */
void be_vec_fprint(FILE * file, const be_vec_t x);

/* infinite and two norms */
double _be_real_vec_infnorm( double * _real, be_size_t size );

double _be_vec_twonorm_ub( double * _real, double * _imag, be_size_t size );

BEFFT_INLINE void be_vec_print (const be_vec_t b) {
    be_vec_fprint(stdout, b);
}

int _be_vec_omega_precomp( double omega_re[], double omega_im[], double *omega_ae, be_size_t k );
BEFFT_INLINE int be_vec_omega_precomp( be_vec_t dest, be_size_t k ){
    return _be_vec_omega_precomp( be_vec_realref(dest), be_vec_imagref(dest), &be_vec_aberref(dest), k );
}

#ifdef BEFFT_HAS_ARB
void _be_vec_omega_uint_with_arb( double omega_re[], double omega_im[], double *omega_ae, uint n, slong prec );
BEFFT_INLINE void be_vec_omega_uint_with_arb( be_vec_t dest, uint n, slong prec ){
    _be_vec_omega_uint_with_arb( be_vec_realref(dest), be_vec_imagref(dest), &be_vec_aberref(dest), n, prec );
}
#endif

#ifdef BEFFT_HAS_MPFR
void _be_vec_omega_uint_with_mpfr( double omega_re[], double omega_im[], double *omega_ae, uint n);
BEFFT_INLINE void be_vec_omega_uint_with_mpfr( be_vec_t dest, uint n){
    _be_vec_omega_uint_with_mpfr( be_vec_realref(dest), be_vec_imagref(dest), &be_vec_aberref(dest), n);
}
#endif

#ifdef BEFFT_HAS_CRLIBM
void _be_vec_omega_uint_with_crlibm( double omega_re[], double omega_im[], double *omega_ae, uint n);
#endif

/* sets i-th cb of dest to -j-th order 2^k root */
void _be_vec_omega_uint( double omega_re[], double omega_im[], double *omega_ae, uint k );
BEFFT_INLINE void be_vec_omega_uint( be_vec_t dest, uint k ) {
    _be_vec_omega_uint( be_vec_realref(dest), be_vec_imagref(dest), &be_vec_aberref(dest), k );
}

#ifdef __cplusplus
}
#endif

#endif

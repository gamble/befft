/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef BEFFT_H
#define BEFFT_H

#include "befft_base.h"
#include "be.h"
#include "be_vec.h"

#define BEFFT_TWOTONM1(n) ( ( ( (ulong) 0x1 )<<n) -1 )
#define BEFFT_KTHBIT(j, k) ((j>>k)%2)

typedef struct {
    uint          _log2size; /* the log 2 of the size */
    be_size_t *   _fft_tree; /* the fft tree */
    be_vec_struct _omegas; /* the 0,-1,-2,...,-N/2-th roots of units of order N */
    be_vec_ptr    _omegass; /* sub-tables of the roots of units */
} befft_fft_rad2_struct;
typedef befft_fft_rad2_struct befft_fft_rad2_t[1];

#define befft_fft_rad2_log2sizeref(X) ( (X)->_log2size ) 
#define befft_fft_rad2_fft_treeref(X) ( (X)->_fft_tree ) 
#define befft_fft_rad2_omegasref(X)   (&(X)->_omegas )
#define befft_fft_rad2_omegassref(X)  ( (X)->_omegass )

void befft_fft_rad2_init( befft_fft_rad2_t rad2, uint log2size );

/* let omegas be the vector in \C^(2^(log2size-1)) defined as:                                              */
/* omegas = [ omega(0, 2^(log2size)), ..., omega(2^(log2size-1)-1, 2^(log2size)) ]                          */
/* where omega(j, k) = e^(-j*2*Pi/k)                                                                        */
/* let omegas_re contain double approx of the real parts of omegas,                                         */
/* let omegas_im contain double approx of the imaginary parts of omegas                                     */
/* and omegas_ae the maximum absolute/relative error:                                                       */
/* forall j\in[0,2^(log2size-1)-1],                                                                         */
/*            (omegas_re[j] + Iomega_im[j])  = omega(j, k) + \delta with \delta\in\C and |\delta|<=omega_ae */
/*        <=> (omegas_re[j] + Iomega_im[j]) <= (1+omega_ae)*omega(j, k)                                     */
void befft_fft_rad2_init_with_omegas( befft_fft_rad2_t rad2, 
                                      double omegas_re[], double omegas_im[], double omegas_ae, 
                                      uint log2size );

void befft_fft_rad2_clear( befft_fft_rad2_t rad2 );

/* Specifications:                                                               */
/* Preconditions:  Assume the rounding mode is NEAREST.                          */
/*                 y_real, y_imag, x_real, x_imag are vectors of size 2^n        */
/*                 y_max_abs_error is a pointer on real number.                  */
/* Postconditions: Let A be the 2^n*2^n matrix of the fft transform of size 2^n. */
/*                 Let x=(x_real + Ix_imag) and z = Ax be the exact fft of x.    */
/*                 Sets y=(y_real + Iy_imag) to an approximation of z.           */
/*                 If the return value is 0, no underflow happend and            */
/*                 y and y_max_abs_error satisfy:                                */
/*                               ||z-y||<= y_max_abs_error.                      */
int befft_fft_rad2_exact_input ( double * restrict y_real, double * restrict y_imag, double * restrict y_max_abs_error,
                                 double * restrict x_real, double * restrict x_imag, uint n );   
/* More comments: the function implements:                                                                                     */
/* @article{brisebarre2020error,                                                                                               */                                                                                            
/*   title={Error analysis of some operations involved in the Cooley-Tukey Fast Fourier Transform},                            */
/*   author={Brisebarre, Nicolas and Jolde{\c{s}}, Mioara and Muller, Jean-Michel and Nane{\c{s}}, Ana-Maria and Picot, Joris},*/
/*   journal={ACM Transactions on Mathematical Software (TOMS)},                                                               */
/*   volume={46},                                                                                                              */
/*   number={2},                                                                                                               */
/*   pages={1--27},                                                                                                            */
/*   year={2020},                                                                                                              */
/*   publisher={ACM New York, NY, USA}                                                                                         */
/* }              */

int befft_fft_rad2_exact_input_precomp ( double * restrict y_real, double * restrict y_imag, double * restrict y_max_abs_error,
                                         double * restrict x_real, double * restrict x_imag, befft_fft_rad2_t rad2 );

/* Specifications:                                                               */
/* Preconditions:  Assume the rounding mode is NEAREST.                          */
/*                 y_real, y_imag, x_real, x_imag are vectors of size 2^n        */
/*                 y_max_abs_error and x_max_abs_error are real numbers.         */
/* Postconditions: Let A be the 2^n*2^n matrix of the fft transform of size 2^n. */
/*                 Let x=(x_real + Ix_imag) and w and e in C^N s.t.:             */
/*                         w = x+e with ||e||_infty <= x_max_abs_error.          */
/*                 Let z = Aw be the exact fft of x.                             */
/*                 Sets y=(y_real + Iy_imag) to an approximation of z.           */
/*                 If the return value is 0, no underflow happend and            */
/*                 y and y_max_abs_error satisfy:                                */
/*                               ||z-y||<= y_max_abs_error.                      */
int befft_fft_rad2_ub_abs_error ( double y_real[], double y_imag[], double * y_max_abs_error,
                                  double x_real[], double x_imag[], double x_max_abs_error, uint n );
/* More comments: A has norm N, thus */
/* ||z-y||=||Aw - Ax|| = ||Ae|| <= N||e|| <= N*sqrt(N)*||e||_infty */

int befft_fft_rad2_ub_abs_error_precomp ( double y_real[], double y_imag[], double * y_max_abs_error,
                                          double x_real[], double x_imag[], double x_max_abs_error, befft_fft_rad2_t rad2 );

/* Specifications:                                                               */
/* Preconditions:  Assume the rounding mode is NEAREST.                          */
/*                 y_real, y_imag, y_aber, x_real, x_imag, x_aber                */
/*                                                are vectors of size 2^n        */
/* Postconditions: Let A be the 2^n*2^n matrix of the fft transform of size 2^n. */
/*                 Let x=(x_real + Ix_imag) and w and e in C^N s.t.:             */
/*                         w = x+e with ||e[j]|| <= x_aber[j].                   */
/*                 Let z = Aw be the exact fft of x.                             */
/*                 Sets y=(y_real + Iy_imag) to an approximation of z.           */
/*                 If the return value is 0, no underflow happend and            */
/*                 y and y_aber satisfy, for 1 <= j <= N:                        */
/*                               ||z[j]-y[j]||<= y_aber[j].                      */
int befft_fft_rad2_vec_abs_error ( double y_real[], double y_imag[], double y_aber[],
                                   double x_real[], double x_imag[], double x_aber[], uint n);
/* More comments: computes explicitly Ax and y_aber the vector s.t. y_aber[j] = ||Ax_aber[j]|| */
/* then let ||Aw[j] - Ax[j]|| = ||Ae[j]|| <= ||Ax_aber[j]|| <= y_aber[j] */ 

int befft_fft_rad2_vec_abs_error_precomp ( double y_real[], double y_imag[], double y_aber[],
                                           double x_real[], double x_imag[], double x_aber[], befft_fft_rad2_t rad2);

int befft_fft_rad2_dynamic    ( double y_real[], double y_imag[], double y_aber[],
                                double x_real[], double x_imag[], double x_aber[], uint n );

int befft_fft_rad2_dynamic_precomp    ( double y_real[], double y_imag[], double y_aber[],
                                        double x_real[], double x_imag[], double x_aber[], 
                                        befft_fft_rad2_t rad2 );

int befft_fft_rad2_dynamic_gathered    ( double y[], double x[], uint n );

int befft_fft_rad2_dynamic_precomp_gathered    ( double y[], double x[], befft_fft_rad2_t rad2 );

/* Exported for tests */

double befft_FirstStep_static( double * restrict y_real, double * restrict y_imag,
                               double * restrict x_real, double * restrict x_imag,
                               uint n);

double befft_SecondStep_static( double * restrict y_real, double * restrict y_imag, 
                                double * restrict x_real, double * restrict x_imag, 
                                uint n);

double befft_OneStep_static( double * restrict y_real, double * restrict y_imag,
                             double * restrict x_real, double * restrict x_imag, 
                             befft_fft_rad2_t rad2, 
                             uint k);

/* For tests */

double befft_FirstStep_static_vect( double * restrict y_real, double * restrict y_imag,
                                    double * restrict x_real, double * restrict x_imag,
                                    befft_fft_rad2_t rad2);

double befft_SecondStep_static_vect( double * restrict y_real, double * restrict y_imag,
                                     double * restrict x_real, double * restrict x_imag, 
                                     uint n);

double befft_OneStep_static_vect( double * restrict y_real, double * restrict y_imag,
                                  double * restrict x_real, double * restrict x_imag,
                                  befft_fft_rad2_t rad2, 
                                  uint k);

int befft_fft_rad2_exact_input_vect_precomp ( double * restrict y_real, double * restrict y_imag, double * restrict y_max_abs_error,
                                              double * restrict x_real, double * restrict x_imag, befft_fft_rad2_t rad2 );

int befft_fft_rad2_ub_abs_error_vect_precomp ( double y_real[], double y_imag[], double * y_max_abs_error,
                                               double x_real[], double x_imag[], double x_max_abs_error, befft_fft_rad2_t rad2 );

int befft_fft_rad2_vec_abs_error_vect_precomp ( double y_real[], double y_imag[], double y_aber[],
                                                double x_real[], double x_imag[], double x_aber[], befft_fft_rad2_t rad2);

/* For memory */
int befft_fft_rad2_dynamic_old    ( double * restrict y_real, double * restrict y_imag, double * restrict y_aber,
                                 double * restrict x_real, double * restrict x_imag, double * restrict x_aber, 
                                 uint n );

int befft_fft_rad2_dynamic_old_precomp    ( double * restrict y_real, double * restrict y_imag, double * restrict y_aber,
                                         double * restrict x_real, double * restrict x_imag, double * restrict x_aber,
                                         befft_fft_rad2_t rad2 );
#endif

/****************************************************************************
        Copyright (C) 2022-2023 Guillaume Moroz <guillaume.moroz@inria.fr>
                                Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "befft.h"
#include <float.h>

#if _MSC_VER
#define BEFFT_SSCANF sscanf_s
#else
#define BEFFT_SSCANF sscanf
#endif

int main( int argc, char* argv[] ) {
    uint kmax = 12;
    int verbose = 0;
    int nbdigits= 17;
    
    if (argc >= 2)
        BEFFT_SSCANF(argv[1], "%u", &kmax);
    
    if (argc >= 3)
        BEFFT_SSCANF(argv[2], "%d", &verbose);
    
    if (argc >= 4)
        BEFFT_SSCANF(argv[2], "%d", &nbdigits);
    
    int checkOK = 1;
    
#if defined(BEFFT_HAS_ARB) && defined(BEFFT_HAS_MPFR)
    
    for (uint k = 0; k < kmax; k++ ) {
        be_size_t K = ((be_size_t) 0x1) << k;
        be_vec_t omegas_prec, omegas_mpfr, omegas_arb;
        be_vec_init(omegas_prec, K);
        be_vec_init(omegas_mpfr, K);
        be_vec_init(omegas_arb,  K);
        
        int precompOK = be_vec_omega_precomp( omegas_prec, K );
        be_vec_omega_uint_with_mpfr( omegas_mpfr, k );
        be_vec_omega_uint_with_arb( omegas_arb, k, 2*BEFFT_PREC );
        
        int precEQmpfr = 1, precEQarb = 1, mpfrEQarb=1;
        for (be_size_t i=0; i<K; i++) {
            
            int precEQmpfr_t = precompOK && ( be_vec_realref( omegas_prec )[i] == be_vec_realref( omegas_mpfr )[i] ) &&
                                            ( be_vec_imagref( omegas_prec )[i] == be_vec_imagref( omegas_mpfr )[i] ) ;
            int precEQarb_t  = precompOK && ( be_vec_realref( omegas_prec )[i] == be_vec_realref( omegas_arb  )[i] ) &&
                                            ( be_vec_imagref( omegas_prec )[i] == be_vec_imagref( omegas_arb  )[i] ) ;
            int mpfrEQarb_t  = ( be_vec_realref( omegas_mpfr )[i] == be_vec_realref( omegas_arb  )[i] ) &&
                               ( be_vec_imagref( omegas_mpfr )[i] == be_vec_imagref( omegas_arb  )[i] ) ;
                                       
            precEQmpfr = precEQmpfr && precEQmpfr_t;
            precEQarb  = precEQarb  && precEQarb_t;
            mpfrEQarb  = mpfrEQarb  && mpfrEQarb_t;
                
            if (verbose) {
                if (precompOK && (!precEQmpfr_t) ) {
                    printf("i: %zu, precEQmpfr: %d\n", i, precEQmpfr);
                    printf("precomp: "); _be_printd( be_vec_realref( omegas_prec )[i], be_vec_imagref( omegas_prec )[i], be_vec_aberref( omegas_prec ), nbdigits ); printf("\n");
                    printf("mpfr   : "); _be_printd( be_vec_realref( omegas_mpfr )[i], be_vec_imagref( omegas_mpfr )[i], be_vec_aberref( omegas_mpfr ), nbdigits ); printf("\n");
                }
                if (precompOK && (!precEQarb_t) ) {
                    printf("i: %zu, precEQarb: %d\n", i, precEQarb);
                    printf("precomp: "); _be_printd( be_vec_realref( omegas_prec )[i], be_vec_imagref( omegas_prec )[i], be_vec_aberref( omegas_prec ), nbdigits ); printf("\n");
                    printf("arb    : "); _be_printd( be_vec_realref( omegas_arb  )[i], be_vec_imagref( omegas_arb  )[i], be_vec_aberref( omegas_arb ), nbdigits ); printf("\n");
                }
                if (!mpfrEQarb_t) {
                    printf("i: %zu, precEQarb: %d\n", i, mpfrEQarb);
                    printf("mpfr   : "); _be_printd( be_vec_realref( omegas_mpfr )[i], be_vec_imagref( omegas_mpfr )[i], be_vec_aberref( omegas_mpfr ), nbdigits ); printf("\n");
                    printf("arb    : "); _be_printd( be_vec_realref( omegas_arb  )[i], be_vec_imagref( omegas_arb  )[i], be_vec_aberref( omegas_arb ), nbdigits ); printf("\n");
                }
            }
        }
        
        checkOK = checkOK && mpfrEQarb && 
                  ( (!precompOK) || precEQmpfr ) && ( (!precompOK) || precEQarb ) ;
        
//         if (verbose) {
//             printf("----- k = %u, K = %zu -----\n", k, K);
//             if (precompOK){
//                 printf(" precEQmpfr: %d\n", precEQmpfr );
//                 printf(" precEQarb : %d\n", precEQarb );
//             }
//             printf(" mpfrEQarb : %d\n", mpfrEQarb );
//         
//             printf("precompiled order K roots of unit:\n");
//             be_vec_print( omegas_prec );
//             printf("\n");
//             
//             printf("mpfr order K roots of unit:\n");
//             be_vec_print( omegas_mpfr );
//             printf("\n");
//             
//             printf("arb order K roots of unit:\n");
//             be_vec_print( omegas_arb );
//             printf("\n");
//         
//             printf("---------------------------\n");
//             printf("\n");
//             
// //             checkOK=1;
//         }
        
        be_vec_clear(omegas_arb);
        be_vec_clear(omegas_mpfr);
        be_vec_clear(omegas_prec);
    }
    if (verbose)
        printf(" checkOK : %d\n", checkOK );
    return !checkOK;
#else
    checkOK = 0;
    if (verbose)
        printf(" checkOK : %d\n", checkOK );
    return 1;
#endif
}

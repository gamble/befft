# /****************************************************************************
#         Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
#                            Remi Imbach     <remi.imbach@laposte.net>
#  
#     This file is part of befft.
# 
#     befft is free software: you can redistribute it and/or modify 
#     it under the terms of the GNU Lesser General Public License as 
#     published by the Free Software Foundation, either version 3 of 
#     the License, or (at your option) any later version.
# 
#     befft is distributed in the hope that it will be useful, but 
#     WITHOUT ANY WARRANTY; without even the implied warranty of 
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#     See the GNU Lesser General Public License for more details.
# 
#     You should have received a copy of the GNU Lesser General Public 
#     License along with befft. If not, see <https://www.gnu.org/licenses/>. 
# *****************************************************************************/

k_precomp:=2048;
omegas:=[]:
Digits:=30:
for j from 0 to k_precomp-1 do
    omegajk:= exp(-j*Pi*I/k_precomp);
    omegas:=[op(omegas), omegajk];
end do:
omegas:

res:="#include \"be_vec.h\"\n\n":

res:=cat(res, "int be_vec_omega_precomp( be_vec_t dest, ulong k ) {\n"):
res:=cat(res, "    ulong k_precomp = ", k_precomp, ";              \n"):
res:=cat(res, "    if (k>k_precomp)                                \n"):
res:=cat(res, "        return 0;                                   \n"):
res:=cat(res, "    double omega_re[", k_precomp, "] = {            \n"):
for j from 1 to k_precomp-1 do
    re:=evalf(Re(omegas[j])):
    res:=cat(res, "                           ", re, ",\n"):
end do:
re:=evalf(Re(omegas[k_precomp])):
res:=cat(res, "                           ", re, "\n"):
res:=cat(res, "                          };\n"):
res:=cat(res, "    double omega_im[", k_precomp, "] = {            \n"):
for j from 1 to k_precomp-1 do
    im:=evalf(Im(omegas[j])):
    res:=cat(res, "                           ", im, ",\n"):
end do:
im:=evalf(Im(omegas[k_precomp])):
res:=cat(res, "                           ", im, "\n"):
res:=cat(res, "                          };\n"):

res:=cat(res, "    ulong shift = k_precomp/k;\n"):
res:=cat(res, "    for (ulong j=0; j<k; j++){\n"):
res:=cat(res, "        be_vec_realref(dest)[j] = omega_re[j*shift];\n"):
res:=cat(res, "        be_vec_imagref(dest)[j] = omega_im[j*shift];\n"):
res:=cat(res, "    }\n"):
res:=cat(res, "    if (k<=2) {\n"):
res:=cat(res, "        be_vec_aberref(dest)    = 0.;\n"):
#res:=cat(res, "        be_vec_reerref(dest)    = 0.;\n"):
res:=cat(res, "    } else {\n"):
res:=cat(res, "        be_vec_aberref(dest)    = ldexp(1, -BEFFT_PREC);\n"):
#res:=cat(res, "        be_vec_reerref(dest)    = be_vec_aberref(dest);\n"):
res:=cat(res, "    }\n"):
res:=cat(res, "    return 1;\n"):
res:=cat(res, "}\n"):
print(res):

fd:=fopen("omega_precomp.c",WRITE):
fprintf(fd, "%s", res):
fclose(fd):

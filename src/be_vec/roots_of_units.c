/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be_vec.h"

#ifdef BEFFT_HAS_ARB
void _be_vec_omega_uint_with_arb( double omega_re[], double omega_im[], double *omega_ae, uint n, slong prec ){
    
    if (n==0) {
        omega_re[0] = 1.;
        omega_im[0] = 0.;
        *omega_ae = 0.;
        return;
    }
    
    be_size_t N  = ((be_size_t) 0x1)<<n;
    be_size_t No2= N>>1;
    be_size_t N2 = N<<1;
    
    acb_ptr x = _acb_vec_init(No2);
    _acb_vec_unit_roots(x, -N2, No2, prec);
    
    double u = ldexp(1, -BEFFT_PREC);
    double temp_aber;
//     double * omega_re = be_vec_realref(dest);
//     double * omega_im = be_vec_imagref(dest);
    
    /* save underflow exception */
    fenv_t fe;
    feholdexcept(&fe);
    omega_re[0] = 1.;
    omega_im[0] = 0.;
    *omega_ae = 0.;
    for (be_size_t j=1; j<No2; j++) {
//         _be_set_acb(omega_re + j, omega_im + j, &temp_aber, x+j);
        omega_re[j] = arf_get_d( arb_midref( acb_realref( x+j )), ARF_RND_NEAR );
        omega_im[j] = arf_get_d( arb_midref( acb_imagref( x+j )), ARF_RND_NEAR );
        omega_re[N-j] = -omega_re[j];
        omega_im[N-j] = omega_im[j];
        if ( arb_can_round_arf( acb_realref( x+j ), BEFFT_PREC, ARF_RND_NEAR )
            && arb_can_round_arf( acb_imagref( x+j ), BEFFT_PREC, ARF_RND_NEAR ) ) {
            /* let omega = a + ib be the unit root, RN(omega) = RN(a) + RN(b) */
            /* RN(a) = (1+ar)a, RN(b) = (1+br)b with |ar| and |br| <= u       */
            /* then |RN(omega) - omega| = |ar*a + iBr*b| <= u|a+ib| <= u      */
            temp_aber = u;
        } else {
            /* let omega = a + ib be the unit root, RN(omega) = RN(a) + RN(b)     */
            /* RN(a) = (1+ar)a + aa, RN(b) = (1+br)b + ba with |ar| and |br| <= u */
            /* then |RN(omega) - omega| = |ar*a +aa + i(Br*b + bb)| <= u|a+ib| + |aa| + |bb| <= u + |aa| + |bb|*/
            temp_aber = u;
            temp_aber = temp_aber + mag_get_d( arb_radref( acb_realref( x+j ) ) );
            temp_aber = temp_aber + mag_get_d( arb_radref( acb_imagref( x+j ) ) );
            /* should correct temp_aber as temp_aber = temp_aber/(1-3*u) */
        }
        *omega_ae = BEFFT_MAX(*omega_ae, temp_aber);
    }
    omega_re[No2] = 0.;
    omega_im[No2] = -1.;
    /* ignore underflow exceptions from arf_get_d and mag_get_d */
    /* their part is mult and div free                          */
    feclearexcept (FE_UNDERFLOW);
    feupdateenv(&fe);
    
    /* correct the error if necessary: */
    if (*omega_ae > u)
        *omega_ae = *omega_ae/(1-3*u);
    
    _acb_vec_clear(x, No2);
}
#endif 

#ifdef BEFFT_HAS_MPFR
/* Lemma 1: let 0 < u <= 1                            */
/*          then 0 < u^2 < u < 1 and (1+u)^2 < 1 + 4u */
/* Lemma 2: let 0 <= a <= Pi/(2(1+4u))                */
/*          and 0 <= at<= (1+u)^u a                   */
/*          then sin(at) <= (1+8u)*sin(a)             */
/*          and |cos(at)|<= (1+8u)*|cos(a)|           */
/* Lemma 3: let p >= n-1                              */
/*          and a <= 2*(2^(n-1)-1)*Pi/2^(n)           */
/*          then a <= Pi/(2(1+4u)) with u = 2^-p      */
void _be_vec_omega_uint_with_mpfr( double omega_re[], double omega_im[], double *omega_ae, uint n){
    
    if (n==0) {
        omega_re[0] = 1.;
        omega_im[0] = 0.;
        *omega_ae = 0.;
        return;
    }
//     printf("here!!!\n");
    mpfr_prec_t prec = 2*BEFFT_PREC+10;
    /* let u  = 2^-BEFFT_PREC */
    /* let up = 2^-prec */
    /* ensures all the integers between 0 and 2^n are exactly represented by floating points with prec-bits mantissas*/
    /* and prec >= n-1 */
    if (n>=prec) {
        prec = 2*n+10;
    }
    mpfr_set_default_prec (prec);
    
    mpfr_t k2oq;                 /* represents 2k/q = 2k/2^(n+1) = k/2^(n) */
    mpfr_t k2Pioq;               /* represents 2kPi/q */
    mpfr_t cosk2Pioq, sink2Pioq; /* represent  cos( 2kPi/q ) and sin( 2kPi/q ) */
    mpfr_t Pi;
    mpfr_init(k2oq);
    mpfr_init(k2Pioq);
    mpfr_init(Pi);
    mpfr_init(cosk2Pioq);
    mpfr_init(sink2Pioq);
    
    fenv_t fe;
    feholdexcept(&fe);
    be_size_t N=((be_size_t) 0x1)<<n;
    be_size_t No2= N>>1;
    
    mpfr_const_pi (Pi, MPFR_RNDN);   /* Pi <= (1+up)exact(Pi) */
    
    omega_re[0] = 1.;
    omega_im[0] = 0.;
    for( be_size_t k = 1; k<No2; k++ ){
        mpfr_set_ui(k2oq, k, MPFR_RNDN);          /* is exact */
        mpfr_neg(   k2oq, k2oq, MPFR_RNDN); 
        mpfr_div_2ui(k2oq, k2oq, n, MPFR_RNDN);   /* is exact */
        mpfr_mul(    k2Pioq, k2oq, Pi, MPFR_RNDN);/*k2Pioq <= (1+up)*exact( k2oq*Pi )        */
                                                  /*       <= (1+up)^2*(k/2^n)*exact(Pi) */
        mpfr_sin_cos(sink2Pioq, cosk2Pioq, k2Pioq, MPFR_RNDN); /* sink2Pioq <= (1+8up)sin(exact( k2oq*Pi )) from Lemma above */
                                                               /* cosk2Pioq <= (1+8up)cos(exact( k2oq*Pi )) from Lemma above */
        
        omega_re[k] = mpfr_get_d(cosk2Pioq, MPFR_RNDN);
        omega_im[k] = mpfr_get_d(sink2Pioq, MPFR_RNDN);
        omega_re[N-k] = -omega_re[k];
        omega_im[N-k] = omega_im[k];
    }
    omega_re[No2] = 0.;
    omega_im[No2] = -1.;
    *omega_ae    = ldexp(1, -BEFFT_PREC);
    
    feclearexcept (FE_UNDERFLOW);
    feupdateenv(&fe);
    
    mpfr_clear(k2oq);
    mpfr_clear(k2Pioq);
    mpfr_clear(Pi);
    mpfr_clear(cosk2Pioq);
    mpfr_clear(sink2Pioq);
}
#endif

/* sets i-th cb of dest to -j-th order 2^k root */
// void be_vec_omega_uint( be_vec_t dest, uint k ){
// #ifdef BEFFT_TIMINGS
//     clock_t start = clock();
// #endif
//     be_size_t K = ( (be_size_t) 0x1 ) << k;
//     /* this will success when k<=2048 */
//     int resprecomp = be_vec_omega_precomp(dest, K);
//     
//     if (resprecomp==0) {
//         
// #if defined(BEFFT_HAS_ARB)
//         be_vec_omega_ulong_with_arb( dest, k, 2*BEFFT_PREC);
// #elif defined(BEFFT_HAS_MPFR)
//         _be_vec_omega_ulong_with_mpfr( be_vec_realref(dest), be_vec_imagref(dest), &(be_vec_aberref(dest)), k );
// #endif
//     }
//     
// #ifdef BEFFT_TIMINGS
//     printf("k=%u, resprecomp = %d, time in omega : %f\n", k, resprecomp, (double) (clock() - start)/CLOCKS_PER_SEC );
// #endif
// }
void _be_vec_omega_uint( double omega_re[], double omega_im[], double *omega_ae, uint k ){
#ifdef BEFFT_TIMINGS
    clock_t start = clock();
#endif
    be_size_t K = ( (be_size_t) 0x1 ) << k;
    /* this will success when K<=2048 */
    int resprecomp = _be_vec_omega_precomp(omega_re, omega_im, omega_ae, K);
    if (resprecomp==0) {
#if defined(BEFFT_HAS_ARB)
        _be_vec_omega_uint_with_arb( omega_re, omega_im, omega_ae, k, 2*BEFFT_PREC);
#elif defined(BEFFT_HAS_MPFR)
        _be_vec_omega_uint_with_mpfr( omega_re, omega_im, omega_ae, k );
#endif
    }
    
#ifdef BEFFT_TIMINGS
    printf("_be_vec_omega_uint: k=%u, resprecomp = %d, time in omega : %f\n", k, resprecomp, (double) (clock() - start)/CLOCKS_PER_SEC );
#endif
}

/* this will never happen...*/
#ifdef BEFFT_HAS_CRLIBM
void _be_vec_omega_uint_with_crlibm( double omega_re[], double omega_im[], double *omega_ae, uint n){
    fenv_t fe;
    feholdexcept(&fe);
    ulong N  = ((ulong) 0x1)<<n;
    ulong No2= N>>1;
    omega_re[0] = 1.;
    omega_im[0] = 0.;
    for( ulong j = 1; j<No2; j++ ){
        double j2oN = ldexp( -((double) j), -n);
        omega_re[j] = cospi_rn( j2oN );
        omega_im[j] = sinpi_rn( j2oN );
        omega_re[N-j] = -omega_re[j];
        omega_im[N-j] = omega_im[j];
    }
    omega_re[No2] = 0.;
    omega_im[No2] = -1.;
    *omega_ae    = ldexp(1, -BEFFT_PREC);
    
    feclearexcept (FE_UNDERFLOW);
    feupdateenv(&fe);
}
#endif

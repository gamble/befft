/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be_vec.h"

/* assume size >= 1 */
double _be_vec_twonorm_ub( double * _real, double * _imag, be_size_t size ){
    double res = 0;
	if (size==0)
		return res;
    double re, im;
    double u   = ldexp(1, -BEFFT_PREC); /* is exact */
    for (be_size_t j = 0; j<size; j++) {
        /* assume res<=(1+u)^kj * exact(res) */
        re = _real[j]*_real[j]; /* re <= (1+u)*exact(re) */
        im = _imag[j]*_imag[j]; /* im <= (1+u)*exact(im) */
        res = res + re;         /* res<= (1+u)*exact(res+re) <= (1+u)res + (1+u)re <= (1+u)^(k+1)exact(res) + (1+u)^2exact(re) */
        res = res + im;         /* res<= (1+u)*exact(res+im) <= (1+u)res + (1+u)im */
                                /*    <= (1+u)*( (1+u)^(kj+1)exact(res) + (1+u)^2exact(re) ) + (1+u)^2exact(im) */
                                /*    <= (1+u)^(kj+2)exact(res) + (1+u)^3exact(re) + (1+u)^2exact(im) */
                                /*    <= (1+u)^(max(kj,1)+2) ( exact(res) + exact(re) + exact(im) ) */
//         res = res/(1-4*u);
    }
    /* when j=0, kj=0, k(j+1) = max(kj,1)+2 = 3 */
    /* when j=1, kj=3, k(j+1) = max(kj,1)+2 = 5 */
    /* when j=2, kj=5, k(j+1) = max(kj,1)+2 = 7 */
    /* when j=n, kj=?, k(j+1) = kj + 2 */
    /* k(size+1) = 3 + 2*(size-1) */
    res = sqrt(res);            /* res<= (1+u)*res <= (1+u)^(1 + 3 + 2*(size-1))*exact(res)*/
    res = res/(1-(5+2*(size-1))*u);
    return res;
}

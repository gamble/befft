/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#include "be_vec.h"

void be_vec_init( be_vec_t dest, be_size_t size ){
    be_vec_realref(dest) = (double *) befft_malloc( size * sizeof(double) );
    be_vec_imagref(dest) = (double *) befft_malloc( size * sizeof(double) );
    be_vec_aberref(dest) = -1.;
    be_vec_sizeref(dest) = size;
}
void be_vec_clear( be_vec_t dest ){
    befft_free( be_vec_realref(dest) );
    be_vec_realref(dest) = NULL;
    befft_free( be_vec_imagref(dest) );
    be_vec_imagref(dest) = NULL;
    be_vec_aberref(dest) = -1.;
    be_vec_sizeref(dest) = 0;
}

void be_vec_fprint(FILE * file, const be_vec_t x){
    fprintf(file, "size %zu vector [ \n", be_vec_sizeref(x));
    for (be_size_t j=0; j<be_vec_sizeref(x); j++) {
        _be_fprint(file, be_vec_realref(x)[j], be_vec_imagref(x)[j], -1.);
        if (j<be_vec_sizeref(x)-1)
            fprintf(file, ", \n");
    }
    fprintf(file, "] ");
    if (be_vec_aberref(x)>=0)
        fprintf(file, " [abs err: (%.5e)] ", be_vec_aberref(x) );
}

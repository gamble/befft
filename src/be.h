/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
                           Remi Imbach     <remi.imbach@laposte.net>
 
    This file is part of befft.

    befft is free software: you can redistribute it and/or modify 
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    befft is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with befft. If not, see <https://www.gnu.org/licenses/>. 
*****************************************************************************/

#ifndef BE_H
#define BE_H

#include "befft_base.h"

#ifdef __cplusplus
extern "C" {
#endif
    
typedef struct {
    double _real; /* the real part */
    double _imag; /* the imag part */
    double _aber; /* the absolute error, <0 if unknown*/
} be_struct;

typedef be_struct be_t[1];
typedef be_struct be_ptr;

#define be_realref(X) ( (X)->_real )
#define be_imagref(X) ( (X)->_imag )
#define be_aberref(X) ( (X)->_aber )

BEFFT_INLINE void be_init( be_t dest ){
    be_realref(dest) = 0.;
    be_imagref(dest) = 0.;
    be_aberref(dest) = -1; /* absolute error is unknown */
}
/* the following does nothing important but */
/* calls be_init to avoid compiler warnings */ 
BEFFT_INLINE void be_clear( be_t dest ) {
	be_init( dest );
}

/* printing */
void _be_fprint(FILE * file, const double real, const double imag, const double aber);
void be_fprint(FILE * file, const be_t x);

void _be_fprintd(FILE * file, const double real, const double imag, const double aber, const int d);
void be_fprintd(FILE * file, const be_t x, const int d);


BEFFT_INLINE void _be_print (const double real, const double imag, const double aber) {
    _be_fprint(stdout, real, imag, aber);
}
BEFFT_INLINE void be_print (const be_t b) {
    be_fprint(stdout, b);
}
BEFFT_INLINE void _be_printd (const double real, const double imag, const double aber, const int d) {
    _be_fprintd(stdout, real, imag, aber, d);
}
BEFFT_INLINE void be_printd (const be_t b, const int d) {
    be_fprintd(stdout, b, d);
}
/* not used but kept for record*/
BEFFT_INLINE int be_isexact(const be_t b) {
    return (be_aberref(b) == 0);
}
/* not used but kept for record*/
BEFFT_INLINE int be_iszero(const be_t b) {
    return ( (be_realref(b) == 0)&&(be_imagref(b) == 0)&&(be_isexact(b)) );
}
/* not used but kept for record*/
BEFFT_INLINE int be_isone(const be_t b) {
    return ( (be_realref(b) == 1)&&(be_imagref(b) == 0)&&(be_isexact(b)) );
}
/* not used but kept for record*/
BEFFT_INLINE int be_isonei(const be_t b) {
    return ( (be_realref(b) == 0)&&(be_imagref(b) == 1)&&(be_isexact(b)) );
}

/* if no underflow happens */
/* 1  => surely contains zero       */
/* 0  => surely separated from zero */
/* -1 => can not decide             */
int _be_contains_zero( const double real, const double imag, const double aber );
/* if no underflow happens */
/* 1  => surely contains zero       */
/* 0  => surely separated from zero */
/* -1 => can not decide             */
int _be_is_non_zero( const double real, const double imag, const double aber );

/* sets real and imag parts with unknown error */
void be_set_double( be_t dest, double real, double imag );
/* sets real and imag parts from exact numbers */
void be_set_double_exact( be_t dest, double real, double imag);
/* sets real and imag parts from unexact unmbers with known errors */
void be_set_double_error( be_t dest, double real, double imag, double aber );

/* sets real part from integer */
BEFFT_INLINE void _be_real_set_si( double * dest, slong src ) {
    *dest = (double) src;
}
void _be_real_set_si_aber( double * dest, double * dest_aber, slong src );

#ifdef BEFFT_HAS_ARB
/* sets a real ball B=B(dest, dest_aber) from an arb ball so that B contains ball */
/* returns 1 <=> both dest and dest_aber are finite */
int _be_real_set_arb( double * dest, double * dest_aber, const arb_t ball );
/* sets a complex ball B=B(dest_real + Idest_imag, dest_aber) from an acb ball so that B contains ball */
/* returns 1 <=> dest_real, dest_imag and dest_aber are finite */
int _be_set_acb( double * dest_real, double * dest_imag, double * dest_aber, const acb_t ball );
int be_set_acb( be_t dest, const acb_t ball );

/* sets an acb ball from B(ar, ai, ae) so that ball = (ar+/-ae) + I*(ai+/-ae) */
/* then ball contains B(ar+Iai, ae) */
void _be_get_acb ( acb_t dest, double ar, double ai, double ae );
#endif

/* assume x and y are >=0 */
/* returns 1 => B(x, x_aber) > B(y, y_aber) */
int _be_real_pos_gt( double x, double x_aber, double y, double y_aber );

int _be_real_pos_lt_2exp_si( double x, double x_aber, int e );

/* returns an upper bound to the 2 norm of real+I*imag */
/* with inequalities twonorm(z)<=sqrt(2)infnorm(z) */
/*                   twonorm(z)<=|real| + |imag|   */
/* the error is at most BEFFT_U */
BEFFT_INLINE double _be_twonorm_ub_fast( const double real, const double imag ) {
    return BEFFT_MIN( BEFFT_ABS(real) + BEFFT_ABS(imag),
                      BEFFT_SQRTTWO*BEFFT_MAX(BEFFT_ABS( real ), BEFFT_ABS( imag )) );
}
#define BEFFT_RELER_be_twonorm_ub_fast 1

BEFFT_INLINE void _be_abs( double * dest, 
                           double   src_re, double   src_im ) {
    *dest = sqrt( src_re*src_re + src_im*src_im );
}

void _be_abs_aber( double * dest, double * dest_aber, 
                   double   src_re, double   src_im, double   src_aber );

BEFFT_INLINE void _be_add(       double * dest_re,       double * dest_im, 
                           const double   srca_re, const double   srca_im,
                           const double   srcb_re, const double   srcb_im){
    *dest_re = srca_re + srcb_re;
    *dest_im = srca_im + srcb_im;
}
#define BEFFT_RELER_be_add 1 /* computed(add) = (1+f)*exact(add) with f\in\C and ||f||<=BEFFT_RELER_be_add*u */

/*sets dest to srca + srcb with unknown error */
/* used only for tests in test_devel/be.c */
BEFFT_INLINE void be_add( be_t dest, const be_t srca, const be_t srcb ){
    _be_add( &be_realref(dest), &be_imagref(dest), 
             be_realref(srca), be_imagref(srca),
             be_realref(srcb), be_imagref(srcb) );
    be_aberref(dest) = -1;
}

/* not used but kept for record*/
void _be_add_aber( double * dest_re, double * dest_im, double * dest_aber, 
                   double   srca_re, double   srca_im, double   srca_aber,
                   double   srcb_re, double   srcb_im, double   srcb_aber);

/*sets dest to srca + srcb with error analysis */
/* used only for tests in test_devel/be.c */
BEFFT_INLINE void be_add_error( be_t dest, const be_t srca, const be_t srcb ){
    _be_add_aber( &be_realref(dest), &be_imagref(dest), &be_aberref(dest),
                   be_realref(srca), be_imagref(srca), be_aberref(srca),
                   be_realref(srcb), be_imagref(srcb), be_aberref(srcb) );
}

void _be_real_add_aber( double *dest, double *dest_aber,
                        double  srca, double  srca_aber,
                        double  srcb, double  srcb_aber);

/* assume srcb, srcb_aber are >=0 */
void _be_add_error_be_real_pos( double * dest_re, double * dest_im, double * dest_aber,
                                double   srca_re, double   srca_im, double   srca_aber,
                                double   srcb,    double srcb_aber                     );

void _be_neg_aber( double * dest_re, double * dest_im, double * dest_aber, 
                   double   src_re,  double   src_im,  double src_aber);

BEFFT_INLINE void _be_sub( double * dest_re, double * dest_im, 
                           double   srca_re, double   srca_im,
                           double   srcb_re, double   srcb_im){
    *dest_re = srca_re - srcb_re;
    *dest_im = srca_im - srcb_im;
}

/*sets dest to srca - srcb with unknown error */
/* used only for tests in test_devel/be.c */
BEFFT_INLINE void be_sub( be_t dest, const be_t srca, const be_t srcb ){
    _be_sub( &be_realref(dest), &be_imagref(dest), 
             be_realref(srca), be_imagref(srca),
             be_realref(srcb), be_imagref(srcb) );
    be_aberref(dest) = -1;
}

/* not used but kept for record*/
BEFFT_INLINE void _be_sub_aber( double * dest_re, double * dest_im, double * dest_aber, 
                                 double   srca_re, double   srca_im, double   srca_aber,
                                 double   srcb_re, double   srcb_im, double   srcb_aber){
    _be_add_aber( dest_re, dest_im, dest_aber, 
                  srca_re, srca_im, srca_aber,
                  -srcb_re, -srcb_im, srcb_aber);
}

/*sets dest to srca - srcb with error analysis */
/* used only for tests in test_devel/be.c */
BEFFT_INLINE void be_sub_error( be_t dest, const be_t srca, const be_t srcb ){
    _be_sub_aber( &be_realref(dest), &be_imagref(dest), &be_aberref(dest),
                   be_realref(srca), be_imagref(srca), be_aberref(srca),
                   be_realref(srcb), be_imagref(srcb), be_aberref(srcb) );
}

void _be_real_sub_aber( double *dest, double *dest_aber,
                        double  srca, double  srca_aber,
                        double  srcb, double  srcb_aber);

BEFFT_INLINE void _be_mul_be_real( double * dest_re, double * dest_im, 
                                   double   srca_re, double   srca_im,
                                   double   srcb_re                   ){
    *dest_re = srca_re*srcb_re;
    *dest_im = srca_im*srcb_re;
}

#define BEFFT_RELER_be_mul_be_real 1
void _be_mul_be_real_aber( double * dest_re, double * dest_im, double * dest_aber, 
                           double   srca_re, double   srca_im, double   srca_aber,
                           double   srcb_re,                   double   srcb_aber);

void _be_mul_si_aber( double * dest_re, double * dest_im, double * dest_aber, 
                      double   srca_re, double   srca_im, double   srca_aber,
                      slong    srcb                                           );

void _be_real_mul_si_aber( double * dest_re, double * dest_aber, 
                           double   srca_re, double   srca_aber,
                           slong    srcb                        );


BEFFT_INLINE void _be_mul( double * dest_re, double * dest_im, 
                           double   srca_re, double   srca_im,
                           double   srcb_re, double   srcb_im){
    *dest_re = srca_re*srcb_re - srca_im*srcb_im;
    *dest_im = srca_re*srcb_im + srca_im*srcb_re;
}
#define BEFFT_RELER_be_mul BEFFT_SQRTFIVE /* computed(mul) = (1+f)*exact(mul) with f\in\C and ||f||<=BEFFT_RELER_be_mul*u */

/*sets dest to srca*srcb with unknown error */
/* used only for tests in test_devel/be.c */
BEFFT_INLINE void be_mul( be_t dest, const be_t srca, const be_t srcb ){
    _be_mul( &be_realref(dest), &be_imagref(dest), 
             be_realref(srca), be_imagref(srca),
             be_realref(srcb), be_imagref(srcb) );
    be_aberref(dest) = -1;
}

void _be_mul_aber( double * dest_re, double * dest_im, double * dest_aber, 
                   double   srca_re, double   srca_im, double   srca_aber,
                   double   srcb_re, double   srcb_im, double   srcb_aber);

/* used only for tests in test_devel/be.c */
BEFFT_INLINE void be_mul_error( be_t dest, const be_t srca, const be_t srcb ){
         _be_mul_aber( &be_realref(dest), &be_imagref(dest), &be_aberref(dest),
                        be_realref(srca), be_imagref(srca), be_aberref(srca),
                        be_realref(srcb), be_imagref(srcb), be_aberref(srcb) );
}

#define BEFFT_RELER_be_real_mul 1
BEFFT_INLINE void _be_real_mul( double * dest, double srca, double srcb) {
    *dest = srca*srcb;
}

void _be_real_mul_aber( double * dest, double *dest_aber,
                        double   srca, double  srca_aber,
                        double   srcb, double  srcb_aber);

#define BEFFT_RELER_be_real_inv 1
BEFFT_INLINE void _be_real_inv( double * dest, double src) {
    *dest = 1/src;
}

void _be_real_inv_aber( double * dest, double *dest_aber,
                        double   src , double  src_aber  );

#define BEFFT_RELER_be_real_div 1
BEFFT_INLINE void _be_real_div( double * dest, double srca, double srcb) {
    *dest = srca/srcb;
}

void _be_real_div_aber( double * dest, double *dest_aber,
                        double   srca, double  srca_aber,
                        double   srcb, double  srcb_aber   );

/* if no underflow , */
/* for all double x, | evact(sqrt(x)) - computed(sqrt(x)) | <  u*computed(sqrt(x)) */ 
/* see for instance HAL Id: hal-00934443
https://hal.inria.fr/hal-00934443v2
On relative errors of floating-point operations: optimal
bounds and applications                                 
Jeannerod and Rump                                        */
#define BEFFT_RELER_be_real_sqrt 1
void _be_real_sqrt_aber( double *dest, double *dest_aber,
                         double  srca, double  srca_aber );

/* returns 1  => surely overlaps */
/* returns 0  => surely no overlaps */
/* returns -1 => can not decide */
int _be_real_overlaps ( double a, double ae,
                        double b, double be );

int _be_overlaps ( double ar, double ai, double ae,
                   double br, double bi, double be );
BEFFT_INLINE int be_overlaps( const be_t a, const be_t b ) {
    return _be_overlaps( be_realref(a), be_imagref(a), be_aberref(a), 
                         be_realref(b), be_imagref(b), be_aberref(b) );
}
#ifdef BEFFT_HAS_ARB
void _be_omega_slong_ulong( double * dest_real, double * dest_imag, double * dest_aber,
                            slong j, ulong k );
/* sets dest to e^(-j*I*Pi/k)*/
void be_omega_slong_ulong( be_t dest, slong j, ulong k );
#endif

#ifdef __cplusplus
}
#endif

#endif
